package com.mozaik.beskem.fcm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mozaik.beskem.ui.MainActivity;
import com.mozaik.beskem.ui.detailkomunitas.DetailKomunitasActivity;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i(TAG, "FAK LOG: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.i(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData());
//                JSONObject json = new JSONObject(String.valueOf(response.getJSONObject()));
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
//        else {
//            // Check if message contains a notification payload.
//            if (remoteMessage.getNotification() != null) {
//                Log.i(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
//                handleNotification(remoteMessage.getNotification().getBody());
//            }
//        }

    }

    private void handleNotification(String message) {
        System.out.println("Message " + message);
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//            Intent pushNotification = new Intent(this, NotifActivity.class);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification

        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.i(TAG, "push json: " + json.toString());

        try {
            String title = json.getString("title");
            String body = json.getString("body");
            String code = json.getString("code");
            String type = json.getString("type");

            System.out.println("Activity " + type);
            System.out.println("Code " + code);
            System.out.println("title " + title);
            System.out.println("message " + body);
            Intent resultIntent;
            if (type.equals("mission")) {
                resultIntent = new Intent(this, DetailKomunitasActivity.class);
            } else {
                System.out.println("Activity Kosong 2 ");
                resultIntent = new Intent(this, MainActivity.class);
                resultIntent.putExtra("message", "");
                resultIntent.putExtra("activity", type);
                resultIntent.putExtra("title", "");
            }

            Log.d("klik", "3");
            showNotificationMessage(getApplicationContext(), title, body, null, resultIntent);
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
        Log.d("klik", "5");
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
        Log.d("klik", "1");
    }
}