package com.mozaik.beskem.di;


import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.remote.RemoteDataSource;

public class Injection {

    public static DataRepository provideDataRepository(Context context) {
        return DataRepository.getInstance(RemoteDataSource.getInstance(context));
    }
}
