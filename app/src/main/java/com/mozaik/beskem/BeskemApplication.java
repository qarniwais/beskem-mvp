package com.mozaik.beskem;

import android.app.Application;

import com.mozaik.beskem.data.local.DbOpenHelper;
import com.mozaik.beskem.data.model.DaoMaster;
import com.mozaik.beskem.data.model.DaoSession;
import com.mozaik.beskem.utils.Constants;

public class BeskemApplication extends Application {


    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        // encrypted SQLCipher database
        // note: you need to add SQLCipher to your dependencies, check the build.gradle file
        // DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db-encrypted");
        // Database db = helper.getEncryptedWritableDb("encryption-key");

        daoSession = new DaoMaster(new DbOpenHelper(this,  Constants.DATABASE_NAME).getWritableDatabase()).newSession();

    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
