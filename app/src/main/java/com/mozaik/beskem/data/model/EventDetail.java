package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventDetail {
    @SerializedName("member_code")
    @Expose
    private String memberCode;
    @SerializedName("community_code")
    @Expose
    private String communityCode;
    @SerializedName("event_code")
    @Expose
    private String eventCode;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("event_image")
    @Expose
    private String eventImage;
    @SerializedName("event_date_start")
    @Expose
    private String eventDateStart;
    @SerializedName("event_date_end")
    @Expose
    private String eventDateEnd;
    @SerializedName("event_time_start")
    @Expose
    private String eventTimeStart;
    @SerializedName("event_time_end")
    @Expose
    private String eventTimeEnd;
    @SerializedName("location_latitude")
    @Expose
    private String locationLatitude;
    @SerializedName("location_longitude")
    @Expose
    private String locationLongitude;
    @SerializedName("location_convertion")
    @Expose
    private String locationConvertion;
    @SerializedName("event_type")
    @Expose
    private String eventType;
    @SerializedName("event_max_member")
    @Expose
    private String eventMaxMember;
    @SerializedName("event_max_date")
    @Expose
    private String eventMaxDate;
    @SerializedName("event_status")
    @Expose
    private String eventStatus;
    @SerializedName("event_date_created")
    @Expose
    private String eventDateCreated;
    @SerializedName("event_image_url")
    @Expose
    private String eventImageUrl;
    @SerializedName("has_rsvp")
    @Expose
    private Integer hasRsvp;

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEventDateStart() {
        return eventDateStart;
    }

    public void setEventDateStart(String eventDateStart) {
        this.eventDateStart = eventDateStart;
    }

    public String getEventDateEnd() {
        return eventDateEnd;
    }

    public void setEventDateEnd(String eventDateEnd) {
        this.eventDateEnd = eventDateEnd;
    }

    public String getEventTimeStart() {
        return eventTimeStart;
    }

    public void setEventTimeStart(String eventTimeStart) {
        this.eventTimeStart = eventTimeStart;
    }

    public String getEventTimeEnd() {
        return eventTimeEnd;
    }

    public void setEventTimeEnd(String eventTimeEnd) {
        this.eventTimeEnd = eventTimeEnd;
    }

    public String getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(String locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public String getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(String locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public String getLocationConvertion() {
        return locationConvertion;
    }

    public void setLocationConvertion(String locationConvertion) {
        this.locationConvertion = locationConvertion;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventMaxMember() {
        return eventMaxMember;
    }

    public void setEventMaxMember(String eventMaxMember) {
        this.eventMaxMember = eventMaxMember;
    }

    public String getEventMaxDate() {
        return eventMaxDate;
    }

    public void setEventMaxDate(String eventMaxDate) {
        this.eventMaxDate = eventMaxDate;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getEventDateCreated() {
        return eventDateCreated;
    }

    public void setEventDateCreated(String eventDateCreated) {
        this.eventDateCreated = eventDateCreated;
    }

    public String getEventImageUrl() {
        return eventImageUrl;
    }

    public void setEventImageUrl(String eventImageUrl) {
        this.eventImageUrl = eventImageUrl;
    }

    public Integer getHasRsvp() {
        return hasRsvp;
    }

    public void setHasRsvp(Integer hasRsvp) {
        this.hasRsvp = hasRsvp;
    }

}
