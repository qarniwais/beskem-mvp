package com.mozaik.beskem.data.local;

import com.mozaik.beskem.data.model.Provinsi;

import java.util.List;

public interface DbHelper {

    void insertProvinsi(List<Provinsi> list);
    void getAllProvinsi(List<Provinsi> list);
}
