package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;


@Entity(nameInDb = "kecamatan")
public class Kecamatan {

    @Id(autoincrement = true)
    Long id;
    @Index(unique = true)
    @SerializedName("district_code")
    @Expose
    private String kecCode;
    @SerializedName("city_code")
    @Expose
    private String cityCode;
    @SerializedName("district_name")
    @Expose
    private String districtName;

    @Generated(hash = 1600565755)
    public Kecamatan(Long id, String kecCode, String cityCode,
            String districtName) {
        this.id = id;
        this.kecCode = kecCode;
        this.cityCode = cityCode;
        this.districtName = districtName;
    }

    @Generated(hash = 766660894)
    public Kecamatan() {
    }

    public String getKecCode() {
        return kecCode;
    }

    public void setKecCode(String kecamatanCode) {
        this.kecCode = kecamatanCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}