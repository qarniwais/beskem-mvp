package com.mozaik.beskem.data.remote;


import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.response.AttendanceResponse;
import com.mozaik.beskem.data.response.BuatKomunitasResponse;
import com.mozaik.beskem.data.response.CheckJoinResponse;
import com.mozaik.beskem.data.response.CheckRegisterResponse;
import com.mozaik.beskem.data.response.DefaultResponse;
import com.mozaik.beskem.data.response.DetailKomunitasResponse;
import com.mozaik.beskem.data.response.EventDetailResponse;
import com.mozaik.beskem.data.response.EventResponse;
import com.mozaik.beskem.data.response.ForgotPasswordResponse;
import com.mozaik.beskem.data.response.KecamatanResponse;
import com.mozaik.beskem.data.response.KelurahanResponse;
import com.mozaik.beskem.data.response.KomunitasKategoriResponse;
import com.mozaik.beskem.data.response.KomunitasMemberResponse;
import com.mozaik.beskem.data.response.KomunitasPostDetailResponse;
import com.mozaik.beskem.data.response.KomunitasPostResponse;
import com.mozaik.beskem.data.response.KomunitasPostUpdateResponse;
import com.mozaik.beskem.data.response.KomunitasResponse;
import com.mozaik.beskem.data.response.KotaResponse;
import com.mozaik.beskem.data.response.LaporanResponse;
import com.mozaik.beskem.data.response.LoginResponse;
import com.mozaik.beskem.data.response.LogoutResponse;
import com.mozaik.beskem.data.response.MemberRequestResponse;
import com.mozaik.beskem.data.response.NewsDetailResponse;
import com.mozaik.beskem.data.response.NewsResponse;
import com.mozaik.beskem.data.response.NotifCountResponse;
import com.mozaik.beskem.data.response.NotificationMResponse;
import com.mozaik.beskem.data.response.PencarianResponse;
import com.mozaik.beskem.data.response.PencarianUserResponse;
import com.mozaik.beskem.data.response.ProfileMemberResponse;
import com.mozaik.beskem.data.response.ProfileResponse;
import com.mozaik.beskem.data.response.ProvinsiResponse;
import com.mozaik.beskem.data.response.RegisterStepOneResponse;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.data.response.UpdatePasswordResponse;
import com.mozaik.beskem.data.response.UpdateProfileResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface RestApiService {

    // Login
    @FormUrlEncoded
    @POST("api/auth/login")
    Call<LoginResponse> login(@Field("email") String email,
                              @Field("password") String password,
                              @Field("screen_category") String screen_category,
                              @Field("device_dencity_1") String device_dencity_1,
                              @Field("device_dencity_2") String device_dencity_2,
                              @Field("screen_size_in_pixel") String screen_size_in_pixel,
                              @Field("screen_size_in_inchi") String screen_size_in_inchi,
                              @Field("os_version") String osversion,
                              @Field("api_version") String apiversion,
                              @Field("device") String device,
                              @Field("model") String model,
                              @Field("manufacturer") String manufacturer,
                              @Field("product") String product,
                              @Field("imei") String imei,
                              @Field("imsi") String imsi,
                              @Field("sim_operator") String sim_id,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longtude);

    @POST("api/auth/logout")
    Call<LogoutResponse> logout(
            @Header("Accept") String accept,
            @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api/user/register_step1")
    Call<RegisterStepOneResponse> registerStepOne(@Field("email") String email,
                                                  @Field("name") String name,
                                                  @Field("user_phone") String user_phone,
                                                  @Field("password") String password,
                                                  @Field("konfirmasi_password") String konfirmasi_password);

    @FormUrlEncoded
    @POST("api/user/register_step2")
    Call<RegisterStepTwoResponse> registerStepTwo(@Field("email") String email,
                                                  @Field("province_code") String provinceCode,
                                                  @Field("city_code") String cityCode,
                                                  @Field("district_code") String districtCode,
                                                  @Field("kelurahan_code") String kelurahan_code);

    @FormUrlEncoded
    @PUT("api/user/profile")
    Call<UpdateProfileResponse> updateProfile(
            @Header("Authorization") String token,
            @Field("name") String name,
            @Field("user_phone") String user_phone,
            @Field("province_code") String province_code,
            @Field("city_code") String city_code,
            @Field("district_code") String district_code,
            @Field("kelurahan_code") String kelurahan_code,
            @Field("kode_pos") String kode_pos,
            @Field("user_img") String user_img,
            @Field("email") String email);

    //update password
    @FormUrlEncoded
    @POST("api/user/profile/password")
    Call<UpdatePasswordResponse> updatePassword(
            @Header("Authorization") String token,
            @Field("old_password") String oldpassword,
            @Field("new_password") String newpassword
    );

    @GET("api/user/profile")
    Call<ProfileResponse> getProfile(
            @Header("Authorization") String token,
            @Query("test") String asyu);

    @GET("api/komunitas/member")
    Call<KomunitasMemberResponse> getMember(
            @Header("Authorization") String token,
            @Query("community_code") String communitycode,
            @Query("limit") String limit,
            @Query("offset") String offset
    );

    @GET("api/news")
    Call<NewsResponse> getNews(
            @Header("Authorization") String token
    );

    // posting
    @GET("api/komunitas/posting")
    Call<KomunitasPostResponse> getPost(
            @Header("Authorization") String token,
            @Query("community_code") String comumunitycode
    );

    @GET("api/komunitas/posting/detail")
    Call<KomunitasPostDetailResponse> getPostDetail(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Header("Content-Type") String contentType,
            @Query("posting_code") String postcode
    );

    @FormUrlEncoded
    @POST("api/komunitas/posting")
    Call<KomunitasPostUpdateResponse> inputPost(
            @Header("Authorization") String token,
            @Field("community_code") String communitycode,
            @Field("posting_content") String postingcontent,
            @Field("posting_media") String postingmedia,
            @Field("posting_judul") String postingjudul
    );

    @FormUrlEncoded
    @PUT("api/komunitas/posting")
    Call<KomunitasPostUpdateResponse> updatePost(
            @Header("Authorization") String token,
            @Field("posting_code") String postingcode,
            @Field("posting_judul") String postingjudul,
            @Field("posting_content") String postingcontent,
            @Field("posting_media") String postingmedia
    );

    //register cek
    @FormUrlEncoded
    @POST("api/user/check_register")
    Call<CheckRegisterResponse> checkRegister(
            @Field("email") String email
    );

    // Master
    @GET("api/master/provinsi")
    Call<ProvinsiResponse> getProvinsi();

    @GET("api/master/kota")
    Call<KotaResponse> getKota();

    @GET("api/master/district")
    Call<KecamatanResponse> getKecamatan(@Query("city_code") String cityCode);

    @GET("api/master/kelurahan")
    Call<KelurahanResponse> getKelurahan(@Query("district_code") String districtCode);

    @GET("api/news/detail")
    Call<NewsDetailResponse> getNewsDetail(
            @Header("Authorization") String token,
            @Query("news_code") String newsCode);


    @GET("api/komunitas/all")
    Call<KomunitasResponse> getAllKomunitas(
            @Header("Authorization") String token
    );

    @GET("api/komunitas")
    Call<KomunitasResponse> getKomunitas(
            @Header("Authorization") String token
    );

    @GET("api/komunitas/kategori")
    Call<KomunitasKategoriResponse> getKomunitasKategori(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("api/komunitas")
    Call<BuatKomunitasResponse> createKomunitas(
            @Header("Authorization") String token,
            @Field("community_cover") String cover,
            @Field("community_icon") String icon,
            @Field("community_name") String name,
            @Field("community_description") String description,
            @Field("community_website") String website,
            @Field("category_code") String categoryCode,
            @Field("community_tanggal_berdiri") String tanggalBerdiri,
            @Field("has_question") String has_question,
            @Field("community_question_teks") String questionTeks
    );

    @FormUrlEncoded
    @PUT("api/komunitas")
    Call<DefaultResponse> editKomunitas(
            @Header("Authorization") String token,
            @Field("community_code") String code,
            @Field("community_name") String name,
            @Field("community_description") String description,
            @Field("community_website") String website,
            @Field("community_tanggal_berdiri") String tanggal_berdiri,
            @Field("community_icon") String icon,
            @Field("community_cover") String cover,
            @Field("category_code") String categoryCode,
            @Field("has_question") String has_question,
            @Field("community_question_code") String questionCode,
            @Field("community_question_teks") String questionTeks
    );

    @GET("api/komunitas/detail")
    Call<DetailKomunitasResponse> getKomunitasDetail(
            @Header("Authorization") String token,
            @Query("community_code") String communityCode,
            @Query("member_code") String memberCode);


    @GET("api/komunitas/search")
    Call<PencarianResponse> searchKomunitas(
            @Header("Authorization") String token,
            @Query("name") String name,
            @Query("limit") String limit,
            @Query("offset") String offset
    );

    @GET("api/user/search")
    Call<PencarianUserResponse> pencarianUser(
            @Header("Authorization") String token,
            @Query("community_code") String communitycode,
            @Query("name") String name,
            @Query("limit") String limit,
            @Query("offset") String offset
    );

    @FormUrlEncoded
    @POST("api/user/forgot")
    Call<ForgotPasswordResponse> forgotPassword(
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("api/komunitas/member")
    Call<DefaultResponse> addMember(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("community_code") String communityCode,
            @Field("user_code") String userCode
    );

    @FormUrlEncoded
    @POST("api/komunitas/member/join")
    Call<DefaultResponse> joinKomunitas(
            @Header("Authorization") String token,
            @Field("community_code") String communitycode,
            @Field("question_code") String question_code,
            @Field("answer_text") String answer_text

    );


    @FormUrlEncoded
    @POST("api/komunitas/member/delete")
    Call<DefaultResponse> deleteMember(
            @Header("Authorization") String token,
            @Field("community_code") String communityCode,
            @Field("user_code") String userCode);

    @GET("api/komunitas/laporan")
    Call<LaporanResponse> getKomunitasLaporan(
            @Header("Authorization") String token,
            @Query("community_code") String community_code
    );

    @FormUrlEncoded
    @POST("api/komunitas/laporan")
    Call<DefaultResponse> createLaporan(
            @Header("Authorization") String token,
            @Field("community_code") String communityCode,
            @Field("member_code") String memberCode,
            @Field("user_code") String userCode,
            @Field("community_laporan_title") String laporan_title,
            @Field("community_laporan_desc") String desc,
            @Field("community_laporan_img") String img,
            @Field("community_laporan_latitude") String latitude,
            @Field("community_laporan_latitude") String longitude
    );

    @FormUrlEncoded
    @PUT("api/komunitas/laporan")
    Call<DefaultResponse> editLaporan(
            @Header("Authorization") String token,
            @Field("community_laporan_code") String laporanCode,
            @Field("community_code") String communityCode,
            @Field("member_code") String memberCode,
            @Field("user_code") String userCode,
            @Field("community_laporan_title") String laporan_title,
            @Field("community_laporan_desc") String desc,
            @Field("community_laporan_img") String img,
            @Field("community_laporan_latitude") String latitude,
            @Field("community_laporan_latitude") String longitude,
            @Field("community_laporan_status") String status
    );

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "api/komunitas/laporan", hasBody = true)
    Call<DefaultResponse> deleteLaporan(@Header("Authorization") String token,
                                        @Field("community_laporan_code") String laporanCode
    );

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "api/komunitas/posting", hasBody = true)
    Call<DefaultResponse> deletePosting(@Header("Authorization") String token,
                                        @Field("posting_code") String postingcode
    );

    //get member request
    @GET("api/komunitas/member/request")
    Call<MemberRequestResponse> getMemberRequest(
            @Header("Authorization") String token,
            @Query("community_code") String communitycode
    );

    @FormUrlEncoded
    @POST("api/komunitas/member/request")
    Call<DefaultResponse> postMemberRequest(
            @Header("Authorization") String token,
            @Field("profile_to_community_code") String profile_to_community_code,
            @Field("user_code") String user_code,
            @Field("status_approve") String status_approve,
            @Field("member_code") String member_code,
            @Field("member_level") String member_level
    );

    //event
    @GET("api/event")
    Call<EventResponse> getEvent(
            @Header("Authorization") String token,
            @Query("community_code") String communitycode
    );

    @FormUrlEncoded
    @POST("api/event")
    Call<DefaultResponse> buatEvent(
            @Header("Authorization") String token,
            @Field("community_code") String community_code,
            @Field("member_code") String member_code,
            @Field("event_name") String event_name,
            @Field("event_description") String event_description,
            @Field("event_image") String event_image,
            @Field("event_date_start") String event_date_start,
            @Field("event_date_end") String event_date_end,
            @Field("event_time_start") String event_time_start,
            @Field("event_time_end") String event_time_end,
            @Field("location_latitude") String location_latitude,
            @Field("location_longitude") String location_longitude,
            @Field("location_convertion") String location_convertion,
            @Field("event_max_member") String event_max_member,
            @Field("event_max_date") String event_max_date,
            @Field("has_rsvp") String has_rsvp
    );

    @FormUrlEncoded
    @PUT("api/event")
    Call<DefaultResponse> updateEvent(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("event_code") String event_code,
            @Field("community_code") String community_code,
            @Field("member_code") String member_code,
            @Field("event_name") String event_name,
            @Field("event_description") String event_description,
            @Field("event_image") String event_image,
            @Field("event_date_start") String event_date_start,
            @Field("event_date_end") String event_date_end,
            @Field("event_time_start") String event_time_start,
            @Field("event_time_end") String event_time_end,
            @Field("location_latitude") String location_latitude,
            @Field("location_longitude") String location_longitude,
            @Field("location_convertion") String location_convertion,
            @Field("has_rsvp") String has_rsvp,
            @Field("event_max_member") String event_max_member,
            @Field("event_max_date") String event_max_date
    );

    @GET("api/event/detail")
    Call<EventDetailResponse> getEventDetail(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Query("event_code") String eventcode
    );

    @FormUrlEncoded
    @POST("api/event/delete")
    Call<DefaultResponse> deleteEvent(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("event_code") String event_code
    );

    @GET("api/user/profile/member")
    Call<ProfileMemberResponse> getQR(
            @Header("Authorization") String token,
            @Query("community_code") String community_code
    );

    //cek token
    @GET("api/user/checktoken")
    Call<DefaultResponse> cekToken(
            @Header("Authorization") String token,
            @Header("Accept") String accept
    );

    //attendance
    @FormUrlEncoded
    @POST("api/event/rsvp/attendance")
    Call<DefaultResponse> joinKegiatan(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("member_code") String member_code,
            @Field("event_code") String event_code,
            @Field("user_code") String userCode
    );

    //attendance  cancel join
    @FormUrlEncoded
    @POST("api/event/rsvp/attendance/cancel")
    Call<DefaultResponse> cancelJoin(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("event_code") String event_code,
            @Field("user_code") String userCode,
            @Field("member_code") String member_code

    );
    // attendance check join
    @FormUrlEncoded
    @POST("api/event/rsvp/attendance/checkJoin")
    Call<CheckJoinResponse> checkJoinAttendance(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("event_code") String event_code,
            @Field("member_code") String member_code,
            @Field("user_code") String userCode
    );

    //attendance get
    @GET("api/event/rsvp/attendance")
    Call<AttendanceResponse> getAttendance(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Query("event_code") String event_code
    );

    //checkQR
    @FormUrlEncoded
    @POST("api/event/rsvp/attendance/check")
    Call<DefaultResponse> checkQR(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("event_code") String event_code,
            @Field("qr_generate") String qr_generate
    );


    // update FCM
    @FormUrlEncoded
    @POST("api/user/updateGCM")
    Call<DefaultResponse> updateFCM(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("token_gcm") String token_gcm,
            @Field("imei") String imei
    );

    //get notif count
    @GET("api/notifikasi/count")
    Call<NotifCountResponse> getNotifCount(
            @Header("Authorization") String token,
            @Header("Accept") String accept
    );
    //post notif read
    @FormUrlEncoded
    @POST("api/notifikasi/read")
    Call<DefaultResponse> postReadNotif(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Field("notifikasi_id") String notifikasi_id
    );

    //get notification list
    @GET("api/notifikasi")
    Call<NotificationMResponse> getNotif(
            @Header("Authorization") String token,
            @Header("Accept") String accept,
            @Query("limit") String limit,
            @Query("offset") String offset
    );

}
