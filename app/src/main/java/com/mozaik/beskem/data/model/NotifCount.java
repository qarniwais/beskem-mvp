package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotifCount {
    @SerializedName("unread_notification")
    @Expose
    private Integer unreadNotification;

    public Integer getUnreadNotification() {
        return unreadNotification;
    }

    public void setUnreadNotification(Integer unreadNotification) {
        this.unreadNotification = unreadNotification;
    }

}
