package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "profile")
public class Profile {

    @Id
    Long id;

    @Index(unique = true)
    @SerializedName("user_code")
    @Expose
    private String userCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user_gender")
    @Expose
    private String userGender;
    @SerializedName("user_birthdate")
    @Expose
    private String userBirthdate;
    @SerializedName("user_alamat")
    @Expose
    private String userAlamat;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("district_name")
    @Expose
    private String districtName;
    @SerializedName("kelurahan_name")
    @Expose
    private String kelurahanName;
    @SerializedName("kode_pos")
    @Expose
    private Integer kodePos;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("user_status")
    @Expose
    private Integer userStatus;
    @SerializedName("user_img")
    @Expose
    private String userImg;
    @SerializedName("user_img_link")
    @Expose
    private String userImgLink;

    @Generated(hash = 858835101)
    public Profile(Long id, String userCode, String email, String name,
            String userGender, String userBirthdate, String userAlamat,
            String provinceName, String cityName, String districtName,
            String kelurahanName, Integer kodePos, String userPhone,
            Integer userStatus, String userImg, String userImgLink) {
        this.id = id;
        this.userCode = userCode;
        this.email = email;
        this.name = name;
        this.userGender = userGender;
        this.userBirthdate = userBirthdate;
        this.userAlamat = userAlamat;
        this.provinceName = provinceName;
        this.cityName = cityName;
        this.districtName = districtName;
        this.kelurahanName = kelurahanName;
        this.kodePos = kodePos;
        this.userPhone = userPhone;
        this.userStatus = userStatus;
        this.userImg = userImg;
        this.userImgLink = userImgLink;
    }

    @Generated(hash = 782787822)
    public Profile() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserBirthdate() {
        return userBirthdate;
    }

    public void setUserBirthdate(String userBirthdate) {
        this.userBirthdate = userBirthdate;
    }

    public String getUserAlamat() {
        return userAlamat;
    }

    public void setUserAlamat(String userAlamat) {
        this.userAlamat = userAlamat;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getKelurahanName() {
        return kelurahanName;
    }

    public void setKelurahanName(String kelurahanName) {
        this.kelurahanName = kelurahanName;
    }

    public Integer getKodePos() {
        return kodePos;
    }

    public void setKodePos(Integer kodePos) {
        this.kodePos = kodePos;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getUserImgLink() {
        return userImgLink;
    }

    public void setUserImgLink(String userImgLink) {
        this.userImgLink = userImgLink;
    }

}
