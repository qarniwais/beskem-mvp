package com.mozaik.beskem.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.Event;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.utils.AppDate;

import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.HolderData> {

    Context context;
    List<Event> modelData = new ArrayList<Event>();

    public EventAdapter(Context context, List<Event> modelData) {
        this.context = context;
        this.modelData = modelData;
    }

    @NonNull
    @Override
    public EventAdapter.HolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_berita, viewGroup, false);
//        BeritaAdapter.HolderData holderData = new BeritaAdapter.HolderData(view);
        return new EventAdapter.HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventAdapter.HolderData holderData, int i) {
        Event mData = modelData.get(i);
        Glide.with(context)
                .load(mData.getEventImage())
                .into(holderData.ivBerita);
        holderData.tvContent.setText(mData.getShortDescription());
        holderData.tvDate.setText(AppDate.changeDateFormatTZ(mData.getEventDateCreated()));

    }

    @Override
    public int getItemCount() {
        return modelData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        ImageView ivBerita;
        TextView tvContent;
        TextView tvDate;
        public HolderData(@NonNull View itemView) {
            super(itemView);

            ivBerita = itemView.findViewById(R.id.iv_berita);
            tvContent = itemView.findViewById(R.id.tv_content);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
