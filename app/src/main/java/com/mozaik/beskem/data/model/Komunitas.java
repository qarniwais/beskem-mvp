package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Komunitas {

    @SerializedName("community_code")
    @Expose
    private String communityCode;
    @SerializedName("member_code")
    @Expose
    private String memberCode;
    @SerializedName("community_icon")
    @Expose
    private String communityIcon;
    @SerializedName("community_name")
    @Expose
    private String communityName;
    @SerializedName("community_description")
    @Expose
    private String communityDescription;
    @SerializedName("community_status")
    @Expose
    private String communityStatus;
    @SerializedName("member_status")
    @Expose
    private String memberStatus;
    @SerializedName("member_level")
    @Expose
    private String memberLevel;

    public Komunitas(String communityCode, String memberCode, String communityIcon, String communityName, String communityDescription, String communityStatus, String memberStatus, String memberLevel) {
        this.communityCode = communityCode;
        this.memberCode = memberCode;
        this.communityIcon = communityIcon;
        this.communityName = communityName;
        this.communityDescription = communityDescription;
        this.communityStatus = communityStatus;
        this.memberStatus = memberStatus;
        this.memberLevel = memberLevel;
    }

    public String getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getCommunityIcon() {
        return communityIcon;
    }

    public void setCommunityIcon(String communityIcon) {
        this.communityIcon = communityIcon;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getCommunityDescription() {
        return communityDescription;
    }

    public void setCommunityDescription(String communityDescription) {
        this.communityDescription = communityDescription;
    }

    public String getCommunityStatus() {
        return communityStatus;
    }

    public void setCommunityStatus(String communityStatus) {
        this.communityStatus = communityStatus;
    }

    public String getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(String memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

}