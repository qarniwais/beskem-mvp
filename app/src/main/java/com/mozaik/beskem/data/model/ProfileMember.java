package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileMember {
    @SerializedName("community_code")
    @Expose
    private String communityCode;
    @SerializedName("community_name")
    @Expose
    private String communityName;
    @SerializedName("member_code")
    @Expose
    private String memberCode;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("member_status")
    @Expose
    private String memberStatus;
    @SerializedName("member_level")
    @Expose
    private String memberLevel;
    @SerializedName("member_level_name")
    @Expose
    private String memberLevelName;
    @SerializedName("join_date_raw")
    @Expose
    private String joinDateRaw;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("name")
    @Expose
    private String name;

    public String getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(String memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getMemberLevelName() {
        return memberLevelName;
    }

    public void setMemberLevelName(String memberLevelName) {
        this.memberLevelName = memberLevelName;
    }

    public String getJoinDateRaw() {
        return joinDateRaw;
    }

    public void setJoinDateRaw(String joinDateRaw) {
        this.joinDateRaw = joinDateRaw;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
