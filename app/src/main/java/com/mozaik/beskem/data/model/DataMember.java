package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataMember {

    @SerializedName("admin")
    @Expose
    private List<Member> admin = null;
    @SerializedName("member")
    @Expose
    private List<Member> member = null;

    public List<Member> getAdmin() {
        return admin;
    }

    public void setAdmin(List<Member> admin) {
        this.admin = admin;
    }

    public List<Member> getMember() {
        return member;
    }

    public void setMember(List<Member> member) {
        this.member = member;
    }

}