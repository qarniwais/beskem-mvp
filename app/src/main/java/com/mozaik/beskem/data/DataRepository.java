package com.mozaik.beskem.data;

import android.content.Context;
import android.util.Log;

import com.mozaik.beskem.data.model.Attendance;
import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.ChekJoin;
import com.mozaik.beskem.data.model.Event;
import com.mozaik.beskem.data.model.EventDetail;
import com.mozaik.beskem.data.model.Kecamatan;
import com.mozaik.beskem.data.model.Kelurahan;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.data.model.KomunitasKategori;
import com.mozaik.beskem.data.model.KomunitasMember;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.KomunitasPostDetail;
import com.mozaik.beskem.data.model.Kota;
import com.mozaik.beskem.data.model.MemberRequest;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.NotifCount;
import com.mozaik.beskem.data.model.NotificationM;
import com.mozaik.beskem.data.model.Pencarian;
import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.data.model.Profile;
import com.mozaik.beskem.data.model.ProfileMember;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.model.RegisterStepOne;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.data.remote.RemoteDataSource;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.NetworkHelper;

import java.util.List;

public class DataRepository {

    private static DataRepository dataRepository;
    private RemoteDataSource remoteDataSource;
    private NetworkHelper networkHelper;

    public DataRepository(RemoteDataSource remoteDataSource, NetworkHelper networkHelper) {
        this.remoteDataSource = remoteDataSource;
        this.networkHelper = networkHelper;
    }


    public static synchronized DataRepository getInstance(RemoteDataSource remoteDataSource) {
        if (dataRepository == null) {
            dataRepository = new DataRepository(remoteDataSource, null);
        }
        return dataRepository;
    }

    public void login(Context context, String username, String password, String screen_category,
                      String device_density, String device_density2, String screen_in_pixel,
                      String screen_in_inchi, String osversion, String apiversion,
                      String device, String model, String manufacturer, String product,
                      String imei, String imsi, String simId, String lat, String lng,
                      final DataResponse.LoginCallback callback) {

        Log.i("DP", "Data Repo");
        if (networkHelper.isNetworkAvailable(context)) {
            remoteDataSource.login(username, password, screen_category,
                    device_density, device_density2, screen_in_pixel,
                    screen_in_inchi, osversion, apiversion,
                    device, model, manufacturer, product,
                    imei, imsi, simId, lat, lng, new DataResponse.LoginCallback() {

                        @Override
                        public void onSuccess(User user) {
                            callback.onSuccess(user);
                        }

                        @Override
                        public void onMessage(String message) {
                            callback.onMessage(message);

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            callback.onFailure(throwable);

                        }

                    });
        }

    }

    public void logout(Context context, String accept, String token, final DataResponse.LogoutCallback callback) {

        remoteDataSource.logout(accept, token, new DataResponse.LogoutCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void registerStepOne(Context context, String email, String name, String user_phone, String password, String konfirmasi_password, final DataResponse.RegisterStepOneCallback callback) {
        remoteDataSource.registerStepOne(email, name, user_phone, password, konfirmasi_password, new DataResponse.RegisterStepOneCallback() {
            @Override
            public void onSuccess(RegisterStepOne registerStepOne) {
                callback.onSuccess(registerStepOne);
            }

            @Override
            public void onMessage(RegisterStepOne message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void registerStepTwo(Context context, String email, String province_code, String city_code, String district_code, String kelurahan_code, final DataResponse.RegisterStepTwoCallback callback) {
        remoteDataSource.registerStepTwo(email, province_code, city_code, district_code, kelurahan_code, new DataResponse.RegisterStepTwoCallback() {
            @Override
            public void onSuccess(String registerStepTwo) {
                callback.onSuccess(registerStepTwo);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getProfile(Context context, String token, final DataResponse.ProfileCallback callback) {
        if (networkHelper.isNetworkAvailable(context)) {
            remoteDataSource.getProfile(token, new DataResponse.ProfileCallback() {
                @Override
                public void onSuccess(Profile profile) {
                    callback.onSuccess(profile);
                }

                @Override
                public void onMessage(String message) {
                    callback.onMessage(message);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    callback.onFailure(throwable);
                }
            });
        }

    }

    public void checkRegister(Context context, String email, final DataResponse.CheckRegisterCallback callback) {
        remoteDataSource.checkRegister(email, new DataResponse.CheckRegisterCallback() {
            @Override
            public void onSuccess(CheckRegister checkRegister) {
                callback.onSuccess(checkRegister);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getNews(Context context, String token, final DataResponse.NewsCallback callback) {
        remoteDataSource.getNews(token, new DataResponse.NewsCallback() {
            @Override
            public void onSuccess(List<News> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }


    public void getProvinsi(Context context, final DataResponse.ProvinsiCallback callback) {
        Log.i("DP", "Data Repo Prov");
        remoteDataSource.getProvinsi(new DataResponse.ProvinsiCallback() {
            @Override
            public void onSuccess(List<Provinsi> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);

            }
        });
    }

    public void getKota(Context context, final DataResponse.KotaCallback callback) {
        remoteDataSource.getKota(new DataResponse.KotaCallback() {
            @Override
            public void onSuccess(List<Kota> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);

            }
        });

    }

    public void getKecamatan(Context context, String idKota, final DataResponse.KecamatanCallback callback) {
        remoteDataSource.getKecamatan(idKota, new DataResponse.KecamatanCallback() {
            @Override
            public void onSuccess(List<Kecamatan> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);

            }
        });
    }


    public void getKelurahan(Context context, String idKec, final DataResponse.KelurahanCallback callback) {

        remoteDataSource.getKelurahan(idKec, new DataResponse.KelurahanCallback() {
            @Override
            public void onSuccess(List<Kelurahan> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);

            }
        });
    }

    public void getNewsDetail(Context context, String token, String newsCode, final DataResponse.NewsDetailCallback callback) {
        remoteDataSource.getNewsDetail(token, newsCode, new DataResponse.NewsDetailCallback() {
            @Override
            public void onSuccess(News news) {
                callback.onSuccess(news);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);

            }
        });
    }


    public void getAllKomunitas(Context context, String token, final DataResponse.AllKomunitasCallback callback) {
        remoteDataSource.getAllKomunitas(token, new DataResponse.AllKomunitasCallback() {
            @Override
            public void onSuccess(List<Komunitas> list) {
                callback.onSuccess(list);

            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getKomunitas(Context context, String token, final DataResponse.KomunitasCallback callback) {
        remoteDataSource.getKomunitas(token, new DataResponse.KomunitasCallback() {
            @Override
            public void onSuccess(List<Komunitas> list) {
                callback.onSuccess(list);

            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void createKomunitas(Context context, String token, String cover, String icon, String name, String desc, String website, String categoryCode,
                                String tanggalBerdiri, String hasQuestion, String questionTeks, final DataResponse.CreateKomunitasCallback callback) {
        remoteDataSource.createKomunitas(token, cover, icon, name, desc, website,
                categoryCode, tanggalBerdiri, hasQuestion,
                questionTeks, new DataResponse.CreateKomunitasCallback() {
                    @Override
                    public void onSuccess(String message) {
                        callback.onSuccess(message);
                    }

                    @Override
                    public void onMessage(String message) {
                        callback.onMessage(message);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        callback.onFailure(throwable);

                    }
                });

    }

    public void forgotPassword(Context context, String email, final DataResponse.ForgotPasswordCallback callback) {
        remoteDataSource.forgotPassword(email, new DataResponse.ForgotPasswordCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void searchKomunitas(Context context, String token, String name, String limit, String offset, final DataResponse.SearchKomunitasCallback callback) {
        remoteDataSource.searchKomunitas(token, name, limit, offset, new DataResponse.SearchKomunitasCallback() {
            @Override
            public void onSuccess(List<Pencarian> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void updateProfile(Context context, String token, String name, String user_phone, String province_code, String city_code, String district_code, String kelurahan_code, String kode_pos, String user_img, String email, final DataResponse.UpdateProfileCallback callback) {
        remoteDataSource.updateProfile(token, name, user_phone, province_code, city_code, district_code, kelurahan_code, kode_pos, user_img, email, new DataResponse.UpdateProfileCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void detailKomunitas(Context context, String token, String communityCode, String memberCode, final DataResponse.DetailKomunitasCallback callback) {
        remoteDataSource.getDetailKomunitas(token, communityCode, memberCode, new DataResponse.DetailKomunitasCallback() {
            @Override
            public void onSuccess(KomunitasDetail komunitas) {
                callback.onSuccess(komunitas);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void updatePassword(Context context, String token, String oldPassword, String newPassword, final DataResponse.UpdatePasswordCallback callback) {
        remoteDataSource.updatePassword(token, oldPassword, newPassword, new DataResponse.UpdatePasswordCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getMember(Context context, String token, String community_code, String limit, String offset, final DataResponse.MemberCallback callback) {
        remoteDataSource.getMember(token, community_code, limit, offset, new DataResponse.MemberCallback() {
            @Override
            public void onSuccess(List<KomunitasMember> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getPost(Context context, String token, String community_code, final DataResponse.PostCallback callback) {
        remoteDataSource.getPost(token, community_code, new DataResponse.PostCallback() {
            @Override
            public void onSuccess(List<KomunitasPost> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getPostDetail(Context context, String token, String accept, String contentType, String post_code, final DataResponse.PostDetailCallback callback) {
        remoteDataSource.getPostDetail(token, accept, contentType, post_code, new DataResponse.PostDetailCallback() {
            @Override
            public void onSuccess(KomunitasPostDetail list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                onFailure(throwable);
            }
        });
    }

    public void updatePost(Context context, String token, String posting_code, String posting_judul, String posting_content, String posting_media, final DataResponse.UpdatePostCallback callback) {
        remoteDataSource.updatePost(token, posting_code, posting_judul, posting_content, posting_media, new DataResponse.UpdatePostCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void inputPost(Context context, String token, String community_code, String posting_content, String posting_media, String posting_judul, final DataResponse.InputPostCallback callback) {
        remoteDataSource.inputPost(token, community_code, posting_content, posting_media, posting_judul, new DataResponse.InputPostCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void updateKomunitas(Context context, String token, String communityCode, String name, String desc, String website, String tanggalBerdiri, String icon, String cover, String categoryCode, String hasQuestion, String questionCode, String questionTeks, final DataResponse.EditKomunitasCallback callback) {
        remoteDataSource.editKomunitas(token, communityCode, name, desc, website, tanggalBerdiri, icon, cover, categoryCode, hasQuestion, questionCode, questionTeks, new DataResponse.EditKomunitasCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void deleteMember(Context context, String token, String community_code, String user_code, final DataResponse.DeleteMemberCallback callback) {
        remoteDataSource.DeleteMember(token, community_code, user_code, new DataResponse.DeleteMemberCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getKomunitasKategori(Context context, String token, final DataResponse.KomunitasKategoriCallback callback) {
        remoteDataSource.getKomunitasKategori(token, new DataResponse.KomunitasKategoriCallback() {
            @Override
            public void onSuccess(List<KomunitasKategori> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void cariUser(Context context, String token, String communityCode, String name, String limit, String offset, final DataResponse.PencarianUserCallback callback) {
        remoteDataSource.pencarianUser(token, communityCode, name, limit, offset, new DataResponse.PencarianUserCallback() {
            @Override
            public void onSuccess(List<PencarianUser> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void joinKomunitas(Context context, String token, String community_code, String question_code, String answer_text, final DataResponse.JoinKomunitasCallback callback) {
        remoteDataSource.joinKomunitas(token, community_code, question_code, answer_text, new DataResponse.JoinKomunitasCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void deletePosting(Context context, String token, String posting_code, final DataResponse.DeletePostingCallback callback) {
        remoteDataSource.deletePosting(token, posting_code, new DataResponse.DeletePostingCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void getMemberRequest(Context context, String token, String community_code, final DataResponse.MemberRequestCallback callback) {
        remoteDataSource.getMemberRequest(token, community_code, new DataResponse.MemberRequestCallback() {
            @Override
            public void onSuccess(List<MemberRequest> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getEvent(Context context, String token, String community_code, final DataResponse.EventCallback callback) {
        remoteDataSource.getEvent(token, community_code, new DataResponse.EventCallback() {
            @Override
            public void onSuccess(List<Event> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void buatEvent(Context context, String token, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String event_max_member, String event_max_date, String has_rsvp, final DataResponse.BuatKegiatanCallback callback) {
        remoteDataSource.buatKegiatan(token, community_code, member_code, event_name, event_description, event_image, event_date_start, event_date_end, event_time_start, event_time_end, location_latitude, location_longitude, location_convertion, event_max_member, event_max_date, has_rsvp, new DataResponse.BuatKegiatanCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void konfirmasiMemberRequest(Context context, String token, String profile_to_community_code, String user_code,
                                        String status_approve, String member_code, String member_level, final DataResponse.KonfirmasiMemberRequestCallback callback) {
        remoteDataSource.konfirmasiMemberRequest(token, profile_to_community_code, user_code, status_approve, member_code, member_level, new DataResponse.KonfirmasiMemberRequestCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void getQR(Context context, String token, String community_code, final DataResponse.QRCodeCallback callback) {
        remoteDataSource.getQR(token, community_code, new DataResponse.QRCodeCallback() {
            @Override
            public void onSuccess(ProfileMember list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void CekToken(Context context, String token, String accept, final DataResponse.TokenCallback callback) {
        remoteDataSource.cekToken(token, accept, new DataResponse.TokenCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getEventDetail(Context context, String token, String accept, String event_code, final DataResponse.EventDetailCallback callback) {
        remoteDataSource.eventDetail(token, accept, event_code, new DataResponse.EventDetailCallback() {
            @Override
            public void onSuccess(EventDetail response) {
                callback.onSuccess(response);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void updateEvent(Context context, String token, String accept, String event_code, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String has_rsvp, String event_max_member, String event_max_date, final DataResponse.UpdateKegiatanCallback callback) {
        remoteDataSource.updateKegiatan(token, accept, event_code, community_code, member_code, event_name, event_description, event_image, event_date_start, event_date_end, event_time_start, event_time_end, location_latitude, location_longitude, location_convertion, has_rsvp, event_max_member, event_max_date, new DataResponse.UpdateKegiatanCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void deleteEvent(Context context, String token, String accept, String event_code, final DataResponse.HapusEventCallback callback) {
        remoteDataSource.hapusEvent(token, accept, event_code, new DataResponse.HapusEventCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void joinKegiatan(Context context, String token, String accept, String member_code, String event_code, String user_code, final DataResponse.JoinEventCallback callback) {
        remoteDataSource.joinEvent(token, accept, member_code, event_code, user_code, new DataResponse.JoinEventCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void batalJoin(Context context,String token,String accept,String event_code,String user_code,String member_code, final DataResponse.CancelJoinCallback callback){
        remoteDataSource.cancelJoin(token, accept, event_code, user_code, member_code, new DataResponse.CancelJoinCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void checkJoinAttendance(Context context,String token,String accept,String event_code,String member_code,String user_code, final DataResponse.CheckJoinCallback callback){
        remoteDataSource.checkJoinAtt(token, accept, event_code, member_code, user_code, new DataResponse.CheckJoinCallback() {
            @Override
            public void onSuccess(ChekJoin message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void updateFcm(Context context, String token, String tokenFcm, String imei, final DataResponse.UpdateFcmCallback callback) {
        remoteDataSource.updateFCM(token, tokenFcm, imei, new DataResponse.UpdateFcmCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getAttendance(Context context, String token, String accept, String event_code, final  DataResponse.AttendanceCallback callback){
        remoteDataSource.getAttendance(token, accept, event_code, new DataResponse.AttendanceCallback() {
            @Override
            public void onSuccess(List<Attendance> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void cekQR(Context context, String token, String accept, String event_code, String qr_generate, final DataResponse.CheckQRCallback callback){
        remoteDataSource.checkQR(token, accept, event_code, qr_generate, new DataResponse.CheckQRCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });

    }

    public void CountNotif(Context context, String token, String accept, final DataResponse.NotifCountCallback callback){
        remoteDataSource.NotifCount(token, accept, new DataResponse.NotifCountCallback() {
            @Override
            public void onSuccess(NotifCount notif) {
                callback.onSuccess(notif);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void Addmember(Context context, String token, String accept, String community_code, String user_code, final DataResponse.AddMemberCallback callback){
        remoteDataSource.AddMember(token, accept, community_code, user_code, new DataResponse.AddMemberCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void getNotif(Context context, String token, String accept, String limit, String offset, final DataResponse.GetNotifCallback callback){
        remoteDataSource.GetNotif(token, accept, limit, offset, new DataResponse.GetNotifCallback() {
            @Override
            public void onSuccess(List<NotificationM> list) {
                callback.onSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }

    public void postReadNotif(Context context, String token, String accept, String notifikasi_id, final DataResponse.PostNotifReadCallback callback){
        remoteDataSource.PostNotifRead(token, accept, notifikasi_id, new DataResponse.PostNotifReadCallback() {
            @Override
            public void onSuccess(String message) {
                callback.onSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                callback.onMessage(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
    }
}
