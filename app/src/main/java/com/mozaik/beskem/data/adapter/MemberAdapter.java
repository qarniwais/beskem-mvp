package com.mozaik.beskem.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.KomunitasMember;

import java.util.ArrayList;
import java.util.List;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.HolderData> {

    Context context;
    List<KomunitasMember> list = new ArrayList<KomunitasMember>();

    public MemberAdapter(Context context, List<KomunitasMember> list){
        this.context = context;
        this.list = list;

    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_cari_komunitas, viewGroup, false);
        return new MemberAdapter.HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holderData, int i) {
        KomunitasMember komunitas = list.get(i);
        if (komunitas.getMemberLevel() == 0){
            holderData.layout.setVisibility(View.VISIBLE);
            holderData.view.setVisibility(View.VISIBLE);
            holderData.tv_name.setText(komunitas.getName());
            Glide.with(context)
                    .load(komunitas.getUserImg())
                    .into(holderData.iv_komunitas);
        }


    }

    @Override
    public int getItemCount() {
//        if (list.size()>5){
//            return 6;
//        }else {
            return list.size();
//        }
    }

    public class HolderData extends RecyclerView.ViewHolder {
        ImageView iv_komunitas;
        TextView tv_name;
        RelativeLayout layout;
        View view;
        public HolderData(@NonNull View itemView) {
            super(itemView);
            iv_komunitas = itemView.findViewById(R.id.iv_komunitas);
            tv_name = itemView.findViewById(R.id.tv_name);
            view = itemView.findViewById(R.id.viewA);
            layout = itemView.findViewById(R.id.relatif);
        }
    }
}
