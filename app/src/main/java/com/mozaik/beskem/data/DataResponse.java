package com.mozaik.beskem.data;

import android.content.Context;

import com.mozaik.beskem.data.model.Attendance;
import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.ChekJoin;
import com.mozaik.beskem.data.model.Event;
import com.mozaik.beskem.data.model.EventDetail;
import com.mozaik.beskem.data.model.Kecamatan;
import com.mozaik.beskem.data.model.Kelurahan;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.data.model.KomunitasKategori;
import com.mozaik.beskem.data.model.KomunitasMember;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.KomunitasPostDetail;
import com.mozaik.beskem.data.model.Kota;
import com.mozaik.beskem.data.model.Laporan;
import com.mozaik.beskem.data.model.MemberRequest;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.NotifCount;
import com.mozaik.beskem.data.model.NotificationM;
import com.mozaik.beskem.data.model.Pencarian;
import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.data.model.Profile;
import com.mozaik.beskem.data.model.ProfileMember;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.model.RegisterStepOne;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;

import java.util.List;

public abstract class DataResponse {

    Context context;

    public DataResponse(Context context) {
        this.context = context;
    }

    public abstract void login(String username, String password, String screen_category,
                               String device_density, String device_density2, String screen_in_pixel,
                               String screen_in_inchi, String osversion, String apiversion,
                               String device, String model, String manufacturer, String product,
                               String imei, String imsi, String simId, String lat, String lng,
                               LoginCallback callback);

    public abstract void logout(String accept, String token, LogoutCallback callback);


    public abstract void registerStepOne(String email, String name, String user_phone, String password, String konfirmasi_password, RegisterStepOneCallback callback);

    public abstract void registerStepTwo(String email, String province_code, String city_code, String district_code, String kelurahan_code, RegisterStepTwoCallback callback);

    public abstract void checkRegister(String email, CheckRegisterCallback callback);

    public abstract void getProfile(String token, ProfileCallback callback);

    public abstract void getNews(String token, NewsCallback callback);

    public abstract void getProvinsi(ProvinsiCallback callback);

    public abstract void getKota(KotaCallback callback);

    public abstract void getKecamatan(String idKota, KecamatanCallback callback);

    public abstract void getKelurahan(String idKec, KelurahanCallback callback);

    public abstract void getNewsDetail(String token, String newsCode, NewsDetailCallback callback);

    public abstract void getAllKomunitas(String token, AllKomunitasCallback callback);

    public abstract void getKomunitas(String token, KomunitasCallback callback);

    public abstract void getMember(String token, String community_code, String limit, String offset, MemberCallback callback);

    public abstract void createKomunitas(String token, String cover, String icon,
                                         String name, String desc, String website,
                                         String categoryCode, String tanggalBerdiri,
                                         String hasQuestion, String questionTeks,
                                         CreateKomunitasCallback callback);

    public abstract void editKomunitas(String token, String communityCode, String name, String desc, String website, String tanggalBerdiri, String icon, String cover, String categoryCode, String hasQuestion, String questionCode, String questionTeks,
                                       EditKomunitasCallback callback);

    public abstract void getDetailKomunitas(String token, String communityCode,
                                            String memberCode, DetailKomunitasCallback callback);

    public abstract void searchKomunitas(String token, String name, String limit, String offset,
                                         SearchKomunitasCallback callback);

    public abstract void pencarianUser(String token, String community_code, String name, String limit, String offset,
                                       PencarianUserCallback callback);

    public abstract void AddMember(String token, String accept, String communityCode, String userCode, AddMemberCallback callback);

    public abstract void DeleteMember(String token, String communityCode, String userCode, DeleteMemberCallback callback);

    public abstract void getLaporan(String token, String communityCode, GetLaporanCallback callback);

    public abstract void createLaporan(String token, String communityCode, String memberCode, String userCode,
                                       String laporan_title, String desc, String img, String latitude,
                                       String longitude, CreateLaporanCallback callback);

    public abstract void editLaporan(String token, String laporanCode, String communityCode, String memberCode,
                                     String userCode, String laporan_title, String desc, String img,
                                     String latitude, String longitude, String status, EditLaporanCallback callback);

    public abstract void deleteLaporan(String token, String laporanCode, DeleteLaporanCallback callback);

    public abstract void forgotPassword(String email, ForgotPasswordCallback callback);

    public abstract void updatePassword(String token, String oldPassword, String newPassword, UpdatePasswordCallback callback);

    public abstract void updateProfile(String token, String name, String user_phone, String province_code, String city_code, String district_code, String kelurahan_code, String kode_pos, String user_img, String email, UpdateProfileCallback callback);

    public abstract void getPost(String token, String community_code, PostCallback callback);

    public abstract void getPostDetail(String token,String accept,String contentType, String post_code, PostDetailCallback callback);

    public abstract void updatePost(String token, String posting_code, String posting_judul, String posting_content, String posting_media, UpdatePostCallback callback);

    public abstract void inputPost(String token, String community_code, String posting_content, String posting_media, String posting_judul, InputPostCallback callback);

    public abstract void getKomunitasKategori(String token, KomunitasKategoriCallback callback);

    public abstract void deletePosting(String token, String postingCode, DeletePostingCallback callback);

    public abstract void getMemberRequest(String token, String communityCode, MemberRequestCallback callback);

    public abstract void getEvent(String token, String community_code, EventCallback callback);

    public abstract void joinKomunitas(String token, String community_code, String question_code, String answer_text, JoinKomunitasCallback callback);

    public abstract void buatKegiatan(String token, String community_code, String member_code, String event_name,
                                     String event_description, String event_image, String event_date_start, String event_date_end,
                                     String event_time_start, String event_time_end, String location_latitude,
                                      String location_longitude, String location_convertion, String event_max_member, String event_max_date,String has_rsvp, BuatKegiatanCallback callback);

    public abstract void konfirmasiMemberRequest(String token, String profile_to_community_code, String user_code,
                                                 String status_approve, String member_code, String member_level, KonfirmasiMemberRequestCallback callback);

    public abstract void getQR(String token, String community_code, QRCodeCallback callback);

    public abstract void cekToken(String token, String accept, TokenCallback callback);

    public abstract void eventDetail(String token,String accept, String event_code, EventDetailCallback callback);

    public abstract void updateKegiatan(String token,String accept, String event_code, String community_code, String member_code, String event_name,
                                      String event_description, String event_image, String event_date_start, String event_date_end,
                                      String event_time_start, String event_time_end, String location_latitude,
                                      String location_longitude, String location_convertion,String has_rsvp, String event_max_member, String event_max_date, UpdateKegiatanCallback callback);

    public abstract void hapusEvent(String token,String accept, String event_code, HapusEventCallback callback);
    public abstract void cancelJoin(String token,String accept,String event_code,String user_code,String member_code,  CancelJoinCallback callback);
    public abstract void joinEvent(String token,String accept,String member_code, String event_code,String user_code, JoinEventCallback callback);

    public abstract void updateFCM(String token,String accept, String event_code, UpdateFcmCallback callback);

    public abstract void getAttendance(String token,String accept, String event_code, AttendanceCallback callback);
    public abstract void checkQR(String token,String accept, String event_code,String qr_generate, CheckQRCallback callback);


    public abstract void checkJoinAtt(String token,String accept,String event_code,String member_code,String user_code,  CheckJoinCallback callback);

    public abstract void NotifCount(String token,String accept, NotifCountCallback callback);

    public abstract void GetNotif(String token,String accept, String limit, String offset, GetNotifCallback callback);

    public abstract void PostNotifRead(String token,String accept, String notifikasi_id, PostNotifReadCallback callback);

    public interface LoginCallback {

        void onSuccess(User user);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface PostNotifReadCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface GetNotifCallback {

        void onSuccess(List<NotificationM> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface NotifCountCallback {

        void onSuccess(NotifCount notif);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface AttendanceCallback {

        void onSuccess(List<Attendance> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface CheckJoinCallback {

        void onSuccess(ChekJoin response);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface EventDetailCallback {

        void onSuccess(EventDetail response);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface UpdateKegiatanCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface CheckQRCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface CancelJoinCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface JoinEventCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface HapusEventCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface TokenCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface QRCodeCallback {

        void onSuccess(ProfileMember list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface BuatKegiatanCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface EventCallback {

        void onSuccess(List<Event> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface MemberRequestCallback {

        void onSuccess(List<MemberRequest> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface KonfirmasiMemberRequestCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface DeletePostingCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface JoinKomunitasCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface LogoutCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface ProvinsiCallback {

        void onSuccess(List<Provinsi> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface RegisterStepOneCallback {

        void onSuccess(RegisterStepOne registerStepOne);

        void onMessage(RegisterStepOne message);

        void onFailure(Throwable throwable);

    }

    public interface RegisterStepTwoCallback {

        void onSuccess(String registerStepTwo);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface ProfileCallback {

        void onSuccess(Profile profile);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface CheckRegisterCallback {

        void onSuccess(CheckRegister checkRegister);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface NewsCallback {

        void onSuccess(List<News> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface KotaCallback {

        void onSuccess(List<Kota> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface KecamatanCallback {

        void onSuccess(List<Kecamatan> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface KelurahanCallback {

        void onSuccess(List<Kelurahan> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }


    public interface NewsDetailCallback {

        void onSuccess(News news);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }


    public interface AllKomunitasCallback {

        void onSuccess(List<Komunitas> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface KomunitasCallback {

        void onSuccess(List<Komunitas> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface PostCallback {

        void onSuccess(List<KomunitasPost> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface PostDetailCallback {

        void onSuccess(KomunitasPostDetail list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface UpdatePostCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface InputPostCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface CreateKomunitasCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface EditKomunitasCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface DetailKomunitasCallback {

        void onSuccess(KomunitasDetail komunitas);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface SearchKomunitasCallback {

        void onSuccess(List<Pencarian> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface PencarianUserCallback {

        void onSuccess(List<PencarianUser> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }


    public interface AddMemberCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface DeleteMemberCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface ForgotPasswordCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface UpdatePasswordCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface GetLaporanCallback {

        void onSuccess(List<Laporan> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface CreateLaporanCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface EditLaporanCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface DeleteLaporanCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface MemberCallback {

        void onSuccess(List<KomunitasMember> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface UpdateProfileCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

    public interface KomunitasKategoriCallback {

        void onSuccess(List<KomunitasKategori> list);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }



    public interface UpdateFcmCallback {

        void onSuccess(String message);

        void onMessage(String message);

        void onFailure(Throwable throwable);

    }

}
