package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "kelurahan")
public class Kelurahan {

    @Id(autoincrement = true)
    Long id;
    @Index(unique = true)
    @SerializedName("kelurahan_code")
    @Expose
    private String kelurahanCode;
    @SerializedName("district_code")
    @Expose
    private String kecCode;
    @SerializedName("kelurahan_name")
    @Expose
    private String kelurahanName;

    @Generated(hash = 590994742)
    public Kelurahan(Long id, String kelurahanCode, String kecCode,
            String kelurahanName) {
        this.id = id;
        this.kelurahanCode = kelurahanCode;
        this.kecCode = kecCode;
        this.kelurahanName = kelurahanName;
    }

    @Generated(hash = 700128520)
    public Kelurahan() {
    }

    public String getKelurahanCode() {
        return kelurahanCode;
    }

    public void setKelurahanCode(String kelurahanCode) {
        this.kelurahanCode = kelurahanCode;
    }

    public String getKecCode() {
        return kecCode;
    }

    public void setKecCode(String kecCode) {
        this.kecCode = kecCode;
    }

    public String getKelurahanName() {
        return kelurahanName;
    }

    public void setKelurahanName(String kelurahanName) {
        this.kelurahanName = kelurahanName;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
