package com.mozaik.beskem.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.NotificationM;
import com.mozaik.beskem.data.model.PencarianUser;

import java.util.List;

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.HolderData> {

    Context context;
    private List<NotificationM> list;

    public NotifAdapter(Context context, List<NotificationM> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_notif, viewGroup, false);
        HolderData holderData = new HolderData(view);
        return holderData;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holderData, int i) {
        NotificationM mData = list.get(i);
        holderData.tvName.setText(mData.getMsg());
        holderData.tv_date.setText(mData.getCreatedAt());
        Glide.with(context)
                .load(R.drawable.image)
                .into(holderData.ivKomunitas);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        ImageView ivKomunitas;
        TextView tvName,tv_date;

        public HolderData(@NonNull View itemView) {
            super(itemView);
            ivKomunitas = itemView.findViewById(R.id.iv_komunitas);
            tvName = itemView.findViewById(R.id.tv_name);
            tv_date = itemView.findViewById(R.id.tv_date);
        }
    }
}
