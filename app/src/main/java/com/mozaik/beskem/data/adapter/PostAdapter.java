package com.mozaik.beskem.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.utils.AppDate;

import java.util.ArrayList;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.HolderData> {

    Context context;
    List<KomunitasPost> postData = new ArrayList<KomunitasPost>();

    public PostAdapter(Context context, List<KomunitasPost> modelData) {
        this.context = context;
        this.postData = modelData;
    }

    @NonNull
    @Override
    public PostAdapter.HolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_berita, viewGroup, false);
//        BeritaAdapter.HolderData holderData = new BeritaAdapter.HolderData(view);
        return new PostAdapter.HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdapter.HolderData holderData, int i) {
        KomunitasPost mData = postData.get(i);
        Glide.with(context)
                .load(mData.getPostingMedia())
                .into(holderData.ivBerita);
        holderData.tvContent.setText(mData.getPostingJudul());
        holderData.tvDate.setText(AppDate.changeDateFormatTZ(mData.getPostingDateCreated()));

    }

    @Override
    public int getItemCount() {
        return postData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        ImageView ivBerita;
        TextView tvContent;
        TextView tvDate;

        public HolderData(@NonNull View itemView) {
            super(itemView);

            ivBerita = itemView.findViewById(R.id.iv_berita);
            tvContent = itemView.findViewById(R.id.tv_content);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
