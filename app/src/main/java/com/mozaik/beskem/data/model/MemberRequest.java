package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberRequest {
    @SerializedName("profile_to_community_code")
    @Expose
    private String profileToCommunityCode;
    @SerializedName("user_code")
    @Expose
    private String userCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user_img")
    @Expose
    private String userImg;
    @SerializedName("community_question_code")
    @Expose
    private String communityQuestionCode;
    @SerializedName("community_question_teks")
    @Expose
    private String communityQuestionTeks;
    @SerializedName("community_question_answer_teks")
    @Expose
    private String communityQuestionAnswerTeks;

    public String getProfileToCommunityCode() {
        return profileToCommunityCode;
    }

    public void setProfileToCommunityCode(String profileToCommunityCode) {
        this.profileToCommunityCode = profileToCommunityCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getCommunityQuestionCode() {
        return communityQuestionCode;
    }

    public void setCommunityQuestionCode(String communityQuestionCode) {
        this.communityQuestionCode = communityQuestionCode;
    }

    public String getCommunityQuestionTeks() {
        return communityQuestionTeks;
    }

    public void setCommunityQuestionTeks(String communityQuestionTeks) {
        this.communityQuestionTeks = communityQuestionTeks;
    }

    public String getCommunityQuestionAnswerTeks() {
        return communityQuestionAnswerTeks;
    }

    public void setCommunityQuestionAnswerTeks(String communityQuestionAnswerTeks) {
        this.communityQuestionAnswerTeks = communityQuestionAnswerTeks;
    }
}
