package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckRegister {
    @SerializedName("status_check")
    @Expose
    private Integer statusCheck;

    public Integer getStatusCheck() {
        return statusCheck;
    }

    public void setStatusCheck(Integer statusCheck) {
        this.statusCheck = statusCheck;
    }
}
