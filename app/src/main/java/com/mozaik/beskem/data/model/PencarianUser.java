package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PencarianUser {
    @SerializedName("user_code")
    @Expose
    private String userCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user_gender")
    @Expose
    private String userGender;
    @SerializedName("user_img")
    @Expose
    private String userImg;
    @SerializedName("user_img_link")
    @Expose
    private String userImgLink;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getUserImgLink() {
        return userImgLink;
    }

    public void setUserImgLink(String userImgLink) {
        this.userImgLink = userImgLink;
    }

}
