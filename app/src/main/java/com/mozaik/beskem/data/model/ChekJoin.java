package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChekJoin {
    @SerializedName("status_join")
    @Expose
    private Integer statusJoin;

    public Integer getStatusJoin() {
        return statusJoin;
    }

    public void setStatusJoin(Integer statusJoin) {
        this.statusJoin = statusJoin;
    }
}
