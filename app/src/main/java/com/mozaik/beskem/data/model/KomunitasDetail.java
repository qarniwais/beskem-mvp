package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KomunitasDetail {
    @SerializedName("community_code")
    @Expose
    private String communityCode;
    @SerializedName("community_cover")
    @Expose
    private String communityCover;
    @SerializedName("community_icon")
    @Expose
    private String communityIcon;
    @SerializedName("community_name")
    @Expose
    private String communityName;
    @SerializedName("community_description")
    @Expose
    private String communityDescription;
    @SerializedName("community_website")
    @Expose
    private String communityWebsite;
    @SerializedName("category_code")
    @Expose
    private String categoryCode;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("community_tanggal_berdiri")
    @Expose
    private String communityTanggalBerdiri;
    @SerializedName("community_status")
    @Expose
    private Integer communityStatus;
    @SerializedName("has_question")
    @Expose
    private Integer hasQuestion;
    @SerializedName("community_question_code")
    @Expose
    private String communityQuestionCode;
    @SerializedName("community_question_teks")
    @Expose
    private String communityQuestionTeks;

    public String getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    public String getCommunityCover() {
        return communityCover;
    }

    public void setCommunityCover(String communityCover) {
        this.communityCover = communityCover;
    }

    public String getCommunityIcon() {
        return communityIcon;
    }

    public void setCommunityIcon(String communityIcon) {
        this.communityIcon = communityIcon;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getCommunityDescription() {
        return communityDescription;
    }

    public void setCommunityDescription(String communityDescription) {
        this.communityDescription = communityDescription;
    }

    public String getCommunityWebsite() {
        return communityWebsite;
    }

    public void setCommunityWebsite(String communityWebsite) {
        this.communityWebsite = communityWebsite;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCommunityTanggalBerdiri() {
        return communityTanggalBerdiri;
    }

    public void setCommunityTanggalBerdiri(String communityTanggalBerdiri) {
        this.communityTanggalBerdiri = communityTanggalBerdiri;
    }

    public Integer getCommunityStatus() {
        return communityStatus;
    }

    public void setCommunityStatus(Integer communityStatus) {
        this.communityStatus = communityStatus;
    }

    public Integer getHasQuestion() {
        return hasQuestion;
    }

    public void setHasQuestion(Integer hasQuestion) {
        this.hasQuestion = hasQuestion;
    }

    public String getCommunityQuestionCode() {
        return communityQuestionCode;
    }

    public void setCommunityQuestionCode(String communityQuestionCode) {
        this.communityQuestionCode = communityQuestionCode;
    }

    public String getCommunityQuestionTeks() {
        return communityQuestionTeks;
    }

    public void setCommunityQuestionTeks(String communityQuestionTeks) {
        this.communityQuestionTeks = communityQuestionTeks;
    }
}
