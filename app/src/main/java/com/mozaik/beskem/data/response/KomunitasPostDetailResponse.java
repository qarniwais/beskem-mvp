package com.mozaik.beskem.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.KomunitasPostDetail;

import java.util.List;

public class KomunitasPostDetailResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private KomunitasPostDetail data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public KomunitasPostDetail getData() {
        return data;
    }

    public void setData(KomunitasPostDetail data) {
        this.data = data;
    }
}
