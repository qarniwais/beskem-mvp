package com.mozaik.beskem.data.model;

public class ModelData {

    private int image;
    private String name;
    private String alamat;

    public ModelData(int image, String name, String alamat) {
        this.image = image;
        this.name = name;
        this.alamat = alamat;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
