package com.mozaik.beskem.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KomunitasMember {
    @SerializedName("member_level")
    @Expose
    private Integer memberLevel;
    @SerializedName("user_code")
    @Expose
    private String userCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user_img")
    @Expose
    private String userImg;

    public Integer getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(Integer memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }
}
