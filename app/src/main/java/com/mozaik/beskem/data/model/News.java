package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News {

    @SerializedName("news_code")
    @Expose
    private String newsCode;
    @SerializedName("news_title")
    @Expose
    private String newsTitle;
    @SerializedName("news_content_short")
    @Expose
    private String newsContentShort;
    @SerializedName("news_content_long")
    @Expose
    private String newsContentLong;
    @SerializedName("news_media")
    @Expose
    private String newsMedia;
    @SerializedName("news_publisher")
    @Expose
    private String newsPublisher;
    @SerializedName("news_date_create")
    @Expose
    private String newsDateCreate;
    @SerializedName("news_media_link")
    @Expose
    private String newsMediaLink;

    public String getNewsCode() {
        return newsCode;
    }

    public void setNewsCode(String newsCode) {
        this.newsCode = newsCode;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsContentShort() {
        return newsContentShort;
    }

    public void setNewsContentShort(String newsContentShort) {
        this.newsContentShort = newsContentShort;
    }

    public String getNewsContentLong() {
        return newsContentLong;
    }

    public void setNewsContentLong(String newsContentLong) {
        this.newsContentLong = newsContentLong;
    }

    public String getNewsMedia() {
        return newsMedia;
    }

    public void setNewsMedia(String newsMedia) {
        this.newsMedia = newsMedia;
    }

    public String getNewsPublisher() {
        return newsPublisher;
    }

    public void setNewsPublisher(String newsPublisher) {
        this.newsPublisher = newsPublisher;
    }

    public String getNewsDateCreate() {
        return newsDateCreate;
    }

    public void setNewsDateCreate(String newsDateCreate) {
        this.newsDateCreate = newsDateCreate;
    }

    public String getNewsMediaLink() {
        return newsMediaLink;
    }

    public void setNewsMediaLink(String newsMediaLink) {
        this.newsMediaLink = newsMediaLink;
    }
}

