package com.mozaik.beskem.data.local;

import android.content.Context;
import android.util.Log;

import com.mozaik.beskem.BeskemApplication;
import com.mozaik.beskem.data.model.DaoSession;
import com.mozaik.beskem.data.model.Kecamatan;
import com.mozaik.beskem.data.model.KecamatanDao;
import com.mozaik.beskem.data.model.Kelurahan;
import com.mozaik.beskem.data.model.KelurahanDao;
import com.mozaik.beskem.data.model.Kota;
import com.mozaik.beskem.data.model.KotaDao;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.model.ProvinsiDao;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.data.model.UserDao;

import java.util.List;

public class AppDbHelper {

    private DaoSession mDaoSession;
    Context context;

    public AppDbHelper(DaoSession daoSession) {
        this.mDaoSession = daoSession;

    }

    public AppDbHelper(Context context) {
        this.context = context;

    }

    public void open(){
        mDaoSession = ((BeskemApplication)context.getApplicationContext()).getDaoSession();
    }

    public void insertProvinsi(List<Provinsi> list) {
        open();
        ProvinsiDao provinsiDao = mDaoSession.getProvinsiDao();
        provinsiDao.insertOrReplaceInTx(list);
        Log.i("insert prov", "Provinsi saved");
    }

    public List<Provinsi> getAllProvinsi() {
        open();
        ProvinsiDao provinsiDao = mDaoSession.getProvinsiDao();
        List<Provinsi> provinsiList = provinsiDao.queryBuilder().build().list();
        Log.i("get prov", "Provinsi loaded");
        return provinsiList;
    }


    public Provinsi getProvinsiById(String id) {
        open();
        Provinsi prov;
        ProvinsiDao provinsiDao = mDaoSession.getProvinsiDao();
        List<Provinsi> provinsi = provinsiDao.queryBuilder().where(ProvinsiDao.Properties.Id.eq(id)).list();
        prov = provinsi.get(0);
        return prov;
    }


    public void insertKota(List<Kota> list) {
        open();
        KotaDao kotaDao = mDaoSession.getKotaDao();
        kotaDao.insertOrReplaceInTx(list);
    }

    public List<Kota> getAllKota() {
        open();
        KotaDao provinsiDao = mDaoSession.getKotaDao();
        List<Kota> provinsiList = provinsiDao.queryBuilder().build().list();
        return provinsiList;
    }

    public List<Kota> getKotaByProv(String idProv) {
        open();
        KotaDao provinsiDao = mDaoSession.getKotaDao();
        List<Kota> provinsi = provinsiDao.queryBuilder().where(KotaDao.Properties.ProvinceCode.eq(idProv)).list();

        return provinsi;
    }


    public void insertKecamatan(List<Kecamatan> list) {
        open();
        KecamatanDao provinsiDao = mDaoSession.getKecamatanDao();
        provinsiDao.insertOrReplaceInTx(list);
    }

    public List<Kecamatan> getAllKecamatan() {
        open();
        KecamatanDao provinsiDao = mDaoSession.getKecamatanDao();
        List<Kecamatan> provinsiList = provinsiDao.queryBuilder().build().list();
        return provinsiList;
    }

    public List<Kecamatan> getKecamatanByProv(String code) {
        open();
        KecamatanDao provinsiDao = mDaoSession.getKecamatanDao();
        List<Kecamatan> provinsi = provinsiDao.queryBuilder().where(KecamatanDao.Properties.CityCode.eq(code)).list();

        return provinsi;
    }



    public void insertKelurahan(List<Kelurahan> list) {
        open();
        KelurahanDao provinsiDao = mDaoSession.getKelurahanDao();
        provinsiDao.insertOrReplaceInTx(list);
    }

    public List<Kelurahan> getAllKelurahan() {
        open();
        KelurahanDao provinsiDao = mDaoSession.getKelurahanDao();
        List<Kelurahan> provinsiList = provinsiDao.queryBuilder().build().list();
        return provinsiList;
    }

    public List<Kelurahan> getKelurahanByProv(String code) {
        open();
        KelurahanDao provinsiDao = mDaoSession.getKelurahanDao();
        List<Kelurahan> provinsi = provinsiDao.queryBuilder().where(KelurahanDao.Properties.KecCode.eq(code)).list();

        return provinsi;
    }



    public void insertUser(User user) {
        open();
        UserDao provinsiDao = mDaoSession.getUserDao();
        provinsiDao.insertOrReplace(user);
    }


    public User getUserById(String id) {
        open();
        User prov;
        UserDao provinsiDao = mDaoSession.getUserDao();
        List<User> provinsi = provinsiDao.queryBuilder().where(UserDao.Properties.Id.eq(id)).list();
        prov = provinsi.get(0);
        return prov;
    }
}
