package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Laporan {

    @SerializedName("community_laporan_code")
    @Expose
    private String communityLaporanCode;
    @SerializedName("community_laporan_title")
    @Expose
    private String communityLaporanTitle;
    @SerializedName("community_laporan_img")
    @Expose
    private String communityLaporanImg;
    @SerializedName("community_laporan_desc")
    @Expose
    private String communityLaporanDesc;
    @SerializedName("community_laporan_latitude")
    @Expose
    private String communityLaporanLatitude;
    @SerializedName("community_laporan_longitude")
    @Expose
    private String communityLaporanLongitude;
    @SerializedName("community_laporan_alamat_konversi")
    @Expose
    private String communityLaporanAlamatKonversi;
    @SerializedName("community_laporan_status")
    @Expose
    private Integer communityLaporanStatus;
    @SerializedName("member_code")
    @Expose
    private String memberCode;
    @SerializedName("user_code")
    @Expose
    private String userCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;

    public String getCommunityLaporanCode() {
        return communityLaporanCode;
    }

    public void setCommunityLaporanCode(String communityLaporanCode) {
        this.communityLaporanCode = communityLaporanCode;
    }

    public String getCommunityLaporanTitle() {
        return communityLaporanTitle;
    }

    public void setCommunityLaporanTitle(String communityLaporanTitle) {
        this.communityLaporanTitle = communityLaporanTitle;
    }

    public String getCommunityLaporanImg() {
        return communityLaporanImg;
    }

    public void setCommunityLaporanImg(String communityLaporanImg) {
        this.communityLaporanImg = communityLaporanImg;
    }

    public String getCommunityLaporanDesc() {
        return communityLaporanDesc;
    }

    public void setCommunityLaporanDesc(String communityLaporanDesc) {
        this.communityLaporanDesc = communityLaporanDesc;
    }

    public String getCommunityLaporanLatitude() {
        return communityLaporanLatitude;
    }

    public void setCommunityLaporanLatitude(String communityLaporanLatitude) {
        this.communityLaporanLatitude = communityLaporanLatitude;
    }

    public String getCommunityLaporanLongitude() {
        return communityLaporanLongitude;
    }

    public void setCommunityLaporanLongitude(String communityLaporanLongitude) {
        this.communityLaporanLongitude = communityLaporanLongitude;
    }

    public String getCommunityLaporanAlamatKonversi() {
        return communityLaporanAlamatKonversi;
    }

    public void setCommunityLaporanAlamatKonversi(String communityLaporanAlamatKonversi) {
        this.communityLaporanAlamatKonversi = communityLaporanAlamatKonversi;
    }

    public Integer getCommunityLaporanStatus() {
        return communityLaporanStatus;
    }

    public void setCommunityLaporanStatus(Integer communityLaporanStatus) {
        this.communityLaporanStatus = communityLaporanStatus;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

}