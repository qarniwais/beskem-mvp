package com.mozaik.beskem.data.remote;

import android.content.Context;

import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.response.AttendanceResponse;
import com.mozaik.beskem.data.response.BuatKomunitasResponse;
import com.mozaik.beskem.data.response.CheckJoinResponse;
import com.mozaik.beskem.data.response.CheckRegisterResponse;
import com.mozaik.beskem.data.response.DefaultDeserialization;
import com.mozaik.beskem.data.response.DefaultResponse;
import com.mozaik.beskem.data.response.DetailKomunitasResponse;
import com.mozaik.beskem.data.response.EventDetailResponse;
import com.mozaik.beskem.data.response.EventResponse;
import com.mozaik.beskem.data.response.ForgotPasswordResponse;
import com.mozaik.beskem.data.response.KecamatanResponse;
import com.mozaik.beskem.data.response.KelurahanResponse;
import com.mozaik.beskem.data.response.KomunitasKategoriResponse;
import com.mozaik.beskem.data.response.KomunitasMemberResponse;
import com.mozaik.beskem.data.response.KomunitasPostDetailResponse;
import com.mozaik.beskem.data.response.KomunitasPostResponse;
import com.mozaik.beskem.data.response.KomunitasPostUpdateResponse;
import com.mozaik.beskem.data.response.KomunitasResponse;
import com.mozaik.beskem.data.response.KotaResponse;
import com.mozaik.beskem.data.response.LoginDeserialization;
import com.mozaik.beskem.data.response.LoginResponse;
import com.mozaik.beskem.data.response.LogoutResponse;
import com.mozaik.beskem.data.response.MemberRequestResponse;
import com.mozaik.beskem.data.response.NewsDetailResponse;
import com.mozaik.beskem.data.response.NewsResponse;
import com.mozaik.beskem.data.response.NotifCountResponse;
import com.mozaik.beskem.data.response.NotificationMResponse;
import com.mozaik.beskem.data.response.PencarianResponse;
import com.mozaik.beskem.data.response.PencarianUserResponse;
import com.mozaik.beskem.data.response.ProfileMemberResponse;
import com.mozaik.beskem.data.response.ProfileResponse;
import com.mozaik.beskem.data.response.ProvinsiResponse;
import com.mozaik.beskem.data.response.RegisterStepOneResponse;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.data.response.UpdatePasswordResponse;
import com.mozaik.beskem.data.response.UpdateProfileResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteDataSource extends DataResponse {

    private static RemoteDataSource remoteDataSource;
    Context context;

    public RemoteDataSource(Context context) {
        super(context);
    }

    public static synchronized RemoteDataSource getInstance(Context context) {
        if (remoteDataSource == null) {

            remoteDataSource = new RemoteDataSource(context);
        }
        return remoteDataSource;
    }

    @Override
    public void login(String username, String password, String screen_category,
                      String device_density, String device_density2, String screen_in_pixel,
                      String screen_in_inchi, String osversion, String apiversion,
                      String device, String model, String manufacturer, String product,
                      String imei, String imsi, String simId, String lat, String lng,
                      final LoginCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, LoginDeserialization.converterFactory());
        Call<LoginResponse> call = service.login(username, password, screen_category, device_density, device_density2,
                screen_in_pixel, screen_in_inchi, osversion, apiversion, device, model, manufacturer, product, imei, imsi, simId, lat, lng);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void logout(String accept, String token, final LogoutCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<LogoutResponse> call = service.logout(accept, token);
        call.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getProvinsi(final ProvinsiCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<ProvinsiResponse> call = service.getProvinsi();
        call.enqueue(new Callback<ProvinsiResponse>() {
            @Override
            public void onResponse(Call<ProvinsiResponse> call, Response<ProvinsiResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProvinsiResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });

    }

    @Override
    public void getKota(final KotaCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KotaResponse> call = service.getKota();
        call.enqueue(new Callback<KotaResponse>() {
            @Override
            public void onResponse(Call<KotaResponse> call, Response<KotaResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KotaResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    @Override
    public void getKecamatan(String idKota, final KecamatanCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KecamatanResponse> call = service.getKecamatan(idKota);
        call.enqueue(new Callback<KecamatanResponse>() {
            @Override
            public void onResponse(Call<KecamatanResponse> call, Response<KecamatanResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KecamatanResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getKelurahan(String idKec, final KelurahanCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KelurahanResponse> call = service.getKelurahan(idKec);
        call.enqueue(new Callback<KelurahanResponse>() {
            @Override
            public void onResponse(Call<KelurahanResponse> call, Response<KelurahanResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KelurahanResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void registerStepOne(String email, String name, String user_phone, String password, String konfirmasi_password, final RegisterStepOneCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<RegisterStepOneResponse> call = service.registerStepOne(email, name, user_phone, password, konfirmasi_password);
        call.enqueue(new Callback<RegisterStepOneResponse>() {
            @Override
            public void onResponse(Call<RegisterStepOneResponse> call, Response<RegisterStepOneResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<RegisterStepOneResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void registerStepTwo(String email, String province_code, String city_code, String district_code, String kelurahan_code, final RegisterStepTwoCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<RegisterStepTwoResponse> call = service.registerStepTwo(email, province_code, city_code, district_code, kelurahan_code);
        call.enqueue(new Callback<RegisterStepTwoResponse>() {
            @Override
            public void onResponse(Call<RegisterStepTwoResponse> call, Response<RegisterStepTwoResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onMessage(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterStepTwoResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getProfile(String token, final ProfileCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<ProfileResponse> call = service.getProfile(token, "");
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void checkRegister(String email, final CheckRegisterCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<CheckRegisterResponse> call = service.checkRegister(email);
        call.enqueue(new Callback<CheckRegisterResponse>() {
            @Override
            public void onResponse(Call<CheckRegisterResponse> call, Response<CheckRegisterResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckRegisterResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getNews(String token, final NewsCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<NewsResponse> call = service.getNews(token);
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });

    }


    @Override
    public void getNewsDetail(String token, String newsCode, final NewsDetailCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<NewsDetailResponse> call = service.getNewsDetail(token, newsCode);
        call.enqueue(new Callback<NewsDetailResponse>() {
            @Override
            public void onResponse(Call<NewsDetailResponse> call, Response<NewsDetailResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getNews());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<NewsDetailResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getAllKomunitas(String token, final AllKomunitasCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasResponse> call = service.getAllKomunitas(token);
        call.enqueue(new Callback<KomunitasResponse>() {
            @Override
            public void onResponse(Call<KomunitasResponse> call, Response<KomunitasResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getKomunitas(String token, final KomunitasCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasResponse> call = service.getKomunitas(token);
        call.enqueue(new Callback<KomunitasResponse>() {
            @Override
            public void onResponse(Call<KomunitasResponse> call, Response<KomunitasResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getPost(String token, String community_code, final PostCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasPostResponse> call = service.getPost(token, community_code);
        call.enqueue(new Callback<KomunitasPostResponse>() {
            @Override
            public void onResponse(Call<KomunitasPostResponse> call, Response<KomunitasPostResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasPostResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void getPostDetail(String token,String accept,String contentType, String post_code, final PostDetailCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasPostDetailResponse> call = service.getPostDetail(token,accept,contentType, post_code);
        call.enqueue(new Callback<KomunitasPostDetailResponse>() {
            @Override
            public void onResponse(Call<KomunitasPostDetailResponse> call, Response<KomunitasPostDetailResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasPostDetailResponse> call, Throwable t) {
                callback.onFailure(t);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void updatePost(String token, String posting_code, String posting_judul, String posting_content, String posting_media, final UpdatePostCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasPostUpdateResponse> call = service.updatePost(token, posting_code, posting_judul, posting_content, posting_media);
        call.enqueue(new Callback<KomunitasPostUpdateResponse>() {
            @Override
            public void onResponse(Call<KomunitasPostUpdateResponse> call, Response<KomunitasPostUpdateResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasPostUpdateResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void inputPost(String token, String community_code, String posting_content, String posting_media, String posting_judul, final InputPostCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasPostUpdateResponse> call = service.inputPost(token, community_code, posting_content, posting_media, posting_judul);
        call.enqueue(new Callback<KomunitasPostUpdateResponse>() {
            @Override
            public void onResponse(Call<KomunitasPostUpdateResponse> call, Response<KomunitasPostUpdateResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasPostUpdateResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getKomunitasKategori(String token, final KomunitasKategoriCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasKategoriResponse> call = service.getKomunitasKategori(token);
        call.enqueue(new Callback<KomunitasKategoriResponse>() {
            @Override
            public void onResponse(Call<KomunitasKategoriResponse> call, Response<KomunitasKategoriResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasKategoriResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void deletePosting(String token, String postingCode, final DeletePostingCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.deletePosting(token, postingCode);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getMemberRequest(String token, String communityCode, final MemberRequestCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<MemberRequestResponse> call = service.getMemberRequest(token, communityCode);
        call.enqueue(new Callback<MemberRequestResponse>() {
            @Override
            public void onResponse(Call<MemberRequestResponse> call, Response<MemberRequestResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<MemberRequestResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getEvent(String token, String community_code, final EventCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<EventResponse> call = service.getEvent(token, community_code);
        call.enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void joinKomunitas(String token, String community_code, String question_code, String answer_text, final JoinKomunitasCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.joinKomunitas(token, community_code, question_code,answer_text);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void buatKegiatan(String token, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String event_max_member, String event_max_date, String has_rsvp, final BuatKegiatanCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.buatEvent(token, community_code,member_code,event_name, event_description, event_image,event_date_start, event_date_end, event_time_start,event_time_end, location_latitude, location_longitude, location_convertion, event_max_member, event_max_date, has_rsvp);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void konfirmasiMemberRequest(String token, String profile_to_community_code, String user_code, String status_approve, String member_code, String member_level, final KonfirmasiMemberRequestCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.postMemberRequest(token, profile_to_community_code, user_code, status_approve, member_code, member_level);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getQR(String token, String community_code, final QRCodeCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<ProfileMemberResponse> call = service.getQR(token, community_code);
        call.enqueue(new Callback<ProfileMemberResponse>() {
            @Override
            public void onResponse(Call<ProfileMemberResponse> call, Response<ProfileMemberResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileMemberResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void cekToken(String token, String accept, final TokenCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.cekToken(token, accept);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void eventDetail(String token,String accept, String event_code, final EventDetailCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<EventDetailResponse> call = service.getEventDetail(token,accept, event_code);
        call.enqueue(new Callback<EventDetailResponse>() {
            @Override
            public void onResponse(Call<EventDetailResponse> call, Response<EventDetailResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<EventDetailResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void updateKegiatan(String token, String accept, String event_code, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String has_rsvp, String event_max_member, String event_max_date, final UpdateKegiatanCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.updateEvent(token, accept, event_code, community_code, member_code, event_name, event_description, event_image,event_date_start, event_date_end, event_time_start,event_time_end,location_latitude, location_longitude, location_convertion, has_rsvp, event_max_member, event_max_date);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void hapusEvent(String token, String accept, String event_code, final HapusEventCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.deleteEvent(token, accept, event_code);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void cancelJoin(String token, String accept, String event_code, String user_code, String member_code, final CancelJoinCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.cancelJoin(token,accept,event_code, user_code,member_code);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void joinEvent(String token, String accept, String member_code, String event_code, String user_code, final JoinEventCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.joinKegiatan(token, accept, member_code, event_code, user_code);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void checkJoinAtt(String token, String accept, String event_code, String member_code, String user_code, final CheckJoinCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<CheckJoinResponse> call = service.checkJoinAttendance(token, accept, event_code, member_code, user_code);
        call.enqueue(new Callback<CheckJoinResponse>() {
            @Override
            public void onResponse(Call<CheckJoinResponse> call, Response<CheckJoinResponse> response) {
               if (response.body().getCode() == 0 ){
                   callback.onSuccess(response.body().getData());
               }else {
                   callback.onMessage(response.body().getMessage());
               }
            }

            @Override
            public void onFailure(Call<CheckJoinResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void NotifCount(String token, String accept, final NotifCountCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<NotifCountResponse> call = service.getNotifCount(token, accept);
        call.enqueue(new Callback<NotifCountResponse>() {
            @Override
            public void onResponse(Call<NotifCountResponse> call, Response<NotifCountResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<NotifCountResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void GetNotif(String token, String accept, String limit, String offset, final GetNotifCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<NotificationMResponse> call = service.getNotif(token, accept, limit, offset);
        call.enqueue(new Callback<NotificationMResponse>() {
            @Override
            public void onResponse(Call<NotificationMResponse> call, Response<NotificationMResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<NotificationMResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void PostNotifRead(String token, String accept, String notifikasi_id, final PostNotifReadCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.postReadNotif(token, accept, notifikasi_id);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getMember(String token, String community_code, String limit, String offset, final MemberCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<KomunitasMemberResponse> call = service.getMember(token, community_code, limit, offset);
        call.enqueue(new Callback<KomunitasMemberResponse>() {
            @Override
            public void onResponse(Call<KomunitasMemberResponse> call, Response<KomunitasMemberResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<KomunitasMemberResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void createKomunitas(String token, String cover, String icon, String name, String desc, String website, String categoryCode,
                                String tanggalBerdiri, String hasQuestion, String questionTeks, final CreateKomunitasCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<BuatKomunitasResponse> call = service.createKomunitas(token, cover, icon, name, desc, website, categoryCode,
                tanggalBerdiri, hasQuestion, questionTeks);
        call.enqueue(new Callback<BuatKomunitasResponse>() {
            @Override
            public void onResponse(Call<BuatKomunitasResponse> call, Response<BuatKomunitasResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BuatKomunitasResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void editKomunitas(String token, String communityCode, String name, String desc, String website, String tanggalBerdiri, String icon, String cover, String categoryCode, String hasQuestion, String questionCode, String questionTeks,
                              final EditKomunitasCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.editKomunitas(token, communityCode, name, desc, website, tanggalBerdiri, icon, cover, categoryCode, hasQuestion, questionCode, questionTeks);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getDetailKomunitas(String token, String communityCode, String memberCode, final DetailKomunitasCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DetailKomunitasResponse> call = service.getKomunitasDetail(token, communityCode, memberCode);
        call.enqueue(new Callback<DetailKomunitasResponse>() {
            @Override
            public void onResponse(Call<DetailKomunitasResponse> call, Response<DetailKomunitasResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DetailKomunitasResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void searchKomunitas(String token, String name, String limit, String offset, final SearchKomunitasCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<PencarianResponse> call = service.searchKomunitas(token, name, limit, offset);
        call.enqueue(new Callback<PencarianResponse>() {
            @Override
            public void onResponse(Call<PencarianResponse> call, Response<PencarianResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getData());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PencarianResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void pencarianUser(String token, String community_code, String name, String limit, String offset, final PencarianUserCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<PencarianUserResponse> call = service.pencarianUser(token, community_code, name, limit, offset);
        call.enqueue(new Callback<PencarianUserResponse>() {
            @Override
            public void onResponse(Call<PencarianUserResponse> call, Response<PencarianUserResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PencarianUserResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    @Override
    public void AddMember(String token, String accept, String communityCode, String userCode, final AddMemberCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.addMember(token, accept, communityCode, userCode);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void DeleteMember(String token, String communityCode, String userCode, final DeleteMemberCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.deleteMember(token, communityCode, userCode);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getLaporan(String token, String communityCode, GetLaporanCallback callback) {

    }

    @Override
    public void createLaporan(String token, String communityCode, String memberCode, String userCode, String laporan_title,
                              String desc, String img, String latitude, String longitude, final CreateLaporanCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.createLaporan(token, communityCode, memberCode, userCode, laporan_title, desc, img, latitude, longitude);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void editLaporan(String token, String laporanCode, String communityCode, String memberCode, String userCode, String laporan_title,
                            String desc, String img, String latitude, String longitude, String status, final EditLaporanCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.editLaporan(token, laporanCode, communityCode, memberCode, userCode, laporan_title,
                desc, img, latitude, longitude, status);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void deleteLaporan(String token, String laporanCode, final DeleteLaporanCallback callback) {

        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.deleteLaporan(token, laporanCode);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void forgotPassword(String email, final ForgotPasswordCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<ForgotPasswordResponse> call = service.forgotPassword(email);
        call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void updatePassword(String token, String oldPassword, String newPassword, final UpdatePasswordCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<UpdatePasswordResponse> call = service.updatePassword(token, oldPassword, newPassword);
        call.enqueue(new Callback<UpdatePasswordResponse>() {
            @Override
            public void onResponse(Call<UpdatePasswordResponse> call, Response<UpdatePasswordResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<UpdatePasswordResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void updateProfile(String token, String name, String user_phone, String province_code, String city_code, String district_code, String kelurahan_code, String kode_pos, String user_img, String email, final UpdateProfileCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<UpdateProfileResponse> call = service.updateProfile(token, name, user_phone, province_code, city_code, district_code, kelurahan_code, kode_pos, user_img, email);
        call.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }


    @Override
    public void updateFCM(String token, String tokenFcm, String imei, final UpdateFcmCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.updateFCM(token, "application/json", tokenFcm, imei);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0) {
                    callback.onSuccess(response.body().getMessage());
                } else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void getAttendance(String token, String accept, String event_code, final AttendanceCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<AttendanceResponse> call = service.getAttendance(token, accept, event_code);
        call.enqueue(new Callback<AttendanceResponse>() {
            @Override
            public void onResponse(Call<AttendanceResponse> call, Response<AttendanceResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getData());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<AttendanceResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    @Override
    public void checkQR(String token, String accept, String event_code, String qr_generate, final CheckQRCallback callback) {
        RestApiService service = Api.getInstance().createService(RestApiService.class, DefaultDeserialization.converterFactory());
        Call<DefaultResponse> call = service.checkQR(token, accept, event_code, qr_generate);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                if (response.body().getCode() == 0){
                    callback.onSuccess(response.body().getMessage());
                }else {
                    callback.onMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }


}
