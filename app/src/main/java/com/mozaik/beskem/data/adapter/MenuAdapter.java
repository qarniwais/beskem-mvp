package com.mozaik.beskem.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.Komunitas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.HolderData> {

    Context context;
    List<Komunitas> list = new ArrayList<Komunitas>();

    public MenuAdapter(Context context, List<Komunitas> list){
        this.context = context;
        this.list = list;

    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_menu, viewGroup, false);
        return new MenuAdapter.HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holderData, int i) {
        Komunitas komunitas = list.get(i);
        holderData.tv_menu.setText(komunitas.getCommunityName());
        Glide.with(context)
                .load(komunitas.getCommunityIcon())
                .into(holderData.iv_menu);

        //get last index array
        if (i == (list.size()-1)){
            Glide.with(context)
                    .load(R.drawable.icons_add)
                    .into(holderData.iv_menu);
            holderData.layout.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
            holderData.layout.setBackgroundResource(R.drawable.bg_white_cyrcle_menu);
        }


    }

    @Override
    public int getItemCount() {
//        if (list.size()>5){
//            return 6;
//        }else {
            return list.size();
//        }
    }

    public class HolderData extends RecyclerView.ViewHolder {
        ImageView iv_menu;
        TextView tv_menu;
        RelativeLayout layout;
        public HolderData(@NonNull View itemView) {
            super(itemView);
            iv_menu = itemView.findViewById(R.id.iv_menu);
            tv_menu = itemView.findViewById(R.id.tv_menu);
            layout = itemView.findViewById(R.id.relatif);
        }
    }
}
