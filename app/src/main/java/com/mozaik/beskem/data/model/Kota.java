package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "kota")
public class Kota {

    @Id(autoincrement = true)
    Long id;

    @Index(unique = true)
    @SerializedName("city_code")
    @Expose
    private String cityCode;
    @SerializedName("province_code")
    @Expose
    private String provinceCode;
    @SerializedName("city_name")
    @Expose
    private String cityName;

    @Generated(hash = 1040174317)
    public Kota(Long id, String cityCode, String provinceCode, String cityName) {
        this.id = id;
        this.cityCode = cityCode;
        this.provinceCode = provinceCode;
        this.cityName = cityName;
    }

    @Generated(hash = 1555707845)
    public Kota() {
    }


    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}