package com.mozaik.beskem.data.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tompihariadi on 19-May-17.
 */

public class DefaultDeserialization implements JsonDeserializer<DefaultResponse> {

    @Override
    public DefaultResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();

        DefaultResponse response = gson.fromJson(json, DefaultResponse.class);
//        System.out.println("test : "+response.toString());
        return response;
    }

    public static GsonConverterFactory converterFactory(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(DefaultResponse.class, new DefaultDeserialization())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
