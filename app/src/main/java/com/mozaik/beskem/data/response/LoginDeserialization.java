package com.mozaik.beskem.data.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mozaik.beskem.data.model.User;

import java.lang.reflect.Type;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ilham Tri Saputra on 18/05/2016.
 */
public class LoginDeserialization implements JsonDeserializer<LoginResponse> {

    @Override
    public LoginResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        LoginResponse response = gson.fromJson(json, LoginResponse.class);

        return response;
    }

    public static GsonConverterFactory converterFactory(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(LoginResponse.class, new LoginDeserialization())
                .setLenient()
                .create();

        return GsonConverterFactory.create(gson);
    }
}
