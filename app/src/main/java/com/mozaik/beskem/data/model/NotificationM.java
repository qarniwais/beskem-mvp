package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationM {
    @SerializedName("notifikasi_id")
    @Expose
    private String notifikasiId;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("is_read")
    @Expose
    private String isRead;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("data")
    @Expose
    private Notif data;

    public String getNotifikasiId() {
        return notifikasiId;
    }

    public void setNotifikasiId(String notifikasiId) {
        this.notifikasiId = notifikasiId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Notif getData() {
        return data;
    }

    public void setData(Notif data) {
        this.data = data;
    }
}
