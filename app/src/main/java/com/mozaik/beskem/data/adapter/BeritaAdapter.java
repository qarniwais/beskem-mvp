package com.mozaik.beskem.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.ModelData;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.utils.AppDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.HolderData> {

    Context context;
    List<News> modelData = new ArrayList<News>();

    public BeritaAdapter(Context context, List<News> modelData) {
        this.context = context;
        this.modelData = modelData;
    }

    @NonNull
    @Override
    public BeritaAdapter.HolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_berita, viewGroup, false);
//        BeritaAdapter.HolderData holderData = new BeritaAdapter.HolderData(view);
        return new BeritaAdapter.HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BeritaAdapter.HolderData holderData, int i) {
        News mData = modelData.get(i);
        Glide.with(context)
                .load(mData.getNewsMediaLink())
                .into(holderData.ivBerita);
        holderData.tvContent.setText(mData.getNewsTitle());
        holderData.tvDate.setText(AppDate.changeDateFormatTZ(mData.getNewsDateCreate()));

    }

    @Override
    public int getItemCount() {
        return modelData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        ImageView ivBerita;
        TextView tvContent;
        TextView tvDate;
        public HolderData(@NonNull View itemView) {
            super(itemView);

            ivBerita = itemView.findViewById(R.id.iv_berita);
            tvContent = itemView.findViewById(R.id.tv_content);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
