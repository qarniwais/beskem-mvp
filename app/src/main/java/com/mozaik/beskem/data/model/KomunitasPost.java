package com.mozaik.beskem.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KomunitasPost {
    @SerializedName("posting_code")
    @Expose
    private String postingCode;
    @SerializedName("posting_judul")
    @Expose
    private String postingJudul;
    @SerializedName("posting_publisher")
    @Expose
    private String postingPublisher;
    @SerializedName("posting_content_short")
    @Expose
    private String postingContentShort;
    @SerializedName("posting_content_long")
    @Expose
    private String postingContentLong;
    @SerializedName("posting_media")
    @Expose
    private String postingMedia;
    @SerializedName("posting_status")
    @Expose
    private String postingStatus;
    @SerializedName("posting_date_created")
    @Expose
    private String postingDateCreated;
    @SerializedName("posting_date_update")
    @Expose
    private String postingDateUpdate;

    public String getPostingCode() {
        return postingCode;
    }

    public void setPostingCode(String postingCode) {
        this.postingCode = postingCode;
    }

    public String getPostingJudul() {
        return postingJudul;
    }

    public void setPostingJudul(String postingJudul) {
        this.postingJudul = postingJudul;
    }

    public String getPostingPublisher() {
        return postingPublisher;
    }

    public void setPostingPublisher(String postingPublisher) {
        this.postingPublisher = postingPublisher;
    }

    public String getPostingContentShort() {
        return postingContentShort;
    }

    public void setPostingContentShort(String postingContentShort) {
        this.postingContentShort = postingContentShort;
    }

    public String getPostingContentLong() {
        return postingContentLong;
    }

    public void setPostingContentLong(String postingContentLong) {
        this.postingContentLong = postingContentLong;
    }

    public String getPostingMedia() {
        return postingMedia;
    }

    public void setPostingMedia(String postingMedia) {
        this.postingMedia = postingMedia;
    }

    public String getPostingStatus() {
        return postingStatus;
    }

    public void setPostingStatus(String postingStatus) {
        this.postingStatus = postingStatus;
    }

    public String getPostingDateCreated() {
        return postingDateCreated;
    }

    public void setPostingDateCreated(String postingDateCreated) {
        this.postingDateCreated = postingDateCreated;
    }

    public String getPostingDateUpdate() {
        return postingDateUpdate;
    }

    public void setPostingDateUpdate(String postingDateUpdate) {
        this.postingDateUpdate = postingDateUpdate;
    }
}
