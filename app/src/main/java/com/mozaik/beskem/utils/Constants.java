package com.mozaik.beskem.utils;

public class Constants {
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PHONE = "user_phone";
    public static final String PASSWORD = "password";
    public static final String USER_CODE = "user_code";
    public static final String TOKEN = "token";
    public static final String DATABASE_NAME = "beskem.db";

    public static final String PROVINSI = "provinsi";
    public static final String KOTA = "kota";
    public static final String KECAMATAN = "kecamatan";
    public static final String KELURAHAN = "kelurahan";

    public static final String COMMUNITYCODE = "community_code";
    public static final String MEMBERCODE = "member_code";
    public static final String NAMEKOMUNITAS = "name_komunitas";
    public static final String EVENTCODE = "event_code";
    public static final String MEMBERLEVEL = "member_level";
    public static final String INFO = "info";
    public static final String DATA = "data";
    public static final String POSTINGCODE = "postingcode";
    public static final String BUATPOST = "buatpost";
    public static final String INFOMEMBER = "infomember";

    public static final String QR = "qr";
    public static final String LOGIN = "login";


    public static final String FCM_TOKEN = "fcm_token";
}
