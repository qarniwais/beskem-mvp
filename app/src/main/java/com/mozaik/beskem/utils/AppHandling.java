package com.mozaik.beskem.utils;

import android.content.Context;
import android.widget.Toast;

import java.io.IOException;
import java.net.SocketTimeoutException;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public class AppHandling {
    public static void handling(Context context, Throwable t){
        if (t instanceof IOException) {
            if(t instanceof SocketTimeoutException) {
                Toast.makeText(context, "Timeout",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context, "No Internet Connection",Toast.LENGTH_SHORT).show();
            }
        }else{
            t.printStackTrace();
        }
    }
}
