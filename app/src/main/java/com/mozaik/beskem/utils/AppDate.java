package com.mozaik.beskem.utils;

import android.util.TimeFormatException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public class AppDate {
    public static String today() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
        Date date = new Date();
        String d = format.format(date);
        return d;
    }

    public static String getTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        SimpleDateFormat date = new SimpleDateFormat("HH:mm:ss");
        date.setTimeZone(TimeZone.getDefault());
        String localTime = date.format(currentLocalTime);

        return localTime;
    }

    public static String todayWithTime() {
        return today() + " " + getTime();
    }

    public static String changeDateFormatTZ(String dateStart) {
        SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date parsedDate = null;
        try {
            parsedDate = sd1.parse(dateStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sd2 = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        String newDate = sd2.format(parsedDate);

        return newDate;
    }

    public static String changeDateFormat(Date dateStart) {
        SimpleDateFormat sd2 = new SimpleDateFormat("dd-MM-yyyy", new Locale("id, ID, ID"));
        String newDate = sd2.format(dateStart);

        return newDate;
    }

}
