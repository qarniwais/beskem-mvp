package com.mozaik.beskem.utils.dialoginterface;

import android.content.DialogInterface;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public interface DialogAction2 {
    public abstract void okClick(DialogInterface dialog);
    public abstract void okClickLong(DialogInterface dialog);

}
