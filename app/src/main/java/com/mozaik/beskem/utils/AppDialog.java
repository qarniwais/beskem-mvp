package com.mozaik.beskem.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mozaik.beskem.R;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.mozaik.beskem.utils.dialoginterface.DialogAction2;
import com.mozaik.beskem.utils.dialoginterface.DialogActionBuatKomunitas;
import com.mozaik.beskem.utils.dialoginterface.DialogActionKartuProfile;
import com.mozaik.beskem.utils.dialoginterface.DialogActionRegisterTwo;
import com.orhanobut.hawk.Hawk;


/**
 * Created by Ilham Saputra on 19/02/19.
 */
public class AppDialog{

    public static void dialogGeneral(Context context, String message, final DialogAction action) {
        if (context != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message);
            builder.setCancelable(true);

            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            action.okClick(dialog);
                        }
                    });

            AlertDialog alert = builder.create();
            if(!((Activity) context).isFinishing())
            {
                alert.show();
            }
        }

    }

    public static void showDialogOpsi(Context context, String title, String message, String textButton, final DialogAction action){
        final Dialog dialog = new Dialog(context, R.style.Theme_AppCompat_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_konfirmasi);
        dialog.setCancelable(false);

        TextView tv_header= dialog.findViewById(R.id.tv_title);
        TextView tv_notifikasi = dialog.findViewById(R.id.tv_desc);
        Button btn_ok = dialog.findViewById(R.id.btndialogconfirm);
        Button btn_cancel = dialog.findViewById(R.id.btndialogcancel);
        tv_header.setText(title);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            tv_notifikasi.setText(Html.fromHtml(message,Html.FROM_HTML_MODE_LEGACY));
        } else {
            tv_notifikasi.setText(Html.fromHtml(message));
        }
        btn_ok.setText(textButton);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action.okClick(dialog);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        if(!((Activity) context).isFinishing())
        {
            dialog.show();
        }
    }

    public static void dialogPermission(Context context, String message, final DialogAction action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "Go to Setting",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        action.okClick(dialog);
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void dialogPermintaanGabung1(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permintaan_gabung);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);


        dialog.show();
    }

    public static void dialogPermintaanGabung2(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permintaan_gabung2);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        final Boolean[] isAgree = {false};
        final ImageView ivIsAgree = dialog.findViewById(R.id.iv_isAgree);
        EditText etInputCode = dialog.findViewById(R.id.et_inputCode);
        TextInputLayout inputCode = dialog.findViewById(R.id.inputCode);
        Button btnTerima = dialog.findViewById(R.id.btn_terima);

        ivIsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgree[0]) {
                    isAgree[0] = false;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle);
                    Toast.makeText(context, "admin", Toast.LENGTH_SHORT).show();
                } else {
                    isAgree[0] = true;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle_grey);
                    Toast.makeText(context, "member", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }

    public static void dialogKartuAnggotaProfile(Context context, DialogActionKartuProfile action) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_kartu_anggota_profile);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);


        dialog.show();
    }


}
