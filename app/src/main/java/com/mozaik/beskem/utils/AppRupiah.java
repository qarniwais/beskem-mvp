package com.mozaik.beskem.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public class AppRupiah {

    public static String convert(String nilai, Boolean startRupiah){
        if (nilai.matches("")){
            nilai = "0";
        }
        NumberFormat df = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        if (startRupiah) {
            dfs.setCurrencySymbol("Rp ");
        }
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator('.');
        df.setMaximumFractionDigits(0);
        ((DecimalFormat) df).setDecimalFormatSymbols(dfs);

        String convert = df.format(Integer.parseInt(nilai)) + ",-";

        return convert;
    }
}
