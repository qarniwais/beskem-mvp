package com.mozaik.beskem.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public class AppProgressDialog {
    private static ProgressDialog dialog;

    public static void show(Context context){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading");
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void dismiss(){
        if (dialog.isShowing()){
            dialog.dismiss();
        }
    }
}
