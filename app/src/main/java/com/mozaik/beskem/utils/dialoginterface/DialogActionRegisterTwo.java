package com.mozaik.beskem.utils.dialoginterface;

import android.content.DialogInterface;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public interface DialogActionRegisterTwo {
    public abstract void okClick(DialogInterface dialog, String email, String province_code, String city_code, String district_code, String kelurahan_code);

}
