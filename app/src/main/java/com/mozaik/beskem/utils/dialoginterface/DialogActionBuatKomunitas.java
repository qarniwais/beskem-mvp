package com.mozaik.beskem.utils.dialoginterface;

import android.content.DialogInterface;

/**
 * Created by Ilham Saputra on 19/02/19.
 */
public interface DialogActionBuatKomunitas {
    public abstract void okClick(DialogInterface dialog);

}
