package com.mozaik.beskem.ui.base;


import android.content.Context;

public interface IBaseView {

    void showToastMessage(String message);

    void setProgressBar(boolean show);

    void showDialog(String message);

    Context getContext();

}
