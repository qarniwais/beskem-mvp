package com.mozaik.beskem.ui.attendance;

import android.content.Context;

import com.mozaik.beskem.data.model.Attendance;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface AttendanceContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onAttendanceSuccess(List<Attendance> message);

        void onAttendanceFailed(String message);

        void onQRSuccess(String message);

        void onQRFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getAttendance(Context context, String token, String accept, String event_code);
        void postQR(Context context, String token, String accept, String event_code, String qr_generate);

    }
}
