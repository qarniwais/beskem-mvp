package com.mozaik.beskem.ui.buatkegiatan;

import android.content.Context;

import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface BuatKegiatanContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onBuatKegiatanSuccess(String message);

        void onBuatKegiatanFailed(String message);

        void onUpdateKegiatanSuccess(String message);

        void onUpdateKegiatanFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void buatKegiatan(Context context, String token, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String event_max_member, String event_max_date,String has_rsvp);

        void updateKegiatan(Context context, String token, String accept, String event_code, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String has_rsvp, String event_max_member, String event_max_date);
    }
}
