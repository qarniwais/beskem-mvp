package com.mozaik.beskem.ui.detailnews;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.model.KomunitasPostDetail;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.MainActivity;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.buatpost.BuatPostActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailNewsActivity extends BaseActivity implements DetailNewsContract.View {

    String codeNews, titleNews, dateNews, contentLongNews, publisherNews, token, kiriman;
    @BindView(R.id.iv_news)
    ImageView ivNews;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_publisher)
    TextView tvPublisher;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_content)
    TextView tvContent;
    DataRepository dataRepository;
    DetailNewsPresenter presenter;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tv_title_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.setting)
    LinearLayout setting;
    @BindView(R.id.layoutSetting)
    RelativeLayout layoutSetting;
    KomunitasPostDetail postDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new DetailNewsPresenter(this, dataRepository);
        token = Hawk.get(Constants.TOKEN);

        final Intent intent = getIntent();
        codeNews = intent.getStringExtra("codeNews");
        titleNews = intent.getStringExtra("titleNews");
        dateNews = intent.getStringExtra("dateNews");
        contentLongNews = intent.getStringExtra("contentLongNews");
        publisherNews = intent.getStringExtra("publisherNews");
        kiriman = intent.getStringExtra("toolbar_title");

        tvTitle.setText(titleNews);
        tvPublisher.setText("Oleh " + publisherNews);
        tvDate.setText(dateNews);
        tvContent.setText(contentLongNews);
        tvTitleToolbar.setText(kiriman);
        if (Hawk.get(Constants.BUATPOST).equals(8)) {
            presenter.getDetailPost(this, "Bearer " + token, "application/json", "application/x-www-form-urlencoded", codeNews);

        } else if(Hawk.get(Constants.BUATPOST).equals(7)){
            presenter.getDetailNews(this, "Bearer " + token, codeNews);

        }
        System.out.println("codenews" + codeNews);

        if (Hawk.get(Constants.MEMBERLEVEL).equals("1")) {
            layoutSetting.setVisibility(View.VISIBLE);
        } else {
            layoutSetting.setVisibility(View.GONE);
        }

        if (Hawk.get(Constants.INFO).equals(1)) {
            layoutSetting.setVisibility(View.GONE);
        }

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPosting();
            }
        });


    }

    public void dialogPosting() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete_posting);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        TextView ubah = dialog.findViewById(R.id.tv_ubah);
        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hawk.put(Constants.INFO, 2);
                Hawk.put(Constants.POSTINGCODE, postDetail.getPostingCode());
                Intent intent1 = new Intent(DetailNewsActivity.this, BuatPostActivity.class);
                intent1.putExtra("postJudul", postDetail.getPostingJudul());
                intent1.putExtra("postContent", postDetail.getPostingContentLong());
                intent1.putExtra("postMedia", postDetail.getPostingMedia());
                startActivity(intent1);
                dialog.dismiss();
            }
        });

        TextView hapus = dialog.findViewById(R.id.tv_hapus);
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.deletePosting(DetailNewsActivity.this, "Bearer " + token, codeNews);

            }
        });

        dialog.show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogSuccess(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onGetDetailNewsSuccess(News list) {
        try {
            Glide.with(DetailNewsActivity.this)
                    .load(list.getNewsMediaLink())
                    .into(ivNews);
            System.out.println("sdsdsdsdd" + list.getNewsMediaLink());
        } catch (Exception e) {

        }


    }

    @Override
    public void onGetDetailNewsFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetDetailPostSuccess(KomunitasPostDetail list) {
        postDetail = list;
        try {
            Glide.with(DetailNewsActivity.this)
                    .load(list.getPostingMedia())
                    .into(ivNews);
        } catch (Exception e) {

        }

    }

    @Override
    public void onGetDetailPostFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeletePostingSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
//                Intent intent = new Intent(DetailNewsActivity.this, MainActivity.class);
//                startActivity(intent);
                onBackPressed();
                dialog.dismiss();
//                finish();
            }
        });
    }

    @Override
    public void onDeletePostingFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }
}
