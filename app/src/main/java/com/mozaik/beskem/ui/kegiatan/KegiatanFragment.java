package com.mozaik.beskem.ui.kegiatan;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.EventAdapter;
import com.mozaik.beskem.data.model.Event;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseView;
import com.mozaik.beskem.ui.buatkegiatan.BuatKegiatanActivity;
import com.mozaik.beskem.ui.detailkegiatan.DetailKegiatanActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class KegiatanFragment extends BaseView implements KegiatanContract.View {

    ProgressBar progressBar;
    Unbinder unbinder;
    @BindView(R.id.tv_title_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.iv_add_post)
    ImageView ivAddPost;
    LinearLayout addPost;
    @BindView(R.id.recycler_kegiatan)
    RecyclerView recyclerKegiatan;
    EventAdapter adapter;
    DataRepository dataRepository;
    KegiatanPresenter presenter;
    List<Event> eventList;

    public KegiatanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kegiatan, container, false);

        dataRepository = Injection.provideDataRepository(getActivity());
        presenter = new KegiatanPresenter(this, dataRepository);
        unbinder = ButterKnife.bind(this, view);
        progressBar = view.findViewById(R.id.progressBar);
        addPost = view.findViewById(R.id.add_post);
        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hawk.put(Constants.INFO,6);
                Intent intent = new Intent(getActivity(), BuatKegiatanActivity.class);
                startActivity(intent);
            }
        });
        if (Hawk.get(Constants.MEMBERLEVEL).equals("1")) {
            addPost.setVisibility(View.VISIBLE);
        } else {
            addPost.setVisibility(View.GONE);
        }

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerKegiatan.setLayoutManager(layoutManager);
        recyclerKegiatan.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerKegiatan, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Event event = eventList.get(position);
                Hawk.put(Constants.EVENTCODE, event.getEventCode());
                Intent intent = new Intent(getActivity(), DetailKegiatanActivity.class);
                intent.putExtra("eventCode", event.getEventCode());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        presenter.getEvent(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getEvent(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString());

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(getActivity(), t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onEventSuccess(List<Event> list) {
        eventList = list;
        try {
            adapter = new EventAdapter(getActivity(), list);
            recyclerKegiatan.setAdapter(adapter);
        } catch (Exception e) {

        }

    }

    @Override
    public void onEventFailed(String message) {
        if (getActivity() != null) {
            AppDialog.dialogGeneral(getActivity(), message, new DialogAction() {
                @Override
                public void okClick(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
        }
    }

    @Override
    public void showDialog(String message) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
