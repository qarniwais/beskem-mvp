package com.mozaik.beskem.ui.attendance;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.AttendanceAdapter;
import com.mozaik.beskem.data.model.Attendance;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttendanceActivity extends BaseActivity implements AttendanceContract.View {

    @BindView(R.id.recycler_attendance)
    RecyclerView recyclerAttendance;
    @BindView(R.id.btn_check_qr)
    Button btnCheckQr;
    @BindView(R.id.check)
    LinearLayout check;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    AttendanceAdapter adapter;
    AttendancePresenter presenter;
    DataRepository dataRepository;
    private CodeScanner mCodeScanner;
    CodeScannerView scannerView;
    static final int CAMERA_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new AttendancePresenter(this, dataRepository);
        presenter.getAttendance(this, "Bearer " + Hawk.get(Constants.TOKEN),"application/json",Hawk.get(Constants.EVENTCODE).toString());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerAttendance.setLayoutManager(layoutManager);
        scannerView = findViewById(R.id.scanner_view);
        btnCheckQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannerView.setVisibility(View.VISIBLE);
                mCodeScanner = new CodeScanner(AttendanceActivity.this, scannerView);
                mCodeScanner.startPreview();
                mCodeScanner.setDecodeCallback(new DecodeCallback() {
                    @Override
                    public void onDecoded(@NonNull final Result result) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(AttendanceActivity.this, result.getText(), Toast.LENGTH_LONG).show();
                                presenter.postQR(AttendanceActivity.this,"Bearer " + Hawk.get(Constants.TOKEN), "application/json",Hawk.get(Constants.EVENTCODE).toString(),result.getText() );

                            }
                        });
                    }
                });
                scannerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mCodeScanner.startPreview();
                    }
                });
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.CAMERA}, 1);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Start your camera handling here
                } else {
                    Toast.makeText(this, "gagal", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {

    }

    @Override
    public void onAttendanceSuccess(List<Attendance> message) {
        adapter = new AttendanceAdapter(AttendanceActivity.this, message);
        recyclerAttendance.setAdapter(adapter);
        System.out.println("sdsdsdsd" + message.get(0).getUserImg());
    }

    @Override
    public void onAttendanceFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onQRSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void onQRFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
