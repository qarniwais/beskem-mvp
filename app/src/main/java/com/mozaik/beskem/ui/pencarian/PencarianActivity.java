package com.mozaik.beskem.ui.pencarian;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.PencarianAdapter;
import com.mozaik.beskem.data.model.Pencarian;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.detailkomunitas.DetailKomunitasActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PencarianActivity extends BaseActivity implements PencarianContract.IPencarianView {

    DataRepository dataRepository;
    PencarianPresenter presenter;
    String token;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.recycler_pencarian)
    RecyclerView recyclerPencarian;
    PencarianAdapter pencarianAdapter;
    List<Pencarian> pencarians = new ArrayList<Pencarian>();
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencarian);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new PencarianPresenter(this, dataRepository);
        token = Hawk.get(Constants.TOKEN);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerPencarian.setLayoutManager(layoutManager);
        recyclerPencarian.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerPencarian, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Pencarian pencarian = pencarians.get(position);
                Intent intent = new Intent(PencarianActivity.this, DetailKomunitasActivity.class);
                intent.putExtra("communityCode", pencarian.getCommunityCode());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                presenter.getSearchKomunitas(PencarianActivity.this, "Bearer " + token, s, "5", "0");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() > 0) {
                    presenter.getSearchKomunitas(PencarianActivity.this, "Bearer " + token, s, "5", "0");
                } else {
                    recyclerPencarian.setVisibility(View.GONE);
                }
                return false;
            }
        });


    }

    @Override
    public void setPresenter(PencarianContract.IPencarianPresenter presenter) {

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onPencarianSuccess(List<Pencarian> response) {
        pencarians = response;
        if (response.size() <= 0) {
            recyclerPencarian.setVisibility(View.GONE);
            AppDialog.dialogGeneral(this, "Data pencarian tidak ada", new DialogAction() {
                @Override
                public void okClick(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
        } else {
            recyclerPencarian.setVisibility(View.VISIBLE);
            pencarianAdapter = new PencarianAdapter(this, response);
            recyclerPencarian.setAdapter(pencarianAdapter);
            System.out.println("sdsdsdsd" + response.get(0).getCommunityName());
        }

    }

    @Override
    public void onPencarianFailed(String response) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
