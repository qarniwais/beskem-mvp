package com.mozaik.beskem.ui.detailnews;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.KomunitasPostDetail;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.ui.base.BasePresenter;


public class DetailNewsPresenter extends BasePresenter<DetailNewsContract.View> implements DetailNewsContract.Presenter {

    private DataRepository dataRepository;

    public DetailNewsPresenter(DetailNewsContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getDetailNews(Context context, String token, String code_news) {
        view.showLoading();
        dataRepository.getNewsDetail(context, token, code_news, new DataResponse.NewsDetailCallback() {
            @Override
            public void onSuccess(News news) {
                view.hideLoading();
                view.onGetDetailNewsSuccess(news);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onGetDetailNewsFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void getDetailPost(Context context, String token,String accept,String contentType, String post_code) {
        view.showLoading();
        dataRepository.getPostDetail(context, token,accept,contentType, post_code, new DataResponse.PostDetailCallback() {
            @Override
            public void onSuccess(KomunitasPostDetail list) {
                view.hideLoading();
                view.onGetDetailPostSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onGetDetailPostFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }

    @Override
    public void deletePosting(Context context, String token, String posting_code) {
        view.showLoading();
        dataRepository.deletePosting(context, token, posting_code, new DataResponse.DeletePostingCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onDeletePostingSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onDeletePostingFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }
}
