package com.mozaik.beskem.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.mozaik.beskem.R;
import com.mozaik.beskem.ui.anggota.AnggotaFragment;

import com.mozaik.beskem.ui.dashboardkomunitas.DashboardFragment;
import com.mozaik.beskem.ui.iuran.IuranFragment;
import com.mozaik.beskem.ui.kegiatan.KegiatanFragment;
import com.mozaik.beskem.utils.Constants;
import com.orhanobut.hawk.Hawk;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener listener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()) {
                case R.id.navigation_beranda:
                    fragment = new DashboardFragment();
                    loadFragment(fragment, "DASHBOARD", 0);
                    return true;
                case R.id.navigation_anggota:
                    fragment = new AnggotaFragment();
                    loadFragment(fragment, "ANGGOTA", 1);
                    return true;
                case R.id.navigation_kegiatan:
                    fragment = new KegiatanFragment();
                    loadFragment(fragment, "KEGIATAN", 2);
                    return true;
                case R.id.navigation_iuran:
                    fragment = new IuranFragment();
                    loadFragment(fragment, "IURAN", 3);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Hawk.init(this).build();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(listener);

        navigation.setSelectedItemId(R.id.navigation_beranda);


    }

    private void loadFragment(Fragment fragment, String title, int icon) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();

    }

    @Override
    public void onBackPressed() {
        if (getSelectedItem(navigation) != R.id.navigation_beranda) {
            navigation.setSelectedItemId(R.id.navigation_beranda);
        } else {
            super.onBackPressed();
        }
    }

    private int getSelectedItem(BottomNavigationView bottomNavigationView) {
        Menu menu = bottomNavigationView.getMenu();
        for (int i = 0; i < bottomNavigationView.getMenu().size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.isChecked()) {
                return menuItem.getItemId();
            }
        }
        return R.id.navigation_beranda;
    }


    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
////        if (id == R.id.action_settings) {
////            return true;
////        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
