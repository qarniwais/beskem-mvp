package com.mozaik.beskem.ui.anggota;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.AdminAdapter;
import com.mozaik.beskem.data.adapter.MemberAdapter;
import com.mozaik.beskem.data.model.KomunitasMember;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseView;
import com.mozaik.beskem.ui.permintaangabung.PermintaanGabungActivity;
import com.mozaik.beskem.ui.tambahanggota.TambahAnggotaActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnggotaFragment extends BaseView implements AnggotaContract.View {

    RecyclerView recyclerAdmin;
    RecyclerView recyclerMember;
    Unbinder unbinder;
    AdminAdapter adapter;
    MemberAdapter memberAdapter;
    AnggotaPresenter presenter;
    ProgressBar progressBar;
    @BindView(R.id.iv_add_post)
    ImageView ivAddPost;
    @BindView(R.id.add_post)
    LinearLayout addPost;
    @BindView(R.id.iv_add_member)
    ImageView ivAddMember;
    @BindView(R.id.iv_add_memberAdmin)
    ImageView ivAddMemberAdmin;
    @BindView(R.id.add_memberAdmin)
    LinearLayout addMemberAdmin;
    @BindView(R.id.tandaTanya)
    RelativeLayout tandaTanya;
    @BindView(R.id.add_member)
    LinearLayout addMember;
    @BindView(R.id.memberAdd)
    RelativeLayout memberAdd;

    private List<KomunitasMember> members;


    public AnggotaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_anggota, container, false);
        unbinder = ButterKnife.bind(this, view);
        Hawk.init(getActivity()).build();
        recyclerAdmin = view.findViewById(R.id.recycler_admin);
        recyclerMember = view.findViewById(R.id.recycler_member);
        progressBar = view.findViewById(R.id.progressBar);
        DataRepository dataRepository = Injection.provideDataRepository(getActivity());
        presenter = new AnggotaPresenter(this, dataRepository);
        presenter.memberProcess(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN).toString(), Hawk.get(Constants.COMMUNITYCODE).toString(), "", "");
        if (Hawk.get(Constants.MEMBERLEVEL).equals("0")) {
            tandaTanya.setVisibility(View.GONE);
            addMember.setVisibility(View.GONE);
            memberAdd.setVisibility(View.VISIBLE);
        } else {
            memberAdd.setVisibility(View.GONE);
            tandaTanya.setVisibility(View.VISIBLE);
            addMember.setVisibility(View.VISIBLE);
        }
        LinearLayoutManager admin = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerAdmin.setLayoutManager(admin);
        recyclerAdmin.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerAdmin, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                KomunitasMember member = members.get(position);
                Toast.makeText(getActivity(), member.getName(), Toast.LENGTH_SHORT).show();
                dialogBeskem(member.getName(),member.getUserImg(),"","",member.getMemberLevel().toString(),"","");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        LinearLayoutManager member = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerMember.setLayoutManager(member);

        addMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TambahAnggotaActivity.class);
                startActivity(intent);
            }
        });
        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PermintaanGabungActivity.class);
                startActivity(intent);
            }
        });

        addMemberAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TambahAnggotaActivity.class);
                startActivity(intent);
            }
        });


        return view;
    }

    public void dialogBeskem(String namaKom, String ivProf, String prof, String name, String level, String cmt, String date) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_anggota_member);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        TextView tvNamaKomunitas = dialog.findViewById(R.id.tv_nama_komunitas);
        ImageView ivProfile = dialog.findViewById(R.id.iv_profile);
        CardView profile = dialog.findViewById(R.id.profile);
        TextView tvName = dialog.findViewById(R.id.tv_name);
        TextView tvLevel = dialog.findViewById(R.id.tv_level);
        TextView tvCmt = dialog.findViewById(R.id.tv_cmt);
        TextView tvDateJoin = dialog.findViewById(R.id.tv_date_join);


        Glide.with(this)
                .load(ivProf)
                .into(ivProfile);
        tvName.setText(namaKom);

        dialog.show();

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(getActivity(), t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onMemberSuccess(List<KomunitasMember> list) {
        members = list;
        adapter = new AdminAdapter(getActivity(), list);
        recyclerAdmin.setAdapter(adapter);
        memberAdapter = new MemberAdapter(getActivity(), list);
        recyclerMember.setAdapter(memberAdapter);
        System.out.println("jnjvnxjn" + list.get(0).getName());
    }

    @Override
    public void onMemberFailed(String message) {
        AppDialog.dialogGeneral(getActivity(), message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showDialog(String message) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
