package com.mozaik.beskem.ui.tambahanggota;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ProgressBar;

import com.mozaik.beskem.R;
import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.cariuser.CariUserActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TambahAnggotaActivity extends BaseActivity implements TambahAnggotaContract.View {

    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_anggota);
        ButterKnife.bind(this);

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TambahAnggotaActivity.this, CariUserActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {

    }


}
