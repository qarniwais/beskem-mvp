package com.mozaik.beskem.ui.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.KartuAnggotaAdapter;
import com.mozaik.beskem.data.local.AppDbHelper;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.Profile;
import com.mozaik.beskem.data.model.ProfileMember;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.remote.RemoteDataSource;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.profile.pengaturan.PengaturanActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.NetworkHelper;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity implements ProfileContract.View {

    TabLayout tabViewpager;
    ViewPager viewPager;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.iv_setting)
    ImageView ivSetting;
    @BindView(R.id.tv_saldo)
    TextView tvSaldo;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;

    ProfilePresenter presenter;
    AppDbHelper appDbHelper;

    @BindView(R.id.isi_saldo)
    LinearLayout isiSaldo;
    @BindView(R.id.transaksi)
    LinearLayout transaksi;
    @BindView(R.id.riwayat)
    LinearLayout riwayat;
    KartuAnggotaAdapter adapter;

    String name, phone, email, provinsi, kota, kecamatan, kelurahan, image;
    @BindView(R.id.recycler_kartu_anggota)
    RecyclerView recyclerKartuAnggota;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    Profile profile;
    ProfileMember profileMember;
    List<Komunitas> komunitas;
    String qr_code, memberLevel, dateJoin;
    @BindView(R.id.notif)
    LinearLayout notif;
    @BindView(R.id.tvtab1)
    TextView tvtab1;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        Hawk.init(this).build();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        appDbHelper = new AppDbHelper(this);


        List<Provinsi> list = appDbHelper.getAllProvinsi();

        System.out.println("List Size " + list.size());
        System.out.println("List Nama Test " + list.get(0).getName());

        final Provinsi prov = appDbHelper.getProvinsiById("P23");
        System.out.println("Single Nama Test " + prov.getName());

        RemoteDataSource remoteDataSource = new RemoteDataSource(this);
        DataRepository dataRepository = new DataRepository(remoteDataSource, NetworkHelper.getInstance());
        presenter = new ProfilePresenter(this, dataRepository);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.profileProcess(ProfileActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString());
            }
        }, 100);


        presenter.getKomunitas(this, "Bearer " + Hawk.get(Constants.TOKEN).toString());


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerKartuAnggota.setLayoutManager(layoutManager);
        recyclerKartuAnggota.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerKartuAnggota, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Komunitas komunitas1 = komunitas.get(position);
                Toast.makeText(ProfileActivity.this, komunitas1.getCommunityCode(), Toast.LENGTH_SHORT).show();
                Toast.makeText(ProfileActivity.this, qr_code, Toast.LENGTH_SHORT).show();
                Hawk.put(Constants.COMMUNITYCODE, komunitas1.getCommunityCode());


                System.out.println("code sdsdsdsdsd" + Hawk.get(Constants.COMMUNITYCODE));
                presenter.getQR(ProfileActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), komunitas1.getCommunityCode());

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(ProfileActivity.this, PengaturanActivity.class);
                    intent.putExtra("name", name);
                    intent.putExtra("phone", phone);
                    intent.putExtra("email", email);
                    intent.putExtra("provinsi", provinsi);
                    intent.putExtra("kota", kota);
                    intent.putExtra("kecamatan", kecamatan);
                    intent.putExtra("kelurahan", kelurahan);
                    intent.putExtra("profileImage", profile.getUserImgLink());
                    startActivity(intent);
                } catch (Exception e) {

                }

            }
        });


        isiSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProfileActivity.this, "isi saldo", Toast.LENGTH_SHORT).show();
            }
        });
        transaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProfileActivity.this, "transaksi", Toast.LENGTH_SHORT).show();
            }
        });
        riwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProfileActivity.this, "riwayat", Toast.LENGTH_SHORT).show();
            }
        });
        swipeRefresh.setColorSchemeResources(R.color.colorBackgroudHome, R.color.colorBackgroudHome);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        presenter.getKomunitas(ProfileActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString());
                        presenter.profileProcess(ProfileActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString());
                    }
                }, 2000);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void dialogUbahPassword() {
        final Dialog dialog = new Dialog(ProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_kartu_anggota_profile);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        ImageView barcod = dialog.findViewById(R.id.iv_barcode);
        ImageView ivP = dialog.findViewById(R.id.iv_profile);
        TextView tvName = dialog.findViewById(R.id.tv_name);
        TextView tvLevel = dialog.findViewById(R.id.tv_level);
        TextView tvCmt = dialog.findViewById(R.id.tv_cmt);
        TextView tvDateJoin = dialog.findViewById(R.id.tv_date_join);
        Glide.with(getApplicationContext())
                .load(profile.getUserImgLink())
                .into(ivP);

        Glide.with(getApplicationContext())
                .load(qr_code)
                .into(barcod);
        tvName.setText(profile.getName());
        tvLevel.setText(memberLevel);
        tvDateJoin.setText(dateJoin);

        System.out.println("code vcvcvcvcvcv" + qr_code);
        System.out.println("code vcvcvcvcvcvv" + profile.getUserImgLink());

        dialog.show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onProfileSuccess(Profile user) {
        profile = user;
        name = user.getName();
        phone = user.getUserPhone();
        email = user.getEmail();
        provinsi = user.getProvinceName();
        kota = user.getCityName();
        kecamatan = user.getDistrictName();
        kelurahan = user.getKelurahanName();
        tvName.setText(user.getName());
        tvEmail.setText(user.getEmail());
        tvPhone.setText(user.getUserPhone());
        if (user.getUserImgLink() != null) {
            Glide.with(getApplicationContext())
                    .load(user.getUserImgLink())
                    .into(ivProfile);
        } else {

        }

    }

    @Override
    public void onGetKomunitasSuccess(List<Komunitas> list) {
        komunitas = list;
        adapter = new KartuAnggotaAdapter(ProfileActivity.this, list);
        recyclerKartuAnggota.setAdapter(adapter);
    }

    @Override
    public void onGetKomunitasFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetQRSuccess(ProfileMember list) {
        profileMember = list;
        System.out.println("code fdfdffsdfsffsf" + list.getQrCode());
        qr_code = list.getQrCode();
        memberLevel = list.getMemberLevelName();
        dateJoin = list.getJoinDate();
        dialogUbahPassword();
    }

    @Override
    public void onGetQRFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
