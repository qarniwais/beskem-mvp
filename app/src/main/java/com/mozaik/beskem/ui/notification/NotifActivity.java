package com.mozaik.beskem.ui.notification;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.NotifAdapter;
import com.mozaik.beskem.data.model.NotificationM;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.cariuser.CariUserActivity;
import com.mozaik.beskem.utils.Constants;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotifActivity extends BaseActivity implements NotifContract.View {

    @BindView(R.id.recycler_notif)
    RecyclerView recyclerNotif;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    DataRepository dataRepository;
    NotifPresenter presenter;
    NotifAdapter adapter;
    private List<NotificationM> notificationMS = new ArrayList<NotificationM>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new NotifPresenter(this, dataRepository);
        presenter.getNotif(this, "Bearer " + Hawk.get(Constants.TOKEN),"application/json","","");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerNotif.setLayoutManager(layoutManager);
        recyclerNotif.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerNotif, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                NotificationM notificationM = notificationMS.get(position);
                dialogHapusNotif(notificationM.getNotifikasiId());
                }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        swipeRefresh.setColorSchemeResources(R.color.colorBackgroudHome, R.color.colorBackgroudHome);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        presenter.getNotif(NotifActivity.this, "Bearer " + Hawk.get(Constants.TOKEN),"application/json","","");
                    }
                }, 2000);
            }
        });
    }

    public void dialogHapusNotif(final String id) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_member);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        Button btndialogconfirm = dialog.findViewById(R.id.btndialogconfirm);
        Button btndialogcancel = dialog.findViewById(R.id.btndialogcancel);
        TextView textView = dialog.findViewById(R.id.tv_title);
        textView.setText("Apakah anda ingin menghapus notifikasi ini?");
        btndialogconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                presenter.postNotifRead(NotifActivity.this, "Bearer " + Hawk.get(Constants.TOKEN),"application/json",id);

                dialog.dismiss();
            }
        });

        btndialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onGetNotifSuccess(List<NotificationM> notif) {
        notificationMS = notif;
        adapter = new NotifAdapter(this, notif);
        recyclerNotif.setAdapter(adapter);
    }

    @Override
    public void onGetNotifFailed(String message) {

    }

    @Override
    public void onPostNotifReadSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPostNotifReadFailed(String message) {

    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
