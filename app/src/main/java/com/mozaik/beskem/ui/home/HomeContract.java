package com.mozaik.beskem.ui.home;

import android.content.Context;

import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.ModelData;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.NotifCount;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

/**
 * Created by Ilham Saputra on 11/03/19.
 */
public interface HomeContract {

    interface View extends IBaseView {

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onDashboardSuccess(List<ModelData> response);
        void onRegisterTwoSuccess(String response);

        void onRegisterTwoFailed(String response);

        void onProvinsiSuccess(List<Provinsi> list);

        void onProvinsiFailed(String message);

        void onCheckRegisterSuccess(CheckRegister list);

        void onCheckRegisterFailed(String message);

        void onGetNewsSuccess(List<News> list);

        void onGetNewsFailed(String message);

        void onGetKomunitasSuccess(List<Komunitas> list);

        void onGetKomunitasFailed(String message);

        void onTokenSuccess(String message);

        void onTokenFailed(String message);

        void onNotifCountSuccess(NotifCount notif);

        void onNotifCountFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getCommunityData();
        void registerTwoProcess(Context context, String email, String province_code, String city_code, String district_code, String kelurahan_code);
        void getProvinsi(Context context);
        void checkRegister(Context context, String email);
        void getNews(Context context, String token);
        void getKomunitas(Context context, String token);
        void cekToken(Context context, String token, String accept);
        void getNotifCount(Context context, String token, String accept);
    }
}
