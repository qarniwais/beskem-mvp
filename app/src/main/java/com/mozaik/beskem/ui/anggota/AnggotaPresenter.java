package com.mozaik.beskem.ui.anggota;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.KomunitasMember;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class AnggotaPresenter extends BasePresenter<AnggotaContract.View> implements AnggotaContract.Presenter {

    DataRepository dataRepository;

    public AnggotaPresenter(AnggotaContract.View view, DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.view = view;
    }

    @Override
    public void memberProcess(Context context, String token, String community_code, String limit, String offset) {
        view.showLoading();
        dataRepository.getMember(context, token, community_code, limit, offset, new DataResponse.MemberCallback() {
            @Override
            public void onSuccess(List<KomunitasMember> list) {
                view.hideLoading();
                view.onMemberSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onMemberFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }
}
