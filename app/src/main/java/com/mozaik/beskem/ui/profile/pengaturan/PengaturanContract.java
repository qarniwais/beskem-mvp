package com.mozaik.beskem.ui.profile.pengaturan;

import android.content.Context;

import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

public interface PengaturanContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onLogoutSuccess(String message);

        void onUpdateProfileSuccess(String message);

        void onUpdateProfileFailed(String message);

        void onUpdatePasswordSuccess(String message);

        void onUpdatePasswordFailed(String message);

//        void onForgotPasswordSuccess(String message);
//
//        void onForgotPasswordFiled(String message);


    }

    interface Presenter extends IBasePresenter<View> {
        void logoutProcess(Context context, String accept, String token);
        void updateProfileProcess(Context context, String token, String name, String user_phone, String province_code, String city_code, String district_code, String kelurahan_code, String kode_pos, String user_img, String email);
        void updatePassword(Context context,String token, String oldPassword, String newPassword);
//        void forgotPasswordProscess(Context context, String email);
    }
}
