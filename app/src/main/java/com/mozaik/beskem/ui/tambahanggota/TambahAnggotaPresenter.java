package com.mozaik.beskem.ui.tambahanggota;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.ui.base.BasePresenter;
import com.mozaik.beskem.ui.base.IBasePresenter;

import java.util.List;

public class TambahAnggotaPresenter extends BasePresenter<TambahAnggotaContract.View> implements TambahAnggotaContract.Presenter {

    private DataRepository dataRepository;

    public TambahAnggotaPresenter(TambahAnggotaContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }



}
