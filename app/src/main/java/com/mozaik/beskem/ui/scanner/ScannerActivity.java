package com.mozaik.beskem.ui.scanner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mozaik.beskem.R;

public class ScannerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
    }
}
