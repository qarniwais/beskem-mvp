package com.mozaik.beskem.ui.pencarian;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.Pencarian;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class PencarianPresenter extends BasePresenter<PencarianContract.IPencarianView> implements PencarianContract.IPencarianPresenter {

    private DataRepository dataRepository;

    public PencarianPresenter(PencarianContract.IPencarianView view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getSearchKomunitas(Context context, String token, String name, String limit, String offset) {
        view.setProgressBar(true);
        dataRepository.searchKomunitas(context, token, name, limit, offset, new DataResponse.SearchKomunitasCallback(){
            @Override
            public void onSuccess(List<Pencarian> list) {
                view.setProgressBar(false);
                view.onPencarianSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onPencarianFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }
}
