package com.mozaik.beskem.ui.iuran;


import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface IuranContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onIuranSuccess(List<String> response);

    }

    interface Presenter extends IBasePresenter<View> {

    }
}
