package com.mozaik.beskem.ui.buatkomunitas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.model.KomunitasKategori;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.home.HomeActivity;
import com.mozaik.beskem.utils.AppDate;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuatKomunitasActivity extends BaseActivity implements BuatKomunitasContract.View {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_CAPTURE2 = 2;
    Calendar calendar = Calendar.getInstance();
    @BindView(R.id.btn_buat_komunitas)
    Button btnBuatKomunitas;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_inputNama)
    EditText etInputNama;
    @BindView(R.id.inputNama)
    TextInputLayout inputNama;
    @BindView(R.id.et_inputDeskripsi)
    EditText etInputDeskripsi;
    @BindView(R.id.inputDeskripsi)
    TextInputLayout inputDeskripsi;
    @BindView(R.id.et_inputWebsite)
    EditText etInputWebsite;
    @BindView(R.id.inputWebsite)
    TextInputLayout inputWebsite;
    @BindView(R.id.et_inputTglberdiri)
    EditText etInputTglberdiri;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.pertanyaan)
    LinearLayout pertanyaan;
    @BindView(R.id.ivIsAgree)
    ImageView ivIsAgree;
    @BindView(R.id.iv_calender)
    ImageView ivCalender;
    Boolean isAgree = false;
    BuatKomunitasPresenter presenter;
    DataRepository dataRepository;
    @BindView(R.id.foto)
    ImageView foto;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    String imageString;
    String user_image;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.spinnerCategory)
    Spinner spinnerCategory;
    @BindView(R.id.et_inputQuestion)
    EditText etInputQuestion;
    @BindView(R.id.inputQuestion)
    TextInputLayout inputQuestion;
    String name, question, desc, date, web, code, coverU, iconU;
    @BindView(R.id.buat)
    LinearLayout buat;
    @BindView(R.id.btn_update_komunitas)
    Button btnUpdateKomunitas;
    @BindView(R.id.update)
    LinearLayout update;
    @BindView(R.id.dateBerdiri)
    LinearLayout dateBerdiri;
    private DatePickerDialog datePickerDialog;
    String kategori;
    String mName, mDeskripsi, mWeb, mDate;

    List<KomunitasKategori> kategoris = new ArrayList<KomunitasKategori>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                //encode
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                ivCover.setImageBitmap(decodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        } else if (resultCode == RESULT_OK && requestCode == 2) {
            try {
                final Uri imageUri2 = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri2);

                //encode
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                user_image = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                imageBytes = Base64.decode(user_image, Base64.DEFAULT);

                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                ivIcon.setImageBitmap(decodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_komunitas);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new BuatKomunitasPresenter(this, dataRepository);
        Intent intent = getIntent();
        code = intent.getStringExtra("communityCode");
        name = intent.getStringExtra("communityName");
        desc = intent.getStringExtra("communityDesc");
        web = intent.getStringExtra("communityWeb");
        date = intent.getStringExtra("communityDate");
        question = intent.getStringExtra("communityQuestion");
        coverU = intent.getStringExtra("communityCover");
        iconU = intent.getStringExtra("communityIcon");


        etInputNama.setText(name);
        etInputDeskripsi.setText(desc);
        etInputWebsite.setText(web);
        etInputTglberdiri.setText(date);
        etInputQuestion.setText(question);


        if (Hawk.get(Constants.INFO).equals(1)) {
            update.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(coverU)
                    .into(ivCover);
            Glide.with(this)
                    .load(iconU)
                    .into(ivIcon);
            buat.setVisibility(View.GONE);
            Toast.makeText(this, "update", Toast.LENGTH_SHORT).show();
        } else {
            update.setVisibility(View.GONE);
            buat.setVisibility(View.VISIBLE);
            Toast.makeText(this, "buat", Toast.LENGTH_SHORT).show();
        }


        ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE2);
            }
        });
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE2);
                if (ivIcon != null) {
                    text1.setVisibility(View.GONE);
                    icon.setVisibility(View.GONE);
                } else {
                    text1.setVisibility(View.VISIBLE);
                    icon.setVisibility(View.VISIBLE);
                }

            }
        });


        ivCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
            }
        });

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
                if (ivCover != null) {
                    text.setVisibility(View.GONE);
                    foto.setVisibility(View.GONE);
                } else {
                    text.setVisibility(View.VISIBLE);
                    foto.setVisibility(View.VISIBLE);
                }

            }
        });

        dateBerdiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        btnBuatKomunitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                mName = etInputNama.getText().toString();
                mDeskripsi = etInputDeskripsi.getText().toString();
                mWeb = etInputWebsite.getText().toString();
                mDate = etInputTglberdiri.getText().toString();

                if (mName.equals("")){
                    etInputNama.setError("Nama Komunitas Wajib Diisi");
                }else if (mDeskripsi.equals("")){
                    etInputDeskripsi.setError("Deskripsi Wajib Diisi");
                }else if (mWeb.equals("")){
                    etInputWebsite.setError("Web Wajib Diisi");
                }else if (mDate.equals("")){
                    etInputTglberdiri.setError("Tanggal Wajib Diisi");
                }else {
                    presenter.createKomunitas(BuatKomunitasActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), imageString, user_image, mName,
                            mDeskripsi, mWeb, kategori, mDate, "", etInputQuestion.getText().toString());

                }

            }
        });
        btnUpdateKomunitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                presenter.updateKomunitas(BuatKomunitasActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), code, etInputNama.getText().toString(), etInputDeskripsi.getText().toString(), etInputWebsite.getText().toString(), etInputTglberdiri.getText().toString(), user_image,
                        imageString, "jhghg", "1", "jhjj", etInputQuestion.getText().toString());
                Toast.makeText(BuatKomunitasActivity.this, code, Toast.LENGTH_SHORT).show();
            }
        });


        ivIsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgree) {
                    isAgree = false;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle);
                    pertanyaan.setVisibility(View.VISIBLE);
                } else {
                    isAgree = true;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle_grey);
                    pertanyaan.setVisibility(View.GONE);
                }
            }
        });
        presenter.getKomunitasKategori(this, "Bearer " + Hawk.get(Constants.TOKEN));

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                kategori = kategoris.get(position).getCategoryCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    private void dialogBuatKomunitas() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_buat_komunitas);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        Button btnSave = dialog.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(BuatKomunitasActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        dialog.show();

    }

    private void showDateDialog() {
        datePickerDialog = new DatePickerDialog(BuatKomunitasActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
                etInputTglberdiri.setText(AppDate.changeDateFormat(calendar.getTime()));
                System.out.println("zfdfdzffdf" + AppDate.changeDateFormat(calendar.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    @Override
    public void showDialogError(String msg) {
        AppDialog.dialogGeneral(this, msg, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBuatKomunitasSuccess(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
                dialogBuatKomunitas();
            }
        });

    }

    @Override
    public void onBuatKomunitasFailed(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdateKomunitasSuccess(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdateKomunitasFailed(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onKomunitasKategoriSuccess(List<KomunitasKategori> list) {
        kategoris = list;
        List<String> arrayKategori = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            arrayKategori.add(list.get(i).getCategoryName());
        }

        ArrayAdapter<String> adapterKota = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, arrayKategori);
        spinnerCategory.setAdapter(adapterKota);
    }

    @Override
    public void onKomunitasKategoriFailed(String response) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }
}
