package com.mozaik.beskem.ui.attendance;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.Attendance;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class AttendancePresenter extends BasePresenter<AttendanceContract.View> implements AttendanceContract.Presenter {
    DataRepository dataRepository;

    public AttendancePresenter(AttendanceContract.View view, DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.view = view;
    }

    @Override
    public void getAttendance(Context context, String token, String accept, String event_code) {
        view.showLoading();
        dataRepository.getAttendance(context, token, accept, event_code, new DataResponse.AttendanceCallback() {
            @Override
            public void onSuccess(List<Attendance> list) {
                view.hideLoading();
                view.onAttendanceSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onAttendanceFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }

    @Override
    public void postQR(Context context, String token, String accept, String event_code, String qr_generate) {
        view.showLoading();
        dataRepository.cekQR(context, token, accept, event_code, qr_generate, new DataResponse.CheckQRCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onQRSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onQRFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }
}
