package com.mozaik.beskem.ui.register;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.model.RegisterStepOne;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.data.remote.RemoteDataSource;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.login.LoginActivity;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.NetworkHelper;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;
import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;

public class RegisterActivity extends BaseActivity implements RegisterContract.View {

    Button btnRegister;
    @BindView(R.id.et_inputPhone)
    EditText etInputPhone;
    @BindView(R.id.et_inputEmail)
    EditText etInputEmail;
    @BindView(R.id.et_inputPassword)
    EditText etInputPassword;
    @BindView(R.id.et_inputUlangPassword)
    EditText etInputUlangPassword;
    @BindView(R.id.et_inputName)
    EditText etInputName;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    TextView tv_login;
    RegisterPresenter presenter;
    String username, password, encodepass, screen_category, device_density, device_density2,
            screen_in_pixel, screen_in_inchi, osversion, apiversion, device,
            model, manufacturer, product, imei, imsi, simId, lat, lng;
    double latitude = 0, longitude = 0;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
    String mEmail, mName, mPhone, mPassword, mKonfirm_password;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        Hawk.init(this).build();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        RemoteDataSource remoteDataSource = new RemoteDataSource(this);
        DataRepository dataRepository = new DataRepository(remoteDataSource, NetworkHelper.getInstance());

//        DataRepository dataRepository = Injection.provideDataRepository(getApplicationContext());
        presenter = new RegisterPresenter(this, dataRepository);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        requestPermission();
        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmail = etInputEmail.getText().toString();
                mName = etInputName.getText().toString();
                mPhone = etInputPhone.getText().toString();
                mPassword = etInputPassword.getText().toString();
                mKonfirm_password = etInputUlangPassword.getText().toString();

                if (mName.equals("")) {
                    etInputName.setError("Nama wajib diisi");
                } else if (mEmail.equals("")) {
                    etInputEmail.setError("Email wajib diisi");
                } else if (mPhone.equals("")) {
                    etInputPhone.setError("Telepon wajib diisi");
                } else {
                    presenter.registerProcess(RegisterActivity.this, etInputEmail.getText().toString(), etInputName.getText().toString(), etInputPhone.getText().toString(), etInputPassword.getText().toString(), etInputUlangPassword.getText().toString());

                }



            }
        });


        tv_login = findViewById(R.id.tv_login);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getPersonalInfo() {

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        screen_category = getScreenCategory(getApplicationContext());
        device_density = getDensityValue();
        device_density2 = getDeviceResolution(getApplicationContext());
        screen_in_pixel = getScreenInPixel();
        screen_in_inchi = getScreenInInch();
        osversion = Build.VERSION.RELEASE;
        apiversion = String.valueOf(Build.VERSION.SDK_INT);
        device = Build.DEVICE;
        model = Build.MODEL;
        manufacturer = Build.HARDWARE;
        product = Build.PRODUCT;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imei = tm.getDeviceId();
        imsi = tm.getSubscriberId();
        simId = tm.getSimSerialNumber();

        lat = String.valueOf(latitude);
        lng = String.valueOf(longitude);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {

    }

    @Override
    public void onRegisterSuccess(RegisterStepOne response) {
        Hawk.put(Constants.USER_NAME, response.getName());
        Hawk.put(Constants.USER_EMAIL, response.getEmail());
        Hawk.put(Constants.USER_PHONE, response.getUserPhone());
        Hawk.put(Constants.PASSWORD, mPassword);
        presenter.loginProcess(getApplicationContext(),
                etInputEmail.getText().toString(), etInputPassword.getText().toString(), screen_category,
                device_density, device_density2, screen_in_pixel, screen_in_inchi, osversion,
                apiversion, device, model, manufacturer, product, imei, imsi, simId,
                lat, lng);
        Intent intent = new Intent(RegisterActivity.this, RegisterSyaratKetentuanActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginSuccess(User user) {
        Hawk.put(Constants.TOKEN, user.getToken());
        Hawk.put(Constants.USER_EMAIL, user.getEmail());
        Hawk.put(Constants.USER_CODE, user.getUserCode());
    }

    @Override
    public void onLoginFailed(String message) {

    }

    private String getDensityValue() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenDensity = metrics.densityDpi;
        return String.valueOf(screenDensity);
    }

    private String getScreenInPixel() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        String h = String.valueOf(height);
        String w = String.valueOf(width);
        return w + " x " + h;
    }

    private String getScreenInInch() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        double wi = (double) width / dm.xdpi;
        double hi = (double) height / dm.ydpi;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);
        return String.valueOf(screenInches);
    }

    private String getScreenCategory(Context context) {
        int screenLayout = context.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large";
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return "xlarge";
            default:
                return "undefined";
        }
    }

    private String getDeviceResolution(Context context) {
        int density = context.getResources().getDisplayMetrics().densityDpi;
        switch (density) {
            case DisplayMetrics.DENSITY_MEDIUM:
                return "MDPI";
            case DisplayMetrics.DENSITY_HIGH:
                return "HDPI";
            case DisplayMetrics.DENSITY_LOW:
                return "LDPI";
            case DisplayMetrics.DENSITY_XHIGH:
                return "XHDPI";
            case DisplayMetrics.DENSITY_TV:
                return "TV";
            case DisplayMetrics.DENSITY_XXHIGH:
                return "XXHDPI";
            case DisplayMetrics.DENSITY_XXXHIGH:
                return "XXXHDPI";
            default:
                return "Unknown";
        }
    }

    private void requestPermission() {
        String permissionAsk[] = {PermissionUtils.Manifest_ACCESS_FINE_LOCATION,
                PermissionUtils.Manifest_ACCESS_COARSE_LOCATION,
                PermissionUtils.Manifest_READ_PHONE_STATE};

        askCompactPermissions(permissionAsk, new PermissionResult() {
            @Override
            public void permissionGranted() {
                //permission granted
                //replace with your action
                getLocationInfo();
                getPersonalInfo();
            }

            @Override
            public void permissionDenied() {
                //permission denied
                //replace with your action
                Toast.makeText(getApplicationContext(), "Tidak Dapat Mengakses Lokasi", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void permissionForeverDenied() {
                openSettingsApp(RegisterActivity.this);
            }
        });

    }

    private void getLocationInfo() {

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            System.out.println("LATITUDE " + location.getLatitude() + "LONGITUDE " + location.getLongitude());
                            // Logic to handle location object
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                });

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null)
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        } else {
            Toast.makeText(getApplicationContext(), "Gagal mengambil lokasi, silakan coba lagi", Toast.LENGTH_SHORT).show();
        }
    }

}
