package com.mozaik.beskem.ui.home;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mozaik.beskem.R;
import com.mozaik.beskem.SessionManager;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.adapter.BeritaAdapter;
import com.mozaik.beskem.data.adapter.MenuAdapter;
import com.mozaik.beskem.data.local.AppDbHelper;
import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.Kecamatan;
import com.mozaik.beskem.data.model.Kelurahan;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.Kota;
import com.mozaik.beskem.data.model.ModelData;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.NotifCount;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.MainActivity;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.buatkomunitas.BuatKomunitasActivity;
import com.mozaik.beskem.ui.detailnews.DetailNewsActivity;
import com.mozaik.beskem.ui.login.LoginActivity;
import com.mozaik.beskem.ui.notification.NotifActivity;
import com.mozaik.beskem.ui.pencarian.PencarianActivity;
import com.mozaik.beskem.ui.profile.ProfileActivity;
import com.mozaik.beskem.ui.profile.pengaturan.PengaturanActivity;
import com.mozaik.beskem.utils.AppDate;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.mozaik.beskem.utils.dialoginterface.DialogActionRegisterTwo;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity implements HomeContract.View {

    HomePresenter presenter;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.tv_saldo)
    TextView tvSaldo;
    @BindView(R.id.iv_notif)
    ImageView ivNotif;
    @BindView(R.id.iv_notif_dot)
    ImageView ivNotifDot;
    @BindView(R.id.notif)
    RelativeLayout notif;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.komunitasku)
    TextView komunitasku;
    @BindView(R.id.recycler_komunitas)
    RecyclerView recyclerKomunitas;
    @BindView(R.id.tv_lihatSemuaBerita)
    TextView tvLihatSemuaBerita;
    @BindView(R.id.recycler_berita)
    RecyclerView recyclerBerita;
    MenuAdapter komunitasAdapter;
    BeritaAdapter beritaAdapter;
    AppDbHelper appDbHelper;
    DataRepository dataRepository;
    String email;
    Spinner spinnerProvinsi;
    Spinner spinnerKota;
    Spinner spinnerKecamatan;
    Spinner spinnerKelurahan;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.search)
    RelativeLayout search;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.profile)
    LinearLayout profile;
    private List<News> news_data = new ArrayList<>();
    private List<Komunitas> komunitas = new ArrayList<>();
    private String provinceCode = "", cityCode = "", districtCode = "", kelurahanCode = "";
    String communityC, memberC;
    String memberL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new HomePresenter(this, dataRepository);
        email = Hawk.get(Constants.USER_EMAIL);
        appDbHelper = new AppDbHelper(this);

//        presenter.getProvinsi(this);
        presenter.checkRegister(this, email);
        presenter.getNews(this, "Bearer " + Hawk.get(Constants.TOKEN));
        presenter.getKomunitas(this, "Bearer " + Hawk.get(Constants.TOKEN));
        presenter.getNotifCount(this, "Bearer " + Hawk.get(Constants.TOKEN), "application/json");
//        presenter.cekToken(this, "Bearer " + Hawk.get(Constants.TOKEN));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerBerita.setLayoutManager(layoutManager);
        recyclerBerita.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerBerita, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Hawk.put(Constants.INFO, 1);
                Hawk.put(Constants.BUATPOST, 7);
                News news = news_data.get(position);
                Intent intent = new Intent(HomeActivity.this, DetailNewsActivity.class);
                intent.putExtra("codeNews", news.getNewsCode());
                intent.putExtra("titleNews", news.getNewsTitle());
                intent.putExtra("dateNews", news.getNewsDateCreate());
                intent.putExtra("contentLongNews", news.getNewsContentLong());
                intent.putExtra("publisherNews", news.getNewsPublisher());
                intent.putExtra("toolbar_title", "DETAIL NEWS");
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        LinearLayoutManager layoutManagerApp = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerKomunitas.setLayoutManager(layoutManagerApp);
        recyclerKomunitas.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerKomunitas, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Komunitas komunitas1 = komunitas.get(position);
                communityC = komunitas1.getCommunityCode();
                memberC = komunitas1.getMemberCode();
                memberL = komunitas1.getMemberLevel();
                Toast.makeText(HomeActivity.this, memberL, Toast.LENGTH_SHORT).show();
                Hawk.put(Constants.COMMUNITYCODE, communityC);
                Hawk.put(Constants.MEMBERCODE, memberC);
                Hawk.put(Constants.MEMBERLEVEL, memberL);
                Hawk.put(Constants.NAMEKOMUNITAS, komunitas1.getCommunityName());
                Hawk.put(Constants.INFO, 0);
                if (position == (komunitas.size() - 1)) {
                    Intent intent = new Intent(HomeActivity.this, BuatKomunitasActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                    intent.putExtra("communityCode", communityC);
                    intent.putExtra("memberCode", memberC);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(intent);

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, PencarianActivity.class);
                startActivity(intent);
            }
        });
        swipeRefresh.setColorSchemeResources(R.color.colorBackgroudHome, R.color.colorBackgroudHome);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        presenter.getNews(HomeActivity.this, "Bearer " + Hawk.get(Constants.TOKEN));
                        presenter.getKomunitas(HomeActivity.this, "Bearer " + Hawk.get(Constants.TOKEN));

                    }
                }, 2000);
            }
        });

        notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, NotifActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getKomunitas(this, "Bearer " + Hawk.get(Constants.TOKEN));
        presenter.cekToken(this, "Bearer " + Hawk.get(Constants.TOKEN), "application/json");
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onDashboardSuccess(List<ModelData> response) {

    }

    @Override
    public void onRegisterTwoSuccess(String response) {
//        AppDialog.dialogGeneral(this, response, new DialogAction() {
//            @Override
//            public void okClick(DialogInterface dialog) {
//                dialog.dismiss();
//            }
//        });
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRegisterTwoFailed(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onProvinsiSuccess(List<Provinsi> list) {

    }

    @Override
    public void onProvinsiFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showDialog(String message) {
//        AppDialog.dialogGeneral(HomeActivity.this, message, new DialogAction() {
//            @Override
//            public void okClick(DialogInterface dialog) {
//                dialog.dismiss();
//            }
//        });
    }

    @Override
    public void onCheckRegisterSuccess(CheckRegister list) {
        System.out.println("ssdsdd" + list.getStatusCheck());
        if (list.getStatusCheck() == 0) {
            dialogBeskem(this, new DialogActionRegisterTwo() {
                @Override
                public void okClick(DialogInterface dialog, String email,
                                    String province_code, String city_code,
                                    String district_code, String kelurahan_code) {
                    presenter.registerTwoProcess(getApplicationContext(), email, provinceCode, cityCode, districtCode, kelurahanCode);
                }
            });
        }
    }

    @Override
    public void onCheckRegisterFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onGetNewsSuccess(List<News> list) {
        beritaAdapter = new BeritaAdapter(HomeActivity.this, list);
        recyclerBerita.setAdapter(beritaAdapter);
        news_data = list;
        System.out.println("sdsdsdsd" + list.get(0).getNewsMediaLink());
    }

    @Override
    public void onGetNewsFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onGetKomunitasSuccess(List<Komunitas> list) {
        System.out.println("size" + list.size());
        komunitas = list;
        komunitas.add(new Komunitas("", "", "", "Tambah", "", "0", "0", "0"));
        komunitasAdapter = new MenuAdapter(HomeActivity.this, list);
        recyclerKomunitas.setAdapter(komunitasAdapter);
        System.out.println("vvvvvv" + list.get(0).getCommunityName());
    }

    @Override
    public void onGetKomunitasFailed(String message) {
        System.out.println("Failed " + message);
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onTokenSuccess(String message) {
    }

    @Override
    public void onTokenFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                SessionManager session = new SessionManager(HomeActivity.this);
                session.logoutUser(HomeActivity.this);
                dialog.dismiss();

            }
        });
    }

    @Override
    public void onNotifCountSuccess(NotifCount notif) {
        if (notif.getUnreadNotification() < 1){
            ivNotifDot.setVisibility(View.GONE);
        }else {
            ivNotifDot.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNotifCountFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    public void dialogBeskem(final Context context, final DialogActionRegisterTwo actionRegisterTwo) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_awal_beskem);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        EditText etInputNama = dialog.findViewById(R.id.et_inputNama);
        EditText etInputEmail = dialog.findViewById(R.id.et_inputEmail);
        EditText etInputPhone = dialog.findViewById(R.id.et_inputPhone);
        spinnerProvinsi = dialog.findViewById(R.id.spinnerProvinsi);
        spinnerKota = dialog.findViewById(R.id.spinnerKabKota);
        spinnerKecamatan = dialog.findViewById(R.id.spinnerKecamatan);
        spinnerKelurahan = dialog.findViewById(R.id.spinnerKelurahan);

        final List<Provinsi> listProvinsi = appDbHelper.getAllProvinsi();
        List<String> arrayProvinsi = new ArrayList<>();

        for (int i = 0; i < listProvinsi.size(); i++) {
            arrayProvinsi.add(listProvinsi.get(i).getName());
        }
        ArrayAdapter<String> adapterProvinsi = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, arrayProvinsi);
        spinnerProvinsi.setAdapter(adapterProvinsi);

        spinnerProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvinsi.get(position).getId();
                getKota(provinceCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Button btnSave = dialog.findViewById(R.id.btn_save);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionRegisterTwo.okClick(dialog, email, provinceCode, cityCode, districtCode, kelurahanCode);
                System.out.println(email + "PROVINSI " + provinceCode + " KOTA " + cityCode + " KECAMATAN " + districtCode + " KELURAHAN " + kelurahanCode);
                dialog.dismiss();
            }
        });

        etInputNama.setText(Hawk.get(Constants.USER_NAME).toString());
        etInputEmail.setText(Hawk.get(Constants.USER_EMAIL).toString());
        etInputPhone.setText(Hawk.get(Constants.USER_PHONE).toString());

        dialog.show();

    }

    private void getKota(String provinceCode) {
        final List<Kota> listKota = appDbHelper.getKotaByProv(provinceCode);
        List<String> arrayKota = new ArrayList<>();
        for (int i = 0; i < listKota.size(); i++) {
            arrayKota.add(listKota.get(i).getCityName());
        }

        ArrayAdapter<String> adapterKota = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, arrayKota);
        spinnerKota.setAdapter(adapterKota);

        spinnerKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listKota.get(position).getCityCode();
                getKecamatan(cityCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getKecamatan(String cityCode) {
        dataRepository.getKecamatan(HomeActivity.this,
                cityCode, new DataResponse.KecamatanCallback() {
                    @Override
                    public void onSuccess(List<Kecamatan> list) {
                        final List<Kecamatan> listKecamatan = list;
                        List<String> arrayKecamatan = new ArrayList<>();
                        for (int i = 0; i < listKecamatan.size(); i++) {
                            arrayKecamatan.add(listKecamatan.get(i).getDistrictName());
                        }

                        ArrayAdapter<String> adapterKecamatan = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_dropdown_item, arrayKecamatan);
                        spinnerKecamatan.setAdapter(adapterKecamatan);

                        spinnerKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                districtCode = listKecamatan.get(position).getKecCode();
                                getKelurahan(districtCode);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                    @Override
                    public void onMessage(String message) {

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });

    }

    private void getKelurahan(String districtCode) {
        System.out.println("DISTRICT CODE " + districtCode);
        dataRepository.getKelurahan(HomeActivity.this, districtCode, new DataResponse.KelurahanCallback() {
            @Override
            public void onSuccess(List<Kelurahan> list) {

                final List<Kelurahan> listKelurahan = list;
                List<String> arrayKelurahan = new ArrayList<>();
                for (int i = 0; i < listKelurahan.size(); i++) {
                    arrayKelurahan.add(listKelurahan.get(i).getKelurahanName());
                }

                ArrayAdapter<String> adapterKelurahan = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_spinner_dropdown_item, arrayKelurahan);
                spinnerKelurahan.setAdapter(adapterKelurahan);

                spinnerKelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        kelurahanCode = listKelurahan.get(position).getKelurahanCode();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onMessage(String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
