package com.mozaik.beskem.ui.cariuser;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class CariUserPresenter extends BasePresenter<CariUserContract.View> implements CariUserContract.Presenter {

    private DataRepository dataRepository;

    public CariUserPresenter(CariUserContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getCariUser(Context context, String token, String community_code, String name, String limit, String offset) {
        view.setProgressBar(true);
        dataRepository.cariUser(context, token, community_code, name, limit, offset, new DataResponse.PencarianUserCallback() {
            @Override
            public void onSuccess(List<PencarianUser> list) {
                view.setProgressBar(false);
                view.onCariUserSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.showDialogError(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void addMember(Context context, String token, String accept, String community_code, String user_code) {
        view.setProgressBar(false);
        dataRepository.Addmember(context, token, accept, community_code, user_code, new DataResponse.AddMemberCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onAddMemberSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onAddMemberFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                throwable.printStackTrace();
                view.showHandling(throwable);
            }
        });
    }
}
