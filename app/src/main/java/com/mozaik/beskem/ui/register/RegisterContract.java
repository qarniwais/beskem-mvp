package com.mozaik.beskem.ui.register;

import android.content.Context;

import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.model.RegisterStepOne;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

/**
 * Created by Ilham Saputra on 11/03/19.
 */
public interface RegisterContract {

    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onRegisterSuccess(RegisterStepOne response);

        void onLoginSuccess(User user);

        void onLoginFailed(String message);



    }

    interface Presenter extends IBasePresenter<View> {

        void registerProcess(Context context, String email, String name, String user_phone, String password, String konfirmasi_password);
        void loginProcess(Context context, String username, String password, String screen_category,
                          String device_density, String device_density2, String screen_in_pixel,
                          String screen_in_inchi, String osversion, String apiversion,
                          String device, String model, String manufacturer, String product,
                          String imei, String imsi, String simId, String lat, String lng);

    }
}
