package com.mozaik.beskem.ui.permintaangabung;

import android.content.Context;

import com.mozaik.beskem.data.model.MemberRequest;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface PermintaanGabungContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onMemberRequestSuccess(List<MemberRequest> list);

        void onMemberRequestFailed(String message);

        void onMemberRequestKonfirmasiSuccess(String message);

        void onMemberRequestKonfirmasiFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getMemberRequest(Context context, String token, String communityCode);
        void postMemberRequestKonfirmasi(Context context, String token, String profile_to_community_code, String user_code,
                                         String status_approve, String member_code, String member_level);
    }
}
