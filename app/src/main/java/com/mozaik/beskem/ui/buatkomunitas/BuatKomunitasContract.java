package com.mozaik.beskem.ui.buatkomunitas;

import android.content.Context;

import com.mozaik.beskem.data.model.KomunitasKategori;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface BuatKomunitasContract {


    interface View extends IBaseView {

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onBuatKomunitasSuccess(String response);

        void onBuatKomunitasFailed(String response);

        void onUpdateKomunitasSuccess(String response);

        void onUpdateKomunitasFailed(String response);

        void onKomunitasKategoriSuccess(List<KomunitasKategori> list);

        void onKomunitasKategoriFailed(String response);
    }

    interface Presenter extends IBasePresenter<View> {
        void createKomunitas(Context context, String token, String cover, String icon, String name, String desc,
                             String website, String categoryCode, String tanggalBerdiri,
                             String hasQuestion, String questionTeks);


        void updateKomunitas(Context context, String token, String communityCode, String name, String desc,
                             String website, String tanggalBerdiri, String icon, String cover, String categoryCode,
                             String hasQuestion, String questionCode, String questionTeks);

        void getKomunitasKategori(Context context, String token);
    }
}
