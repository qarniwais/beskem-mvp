package com.mozaik.beskem.ui.detailkegiatan;

import android.content.Context;

import com.mozaik.beskem.data.model.ChekJoin;
import com.mozaik.beskem.data.model.EventDetail;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface DetailKegiatanContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onDetailKegiatanSuccess(EventDetail response);

        void onDetailKegiatanFailed(String response);

        void onHapusKegiatanSuccess(String response);

        void onHapusKegiatanFailed(String response);

        void onJoinKegiatanSuccess(String response);

        void onJoinKegiatanFailed(String response);

        void onCancelKegiatanSuccess(String response);

        void onCancelKegiatanFailed(String response);

        void onCheckKegiatanSuccess(ChekJoin response);

        void onCheckKegiatanFailed(String response);

    }

    interface Presenter extends IBasePresenter<View> {
        void getDetailKegiatan(Context context, String token, String accept, String event_code);

        void hapusKegiatan(Context context, String token, String accept, String event_code);

        void joinKegiatan(Context context, String token, String accept, String member_code, String event_code, String user_code);

        void cancelKegiatan(Context context, String token, String accept, String event_code, String user_code, String member_code);

        void checkKegiatan(Context context, String token, String accept, String event_code, String member_code, String user_code);
    }
}
