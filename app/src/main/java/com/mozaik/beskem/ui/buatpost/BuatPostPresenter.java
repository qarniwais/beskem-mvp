package com.mozaik.beskem.ui.buatpost;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.ui.base.BasePresenter;

public class BuatPostPresenter extends BasePresenter<BuatPostContract.View> implements BuatPostContract.Presenter {

    DataRepository dataRepository;

    public BuatPostPresenter(BuatPostContract.View view, DataRepository dataRepository){
        this.dataRepository = dataRepository;
        this.view = view;
    }

    @Override
    public void inputPost(Context context, String token, String community_code, String posting_content, String posting_media, String posting_judul) {
        view.showLoading();
        dataRepository.inputPost(context, token, community_code, posting_content, posting_media, posting_judul, new DataResponse.InputPostCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onInputPostSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onInputPostFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void updatePost(Context context, String token, String posting_code, String posting_judul, String posting_content, String posting_media) {
        view.showLoading();
        dataRepository.updatePost(context, token, posting_code, posting_judul, posting_content, posting_media, new DataResponse.UpdatePostCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onUpdatePostSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onUpdatePostFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }
}
