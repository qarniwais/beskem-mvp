package com.mozaik.beskem.ui.buatpost;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.MainActivity;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuatPostActivity extends BaseActivity implements BuatPostContract.View {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    @BindView(R.id.et_inputJudul)
    EditText etInputJudul;
    @BindView(R.id.et_inputDeskripsi)
    EditText etInputDeskripsi;
    @BindView(R.id.btn_buat_post)
    Button btnBuatPost;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    DataRepository dataRepository;
    BuatPostPresenter presenter;
    String imageString, user_image, judul, content;
    @BindView(R.id.layoutImage)
    LinearLayout layoutImage;
    @BindView(R.id.btn_update_post)
    Button btnUpdatePost;
    @BindView(R.id.layoutupdatepost)
    LinearLayout layoutupdatepost;
    @BindView(R.id.layoutbuatpost)
    LinearLayout layoutbuatpost;
    String mJudul, mDeskripsi;

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                //encode
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                ivIcon.setImageBitmap(decodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_post);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Hawk.init(this).build();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        etInputJudul.setText(intent.getStringExtra("postJudul"));
        etInputDeskripsi.setText(intent.getStringExtra("postContent"));
        dataRepository = Injection.provideDataRepository(this);
        presenter = new BuatPostPresenter(this, dataRepository);

        layoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
                if (ivIcon != null) {
                    layoutImage.setVisibility(View.GONE);
                } else {
                    layoutImage.setVisibility(View.VISIBLE);
                }
            }
        });
        if (Hawk.get(Constants.BUATPOST).equals(9)) {
            layoutbuatpost.setVisibility(View.VISIBLE);
            layoutupdatepost.setVisibility(View.GONE);
        } else if (Hawk.get(Constants.BUATPOST).equals(8)) {
            layoutupdatepost.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(intent.getStringExtra("postMedia"))
                    .into(ivIcon);
            layoutbuatpost.setVisibility(View.GONE);
        }

        ivIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
            }
        });

        btnBuatPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                mJudul = etInputJudul.getText().toString();
                mDeskripsi = etInputDeskripsi.getText().toString();

                if (mJudul.equals("")) {
                    etInputJudul.setError("Judul berita wajib diisi");
                } else if (mDeskripsi.equals("")) {
                    etInputDeskripsi.setError("Deskripsi wajib diisi");
                } else {
                    presenter.inputPost(BuatPostActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString(), mDeskripsi, imageString, mJudul);

                }
            }
        });

        btnUpdatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                presenter.updatePost(BuatPostActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.POSTINGCODE).toString(), etInputJudul.getText().toString(), etInputDeskripsi.getText().toString(), imageString);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onInputPostSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
                Intent intent = new Intent(BuatPostActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onInputPostFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdatePostSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {

                onBackPressed();
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdatePostFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }
}
