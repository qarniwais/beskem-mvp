package com.mozaik.beskem.ui.buatkegiatan;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.utils.AppDate;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuatKegiatanActivity extends BaseActivity implements BuatKegiatanContract.View {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Calendar calendar = Calendar.getInstance();
    @BindView(R.id.et_inputAcara)
    EditText etInputAcara;
    @BindView(R.id.inputAcara)
    TextInputLayout inputAcara;
    @BindView(R.id.et_inputDeskripsi)
    EditText etInputDeskripsi;
    @BindView(R.id.inputDeskripsi)
    TextInputLayout inputDeskripsi;
    @BindView(R.id.et_inputWaktumulai)
    EditText etInputWaktumulai;
    @BindView(R.id.inputWaktumulai)
    TextInputLayout inputWaktumulai;
    @BindView(R.id.et_inputWaktuberakhir)
    EditText etInputWaktuberakhir;
    @BindView(R.id.inputWaktuberakhir)
    TextInputLayout inputWaktuberakhir;
    @BindView(R.id.et_inputLokasi)
    EditText etInputLokasi;
    @BindView(R.id.inputLokasi)
    TextInputLayout inputLokasi;
    @BindView(R.id.rsvp)
    TextView rsvp;
    @BindView(R.id.et_inputDeadline)
    EditText etInputDeadline;
    @BindView(R.id.inputDeadline)
    TextInputLayout inputDeadline;
    @BindView(R.id.et_inputLimit)
    EditText etInputLimit;
    @BindView(R.id.inputLimit)
    TextInputLayout inputLimit;
    @BindView(R.id.layout_hide_rsvp)
    LinearLayout layoutHideRsvp;
    @BindView(R.id.btn_buat)
    Button btnBuat;
    Boolean isAgree = false;
    @BindView(R.id.iv_isAgree)
    ImageView ivIsAgree;
    @BindView(R.id.et_inputTanggalmulai)
    EditText etInputTanggalmulai;
    @BindView(R.id.iv_calenderDatemulai)
    ImageView ivCalenderDatemulai;
    @BindView(R.id.iv_calenderTimemulai)
    ImageView ivCalenderTimemulai;
    @BindView(R.id.et_inputTanggalberakhir)
    EditText etInputTanggalberakhir;
    @BindView(R.id.iv_calenderDateberakhir)
    ImageView ivCalenderDateberakhir;
    @BindView(R.id.iv_calenderTimeberakhir)
    ImageView ivCalenderTimeberakhir;
    @BindView(R.id.iv_calenderDeadline)
    ImageView ivCalenderDeadline;
    @BindView(R.id.cover)
    ImageView cover;
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.layoutImage)
    LinearLayout layoutImage;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    String imageString;
    Fragment fragment;
    BuatKegiatanPresenter presenter;
    DataRepository dataRepository;
    String mNama, mDeskripsi, mDateStart, mDateEnd, mTimeStart, mTimeEnd, mLokasi, mDeadline, mLimit;
    String amPM;
    Integer mRSVP = 1;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.update)
    LinearLayout update;
    @BindView(R.id.buat)
    LinearLayout buat;
    String event_code, community_code, member_code, event_name, event_description, event_image, event_date_start, event_date_end, event_time_start, event_time_end, location_latitude, location_longitude, location_convertion, has_rsvp, event_max_member, event_max_date;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                //encode
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                byte[] imageBytes = baos.toByteArray();
                imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                ivCover.setImageBitmap(decodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_kegiatan);
        ButterKnife.bind(this);
        Hawk.init(this).build();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        dataRepository = Injection.provideDataRepository(this);
        presenter = new BuatKegiatanPresenter(this, dataRepository);
        Intent intent = getIntent();
        event_code = intent.getStringExtra("eventCode");
        community_code = intent.getStringExtra("communityCode");
        member_code = intent.getStringExtra("memberCode");
        event_name = intent.getStringExtra("eventName");
        event_description = intent.getStringExtra("eventDeskrip");
        event_image = intent.getStringExtra("eventImage");
        event_date_start = intent.getStringExtra("eventDateStart");
        event_date_end = intent.getStringExtra("eventDateEnd");
        event_time_start = intent.getStringExtra("eventTimeStart");
        event_time_end = intent.getStringExtra("eventTimeEnd");
        location_latitude = intent.getStringExtra("eventLatitude");
        location_longitude = intent.getStringExtra("eventLongtitude");
        location_convertion = intent.getStringExtra("eventLocation");
        has_rsvp = intent.getStringExtra("eventRSVP");
        event_max_member = intent.getStringExtra("eventMaxMember");
        event_max_date = intent.getStringExtra("eventMaxDate");
        ivIsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgree) {
                    isAgree = false;
                    mRSVP = 1;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle);
                    layoutHideRsvp.setVisibility(View.VISIBLE);
                } else {
                    isAgree = true;
                    mRSVP = 0;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle_grey);
                    layoutHideRsvp.setVisibility(View.GONE);
                }
            }
        });
        Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        if (Hawk.get(Constants.INFO).equals(5)) {
            update.setVisibility(View.VISIBLE);
            buat.setVisibility(View.GONE);
            etInputAcara.setText(event_name);
            etInputDeskripsi.setText(event_description);
            etInputTanggalmulai.setText(event_date_start);
            etInputTanggalberakhir.setText(event_date_end);
            etInputWaktumulai.setText(event_time_start);
            etInputWaktuberakhir.setText(event_time_end);
            etInputLokasi.setText(location_convertion);
            etInputDeadline.setText(event_max_date);
            etInputLimit.setText(event_max_member);
            Glide.with(this)
                    .load(event_image)
                    .into(ivCover);
        } else if (Hawk.get(Constants.INFO).equals(6)) {
            update.setVisibility(View.GONE);
            buat.setVisibility(View.VISIBLE);
        }
        ivCalenderDatemulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialogStart();
            }
        });

        ivCalenderTimemulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialogStart();
            }
        });

        ivCalenderDateberakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialogEnd();
            }
        });
        ivCalenderTimeberakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialogEnd();
            }
        });
        ivCalenderDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialogDeadline();
            }
        });

        layoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
                if (ivCover != null) {
                    layoutImage.setVisibility(View.GONE);
                } else {
                    layoutImage.setVisibility(View.VISIBLE);
                }
            }
        });

        btnBuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                mNama = etInputAcara.getText().toString();
                mDeskripsi = etInputDeskripsi.getText().toString();
                mDateStart = etInputTanggalmulai.getText().toString();
                mDateEnd = etInputTanggalberakhir.getText().toString();
                mTimeStart = etInputWaktumulai.getText().toString();
                mTimeEnd = etInputWaktuberakhir.getText().toString();
                mLokasi = etInputLokasi.getText().toString();
                mDeadline = etInputDeadline.getText().toString();
                mLimit = etInputLimit.getText().toString();

                if (mNama.equals("")) {
                    etInputAcara.setError("Nama acara wajib diisi");
                    etInputAcara.setFocusable(true);
                } else if (mDeskripsi.equals("")) {
                    etInputDeskripsi.setError("Deskripsi wajib diisi");
                    etInputDeskripsi.setFocusable(true);
                } else if (mDateStart.equals("")) {
                    etInputTanggalmulai.setError("Tanggal mulai wajib diisi");
                    etInputTanggalmulai.setFocusable(true);
                } else if (mDateEnd.equals("")) {
                    etInputTanggalberakhir.setError("Tanggal berakhir wajib diisi");
                    etInputTanggalberakhir.setFocusable(true);
                } else if (mTimeStart.equals("")) {
                    etInputWaktumulai.setError("Waktu mulai wajib diisi");
                    etInputWaktumulai.setFocusable(true);
                } else if (mTimeEnd.equals("")) {
                    etInputWaktuberakhir.setError("Waktu berakhir wajib diisi");
                    etInputWaktuberakhir.setFocusable(true);
                } else if (mLokasi.equals("")) {
                    etInputLokasi.setError("Lokasi wajib diisi");
                    etInputLokasi.setFocusable(true);
                } else if (mRSVP == 1) {
                    if (mDeadline.equals("")) {
                        etInputDeadline.setError("Tanggal kegiatan berakhir wajib diisi");
                        etInputDeadline.setFocusable(true);
                    } else if (mLimit.equals("")) {
                        etInputLimit.setError("Limit wajib diisi");
                        etInputLimit.setFocusable(true);
                    } else {
                        presenter.buatKegiatan(BuatKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString(), Hawk.get(Constants.MEMBERCODE).toString(),
                                etInputAcara.getText().toString(), etInputDeskripsi.getText().toString(), imageString, etInputTanggalmulai.getText().toString(), etInputTanggalberakhir.getText().toString(),
                                etInputWaktumulai.getText().toString(), etInputWaktuberakhir.getText().toString(), "", "", etInputLokasi.getText().toString(),
                                etInputLimit.getText().toString(), etInputDeadline.getText().toString(), "1");
                    }

                } else {
                    presenter.buatKegiatan(BuatKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString(), Hawk.get(Constants.MEMBERCODE).toString(),
                            etInputAcara.getText().toString(), etInputDeskripsi.getText().toString(), imageString, etInputTanggalmulai.getText().toString(), etInputTanggalberakhir.getText().toString(),
                            etInputWaktumulai.getText().toString(), etInputWaktuberakhir.getText().toString(), "", "", etInputLokasi.getText().toString(),
                            etInputLimit.getText().toString(), etInputDeadline.getText().toString(), "0");
                }

                etInputAcara.setText("");
                etInputDeadline.setText("");
                etInputLimit.setText("");
                etInputLokasi.setText("");
                etInputWaktuberakhir.setText("");
                etInputTanggalberakhir.setText("");
                etInputTanggalmulai.setText("");
                etInputDeskripsi.setText("");
                etInputWaktumulai.setText("");

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNama = etInputAcara.getText().toString();
                mDeskripsi = etInputDeskripsi.getText().toString();
                mDateStart = etInputTanggalmulai.getText().toString();
                mDateEnd = etInputTanggalberakhir.getText().toString();
                mTimeStart = etInputWaktumulai.getText().toString();
                mTimeEnd = etInputWaktuberakhir.getText().toString();
                mLokasi = etInputLokasi.getText().toString();
                mDeadline = etInputDeadline.getText().toString();
                mLimit = etInputLimit.getText().toString();

                if (mNama.equals("")) {
                    etInputAcara.setError("Nama acara wajib diisi");
                    etInputAcara.setFocusable(true);
                } else if (mDeskripsi.equals("")) {
                    etInputDeskripsi.setError("Deskripsi wajib diisi");
                    etInputDeskripsi.setFocusable(true);
                } else if (mDateStart.equals("")) {
                    etInputTanggalmulai.setError("Tanggal mulai wajib diisi");
                    etInputTanggalmulai.setFocusable(true);
                } else if (mDateEnd.equals("")) {
                    etInputTanggalberakhir.setError("Tanggal berakhir wajib diisi");
                    etInputTanggalberakhir.setFocusable(true);
                } else if (mTimeStart.equals("")) {
                    etInputWaktumulai.setError("Waktu mulai wajib diisi");
                    etInputWaktumulai.setFocusable(true);
                } else if (mTimeEnd.equals("")) {
                    etInputWaktuberakhir.setError("Waktu berakhir wajib diisi");
                    etInputWaktuberakhir.setFocusable(true);
                } else if (mLokasi.equals("")) {
                    etInputLokasi.setError("Lokasi wajib diisi");
                    etInputLokasi.setFocusable(true);
                } else if (mRSVP == 1) {
                    if (mDeadline.equals("")) {
                        etInputDeadline.setError("Tanggal kegiatan berakhir wajib diisi");
                        etInputDeadline.setFocusable(true);
                    } else if (mLimit.equals("")) {
                        etInputLimit.setError("Limit wajib diisi");
                        etInputLimit.setFocusable(true);
                    } else {
                        presenter.updateKegiatan(BuatKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), "application/json", event_code, Hawk.get(Constants.COMMUNITYCODE).toString(), Hawk.get(Constants.MEMBERCODE).toString(),
                                etInputAcara.getText().toString(), etInputDeskripsi.getText().toString(), imageString, etInputTanggalmulai.getText().toString(), etInputTanggalberakhir.getText().toString(),
                                etInputWaktumulai.getText().toString(), etInputWaktuberakhir.getText().toString(), "", "", etInputLokasi.getText().toString(), "1",
                                etInputLimit.getText().toString(), etInputDeadline.getText().toString());
                    }

                } else {
                    presenter.updateKegiatan(BuatKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), "application/json", event_code, Hawk.get(Constants.COMMUNITYCODE).toString(), Hawk.get(Constants.MEMBERCODE).toString(),
                            etInputAcara.getText().toString(), etInputDeskripsi.getText().toString(), imageString, etInputTanggalmulai.getText().toString(), etInputTanggalberakhir.getText().toString(),
                            etInputWaktumulai.getText().toString(), etInputWaktuberakhir.getText().toString(), "", "", etInputLokasi.getText().toString(), "0",
                            etInputLimit.getText().toString(), etInputDeadline.getText().toString());
                }
            }
        });


    }


    private void showTimeDialogStart() {

        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                tvTimeResult.setText("Waktu dipilih = " + hourOfDay + ":" + minute);
                if (hourOfDay >= 12) {
                    amPM = "PM";
                } else {
                    amPM = "AM";
                }

                etInputWaktumulai.setText(String.format("%02d:%02d", hourOfDay, minute));
            }
        },
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                DateFormat.is24HourFormat(this));

        timePickerDialog.show();
    }

    private void showTimeDialogEnd() {

        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                tvTimeResult.setText("Waktu dipilih = " + hourOfDay + ":" + minute);
                if (hourOfDay >= 12) {
                    amPM = "PM";
                } else {
                    amPM = "AM";
                }
                etInputWaktuberakhir.setText(String.format("%02d:%02d", hourOfDay, minute));
            }
        },
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                DateFormat.is24HourFormat(this));

        timePickerDialog.show();
    }

    private void showDateDialogStart() {
        datePickerDialog = new DatePickerDialog(BuatKegiatanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
                etInputTanggalmulai.setText(AppDate.changeDateFormat(calendar.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void showDateDialogEnd() {
        datePickerDialog = new DatePickerDialog(BuatKegiatanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
                etInputTanggalberakhir.setText(AppDate.changeDateFormat(calendar.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void showDateDialogDeadline() {
        datePickerDialog = new DatePickerDialog(BuatKegiatanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
                etInputDeadline.setText(AppDate.changeDateFormat(calendar.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {
        AppDialog.dialogGeneral(this, msg, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBuatKegiatanSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
//                Intent intent = new Intent(BuatKegiatanActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
                onBackPressed();
            }
        });
    }

    @Override
    public void onBuatKegiatanFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdateKegiatanSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                onBackPressed();
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdateKegiatanFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }
}
