package com.mozaik.beskem.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mozaik.beskem.R;
import com.mozaik.beskem.SessionManager;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.local.AppDbHelper;
import com.mozaik.beskem.data.model.Kota;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.home.HomeActivity;
import com.mozaik.beskem.ui.login.LoginActivity;
import com.mozaik.beskem.utils.Constants;
import com.orhanobut.hawk.Hawk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {
    DataRepository dataRepository;
    AppDbHelper appDbHelper;
    //    @BindView(R.id.text_splash)
//    TextView textView;
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        dataRepository = Injection.provideDataRepository(this);
        appDbHelper = new AppDbHelper(this);
        Hawk.init(this).build();

        getProvinsi();


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SplashActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);
                Hawk.put(Constants.FCM_TOKEN, newToken);

            }
        });
    }

    private void getProvinsi() {
//        textView.setText("Download provinsi....");
        dataRepository.getProvinsi(this, new DataResponse.ProvinsiCallback() {
            @Override
            public void onSuccess(List<Provinsi> list) {
                appDbHelper.insertProvinsi(list);
                Hawk.put(Constants.PROVINSI, true);
                getKota();
            }

            @Override
            public void onMessage(String message) {
//                textView.setText(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.getStackTrace();
            }
        });
    }

    private void getKota() {
//        textView.setText("Download kota....");
        dataRepository.getKota(this, new DataResponse.KotaCallback() {
            @Override
            public void onSuccess(List<Kota> list) {
                appDbHelper.insertKota(list);
                Hawk.put(Constants.KOTA, true);

//                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                startActivity(intent);
//                finish();
//                getKecamatan();
                final SessionManager session = new SessionManager(SplashActivity.this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (session.isLoggedIn()) {
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }
                        finish();
                    }
                }, SPLASH_TIME_OUT);
            }

            @Override
            public void onMessage(String message) {
//                textView.setText(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.getStackTrace();

            }
        });
    }

}
