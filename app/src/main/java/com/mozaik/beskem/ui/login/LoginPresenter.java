package com.mozaik.beskem.ui.login;

import android.content.Context;
import android.util.Log;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.ui.base.BasePresenter;
import com.mozaik.beskem.utils.threading.MainUiThread;
import com.mozaik.beskem.utils.threading.ThreadExecutor;

/**
 * Created by Ilham Saputra on 08/03/19.
 */
public class LoginPresenter extends BasePresenter<LoginContract.ILoginView> implements LoginContract.ILoginPresenter {

    private DataRepository dataRepository;

    public LoginPresenter(LoginContract.ILoginView view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }


    @Override
    public void loginProcess(Context context, String username, String password,
                             String screen_category, String device_density, String device_density2,
                             String screen_in_pixel, String screen_in_inchi, String osversion,
                             String apiversion, String device, String model, String manufacturer,
                             String product, String imei, String imsi, String simId, String lat, String lng) {
        Log.i("LP", "Login Presenter");
        view.setProgressBar(true);

        dataRepository.login(context, username, password, screen_category,
                device_density, device_density2, screen_in_pixel,
                screen_in_inchi, osversion, apiversion,
                device, model, manufacturer, product,
                imei, imsi, simId, lat, lng, new DataResponse.LoginCallback() {
                    @Override
                    public void onSuccess(User user) {
                        view.setProgressBar(false);
                        view.onLoginSuccess(user);
                    }

                    @Override
                    public void onMessage(String message) {
                        System.out.println("failed msg");
                        view.onLoginFailed(message);
                        view.setProgressBar(false);
                        view.showToastMessage(message);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        System.out.println("failed aja");
                        throwable.printStackTrace();
                        view.setProgressBar(false);
                        view.showHandling(throwable);
                    }
                });

    }

    @Override
    public void forgotPasswordProscess(Context context, String email) {
        view.setProgressBar(true);
        dataRepository.forgotPassword(context, email, new DataResponse.ForgotPasswordCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onForgotPasswordSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onForgotPasswordFiled(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void updateFcm(Context context, String token, String tokenFcm, String imei) {
        dataRepository.updateFcm(context, token, tokenFcm, imei, new DataResponse.UpdateFcmCallback() {
            @Override
            public void onSuccess(String message) {
                System.out.println("fcm updated");

            }

            @Override
            public void onMessage(String message) {
                System.out.println("fcm failed");

            }

            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("fcm throwable");
                throwable.printStackTrace();

            }
        });
    }
}
