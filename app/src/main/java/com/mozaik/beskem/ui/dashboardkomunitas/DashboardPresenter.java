package com.mozaik.beskem.ui.dashboardkomunitas;

import android.content.Context;

import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.ModelData;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

public class DashboardPresenter extends BasePresenter<DashboardContract.View> implements DashboardContract.IDashboardPresenter {
    private DataRepository dataRepository;

    public DashboardPresenter(DashboardContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getDetailKomunitas(Context context, String token, String communityCode, String memberCode) {
        view.showLoading();
        dataRepository.detailKomunitas(context, token, communityCode, memberCode, new DataResponse.DetailKomunitasCallback() {
            @Override
            public void onSuccess(KomunitasDetail komunitas) {
                view.hideLoading();
                view.onDetailKegiatanSuccess(komunitas);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onDetailKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void getPost(Context context, String token, String communityCode) {
        view.showLoading();
        dataRepository.getPost(context, token, communityCode, new DataResponse.PostCallback() {
            @Override
            public void onSuccess(List<KomunitasPost> list) {
                view.hideLoading();
                view.onKomunitasPostSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onKomunitasPostFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void deleteMember(Context context, String token, String communityCode, String userCode) {
        view.showLoading();
        dataRepository.deleteMember(context, token, communityCode, userCode, new DataResponse.DeleteMemberCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onDeleteMemberSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onDeleteMemberFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }


}
