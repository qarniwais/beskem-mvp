package com.mozaik.beskem.ui.cariuser;

import android.content.Context;

import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;


import java.util.List;

public interface CariUserContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onCariUserSuccess(List<PencarianUser> response);

        void onAddMemberSuccess(String message);

        void onAddMemberFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getCariUser(Context context, String token, String community_code, String name, String limit, String offset);
        void addMember(Context context, String token, String accept, String community_code, String user_code);

    }
}
