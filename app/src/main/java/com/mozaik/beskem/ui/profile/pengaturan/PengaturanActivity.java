package com.mozaik.beskem.ui.profile.pengaturan;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.SessionManager;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.local.AppDbHelper;
import com.mozaik.beskem.data.model.Kecamatan;
import com.mozaik.beskem.data.model.Kelurahan;
import com.mozaik.beskem.data.model.Kota;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.login.LoginActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PengaturanActivity extends BaseActivity implements PengaturanContract.View {

    @BindView(R.id.btn_save)
    Button btnSave;
    Button btnUbahPassword;
    String name, phone, email, provinsi, kota, kecamatan, kelurahan;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_inputNama)
    EditText etInputNama;
    @BindView(R.id.et_inputPhone)
    EditText etInputPhone;
    @BindView(R.id.et_inputEmail)
    EditText etInputEmail;
    @BindView(R.id.spinnerProvinsi)
    Spinner spinnerProvinsi;
    @BindView(R.id.spinnerKabKota)
    Spinner spinnerKabKota;
    @BindView(R.id.spinnerKecamatan)
    Spinner spinnerKecamatan;
    AppDbHelper appDbHelper;
    DataRepository dataRepository;
    @BindView(R.id.iv_logout)
    ImageView ivLogout;
    PengaturanPresenter presenter;
    @BindView(R.id.spinnerKelurahan)
    Spinner spinnerKelurahan;
    String imageString;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    private String provinceCode = "", cityCode = "", districtCode = "", kelurahanCode = "";

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                //encode
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                ivProfile.setImageBitmap(decodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        btnUbahPassword = findViewById(R.id.btn_ubahPassword);

        btnUbahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUbahPassword();
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Hawk.init(this).build();
        appDbHelper = new AppDbHelper(this);
        dataRepository = Injection.provideDataRepository(this);
        presenter = new PengaturanPresenter(this, dataRepository);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        phone = intent.getStringExtra("phone");
        email = intent.getStringExtra("email");
        provinsi = intent.getStringExtra("provinsi");
        kota = intent.getStringExtra("kota");
        kecamatan = intent.getStringExtra("kecamatan");
        kelurahan = intent.getStringExtra("kelurahan");

        etInputNama.setText(name);
        etInputPhone.setText(phone);
        etInputEmail.setText(email);

        getProvinsi();
        //provinsi
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.select_state, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerProvinsi.setAdapter(adapter);
//        if (provinsi != null) {
//            int spinnerPosition = adapter.getPosition(provinsi);
//            spinnerProvinsi.setSelection(spinnerPosition);
//        }
//        Toast.makeText(this, provinsi, Toast.LENGTH_SHORT).show();
//        //kota
//        List<String> kot = new ArrayList<>();
//        kot.add(kota);
//        ArrayAdapter<String> adapterKota = new ArrayAdapter<String>(PengaturanActivity.this,
//                android.R.layout.simple_spinner_dropdown_item, kot);
//        spinnerKabKota.setAdapter(adapterKota);
//        //kecamatan
//        List<String> kec = new ArrayList<>();
//        kec.add(provinsi);
//        ArrayAdapter<String> adapterKecamatan = new ArrayAdapter<String>(PengaturanActivity.this,
//                android.R.layout.simple_spinner_dropdown_item, kec);
//        spinnerKecamatan.setAdapter(adapterKecamatan);
        Glide.with(getApplicationContext())
                .load(intent.getStringExtra("profileImage"))
                .into(ivProfile);

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_CAPTURE);
            }
        });

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.logoutProcess(PengaturanActivity.this, "application/json", "Bearer " + Hawk.get(Constants.TOKEN));


            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.updateProfileProcess(PengaturanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN),etInputNama.getText().toString(),etInputPhone.getText().toString(),provinceCode,
                        cityCode,districtCode,kelurahanCode,"",imageString,etInputEmail.getText().toString());
            }
        });
    }

    private void getProvinsi() {
        final List<Provinsi> listProvinsi = appDbHelper.getAllProvinsi();
        List<String> arrayProvinsi = new ArrayList<>();

        for (int i = 0; i < listProvinsi.size(); i++) {
            arrayProvinsi.add(listProvinsi.get(i).getName());
        }
        ArrayAdapter<String> adapterProvinsi = new ArrayAdapter<String>(PengaturanActivity.this,
                android.R.layout.simple_spinner_dropdown_item, arrayProvinsi);
        spinnerProvinsi.setAdapter(adapterProvinsi);
        spinnerProvinsi.setSelection(arrayProvinsi.indexOf(provinsi));

        spinnerProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvinsi.get(position).getId();
                getKota(provinceCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getKota(String provinceCode) {
        final List<Kota> listKota = appDbHelper.getKotaByProv(provinceCode);
        List<String> arrayKota = new ArrayList<>();
        for (int i = 0; i < listKota.size(); i++) {
            arrayKota.add(listKota.get(i).getCityName());
        }

        ArrayAdapter<String> adapterKota = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, arrayKota);
        spinnerKabKota.setAdapter(adapterKota);
        spinnerKabKota.setSelection(arrayKota.indexOf(kota));

        spinnerKabKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listKota.get(position).getCityCode();
                getKecamatan(cityCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getKecamatan(String cityCode) {
        dataRepository.getKecamatan(PengaturanActivity.this,
                cityCode, new DataResponse.KecamatanCallback() {
                    @Override
                    public void onSuccess(List<Kecamatan> list) {
                        final List<Kecamatan> listKecamatan = list;
                        List<String> arrayKecamatan = new ArrayList<>();
                        for (int i = 0; i < listKecamatan.size(); i++) {
                            arrayKecamatan.add(listKecamatan.get(i).getDistrictName());
                        }

                        ArrayAdapter<String> adapterKecamatan = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_dropdown_item, arrayKecamatan);
                        spinnerKecamatan.setAdapter(adapterKecamatan);
                        spinnerKecamatan.setSelection(arrayKecamatan.indexOf(kecamatan));

                        spinnerKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                districtCode = listKecamatan.get(position).getKecCode();
                                getKelurahan(districtCode);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                    @Override
                    public void onMessage(String message) {

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });

    }

    private void getKelurahan(String districtCode) {
        System.out.println("DISTRICT CODE " + districtCode);
        dataRepository.getKelurahan(PengaturanActivity.this, districtCode, new DataResponse.KelurahanCallback() {
            @Override
            public void onSuccess(List<Kelurahan> list) {

                final List<Kelurahan> listKelurahan = list;
                List<String> arrayKelurahan = new ArrayList<>();
                for (int i = 0; i < listKelurahan.size(); i++) {
                    arrayKelurahan.add(listKelurahan.get(i).getKelurahanName());
                }

                ArrayAdapter<String> adapterKelurahan = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_spinner_dropdown_item, arrayKelurahan);
                spinnerKelurahan.setAdapter(adapterKelurahan);
                spinnerKelurahan.setSelection(arrayKelurahan.indexOf(kelurahan));
                spinnerKelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        kelurahanCode = listKelurahan.get(position).getKelurahanCode();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onMessage(String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }


    private void dialogUbahPassword() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_ubah_password);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

//        TextView btn_lupaPassword = dialog.findViewById(R.id.btn_lupaPassword);
        final EditText etInputOldPassword = dialog.findViewById(R.id.et_inputOldPassword);
        final EditText etInputNewPassword = dialog.findViewById(R.id.et_inputNewPassword);
        Button btnSave = dialog.findViewById(R.id.btn_save);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.updatePassword(PengaturanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), etInputOldPassword.getText().toString(), etInputNewPassword.getText().toString());
                dialog.dismiss();
            }
        });

//        btn_lupaPassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialogForgotPassword();
//                dialog.dismiss();
//            }
//        });


        dialog.show();
    }

//    private void dialogForgotPassword() {
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_forgot_password);
//        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        dialog.setCancelable(true);
//
//        final EditText etInputEmail = dialog.findViewById(R.id.et_inputEmail);
//        Button btnSend = dialog.findViewById(R.id.btn_send);
//
//        btnSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                presenter.forgotPasswordProscess(getApplicationContext(), etInputEmail.getText().toString());
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {

    }

    @Override
    public void onLogoutSuccess(String message) {
        AppDialog.dialogGeneral(PengaturanActivity.this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
//                Intent intent = new Intent(PengaturanActivity.this, LoginActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();
                SessionManager session = new SessionManager(PengaturanActivity.this);
                session.logoutUser(PengaturanActivity.this);
                dialog.dismiss();



            }
        });
    }

    @Override
    public void onUpdateProfileSuccess(String message) {
        AppDialog.dialogGeneral(PengaturanActivity.this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdateProfileFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdatePasswordSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onUpdatePasswordFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

//    @Override
//    public void onForgotPasswordSuccess(String message) {
//        AppDialog.dialogGeneral(this, message, new DialogAction() {
//            @Override
//            public void okClick(DialogInterface dialog) {
//                dialog.dismiss();
//            }
//        });
//    }
//
//    @Override
//    public void onForgotPasswordFiled(String message) {
//        AppDialog.dialogGeneral(this, message, new DialogAction() {
//            @Override
//            public void okClick(DialogInterface dialog) {
//                dialog.dismiss();
//            }
//        });
//    }
}
