package com.mozaik.beskem.ui.detailkomunitas;

import android.content.Context;

import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface DetailKomunitasContract {
    interface View extends IBaseView {

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onDetailKegiatanSuccess(KomunitasDetail list);
        void onDetailKegiatanFailed(String message);

        void onJoinKomunitasSuccess(String message);

        void onJoinKomunitasFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getDetailKomunitas(Context context, String token, String communityCode, String memberCode);
        void joinKomunitas(Context context, String token, String community_code, String question_code, String answer_text);
    }
}
