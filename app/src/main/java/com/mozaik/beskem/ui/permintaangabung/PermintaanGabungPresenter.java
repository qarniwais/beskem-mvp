package com.mozaik.beskem.ui.permintaangabung;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.MemberRequest;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class PermintaanGabungPresenter extends BasePresenter<PermintaanGabungContract.View> implements PermintaanGabungContract.Presenter {

    private DataRepository dataRepository;

    public PermintaanGabungPresenter(PermintaanGabungContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getMemberRequest(Context context, String token, String communityCode) {
        view.setProgressBar(true);
        dataRepository.getMemberRequest(context, token, communityCode, new DataResponse.MemberRequestCallback() {
            @Override
            public void onSuccess(List<MemberRequest> list) {
                view.setProgressBar(false);
                view.onMemberRequestSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onMemberRequestFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void postMemberRequestKonfirmasi(Context context, String token, String profile_to_community_code, String user_code, String status_approve, String member_code, String member_level) {
        view.setProgressBar(true);
        dataRepository.konfirmasiMemberRequest(context, token, profile_to_community_code, user_code, status_approve, member_code, member_level, new DataResponse.KonfirmasiMemberRequestCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onMemberRequestKonfirmasiSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onMemberRequestKonfirmasiFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }
}
