package com.mozaik.beskem.ui.anggota;


import android.content.Context;

import com.mozaik.beskem.data.model.KomunitasMember;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

import retrofit2.Response;

public interface AnggotaContract {

    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showHandling(Throwable t);

        void onMemberSuccess(List<KomunitasMember> list);

        void onMemberFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void memberProcess(Context context, String token, String community_code, String limit, String offset);
    }
}
