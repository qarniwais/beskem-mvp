package com.mozaik.beskem.ui.detailkegiatan;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.ChekJoin;
import com.mozaik.beskem.data.model.EventDetail;
import com.mozaik.beskem.ui.base.BasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

public class DetailKegiatanPresenter extends BasePresenter<DetailKegiatanContract.View> implements DetailKegiatanContract.Presenter {
    private DataRepository dataRepository;

    public DetailKegiatanPresenter(DetailKegiatanContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getDetailKegiatan(Context context, String token,String accept, String event_code) {
        view.showLoading();
        dataRepository.getEventDetail(context, token,accept, event_code, new DataResponse.EventDetailCallback() {
            @Override
            public void onSuccess(EventDetail response) {
                view.hideLoading();
                view.onDetailKegiatanSuccess(response);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onDetailKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void hapusKegiatan(Context context, String token, String accept, String event_code) {
        view.showLoading();
        dataRepository.deleteEvent(context, token, accept, event_code, new DataResponse.HapusEventCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onHapusKegiatanSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onHapusKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void joinKegiatan(Context context, String token, String accept, String member_code, String event_code, String user_code) {
        view.showLoading();
        dataRepository.joinKegiatan(context, token, accept, member_code, event_code, user_code, new DataResponse.JoinEventCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onJoinKegiatanSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onJoinKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }

    @Override
    public void cancelKegiatan(Context context, String token, String accept, String event_code, String user_code, final String member_code) {
        view.showLoading();
        dataRepository.batalJoin(context, token, accept, event_code, user_code, member_code, new DataResponse.CancelJoinCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onCancelKegiatanSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onCancelKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }

    @Override
    public void checkKegiatan(Context context, String token, String accept, String event_code, String member_code, String user_code) {
        view.showLoading();
        dataRepository.checkJoinAttendance(context, token, accept, event_code, member_code, user_code, new DataResponse.CheckJoinCallback() {
            @Override
            public void onSuccess(ChekJoin message) {
                view.hideLoading();
                view.onCheckKegiatanSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onCheckKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }
}
