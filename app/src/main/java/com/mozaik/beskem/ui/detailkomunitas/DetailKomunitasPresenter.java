package com.mozaik.beskem.ui.detailkomunitas;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.ui.base.BasePresenter;

public class DetailKomunitasPresenter extends BasePresenter<DetailKomunitasContract.View> implements DetailKomunitasContract.Presenter {
    private DataRepository dataRepository;

    public DetailKomunitasPresenter(DetailKomunitasContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }
    @Override
    public void getDetailKomunitas(Context context, String token, String communityCode, String memberCode) {
        view.setProgressBar(true);
        dataRepository.detailKomunitas(context, token, communityCode, memberCode, new DataResponse.DetailKomunitasCallback() {
            @Override
            public void onSuccess(KomunitasDetail komunitas) {
                view.setProgressBar(false);
                view.onDetailKegiatanSuccess(komunitas);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onDetailKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void joinKomunitas(Context context, String token, String community_code, String question_code, String answer_text) {
        view.setProgressBar(true);
        dataRepository.joinKomunitas(context, token, community_code, question_code, answer_text, new DataResponse.JoinKomunitasCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onJoinKomunitasSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onJoinKomunitasFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }
}
