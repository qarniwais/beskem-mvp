package com.mozaik.beskem.ui.login;

import android.content.Context;

import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

/**
 * Created by Ilham Saputra on 08/03/19.
 */
public interface LoginContract {

    interface ILoginView extends IBaseView {
//        void setPresenter(ILoginPresenter presenter);

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onLoginSuccess(User user);

        void onLoginFailed(String message);

        void onForgotPasswordSuccess(String message);

        void onForgotPasswordFiled(String message);


    }

    interface ILoginPresenter extends IBasePresenter<ILoginView> {
        void loginProcess(Context context, String username, String password, String screen_category,
                          String device_density, String device_density2, String screen_in_pixel,
                          String screen_in_inchi, String osversion, String apiversion,
                          String device, String model, String manufacturer, String product,
                          String imei, String imsi, String simId, String lat, String lng);
        void forgotPasswordProscess(Context context, String email);

        void updateFcm(Context context, String token, String tokenFcm, String imei);
    }
}
