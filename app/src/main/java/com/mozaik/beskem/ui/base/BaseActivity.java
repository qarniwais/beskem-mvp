package com.mozaik.beskem.ui.base;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mozaik.beskem.R;

import butterknife.BindView;
import permission.auron.com.marshmallowpermissionhelper.ActivityManagePermission;

public abstract class BaseActivity extends ActivityManagePermission implements IBaseView{

    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setProgressBar(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void showDialog(String message) {

    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

}
