package com.mozaik.beskem.ui.kegiatan;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.Event;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class KegiatanPresenter extends BasePresenter<KegiatanContract.View> implements KegiatanContract.Presenter {

    private DataRepository dataRepository;

    public KegiatanPresenter(KegiatanContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getEvent(Context context, String token, String community_code) {
        view.showLoading();
        dataRepository.getEvent(context, token, community_code, new DataResponse.EventCallback() {
            @Override
            public void onSuccess(List<Event> list) {
                view.hideLoading();
                view.onEventSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onEventFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
            }
        });
    }
}
