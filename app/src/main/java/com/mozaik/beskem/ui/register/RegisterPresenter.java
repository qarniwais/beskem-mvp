package com.mozaik.beskem.ui.register;

import android.content.Context;
import android.util.Log;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.RegisterStepOne;
import com.mozaik.beskem.data.model.User;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by Ilham Saputra on 11/03/19.
 */
public class RegisterPresenter extends BasePresenter<RegisterContract.View> implements RegisterContract.Presenter {
    private DataRepository dataRepository;


    public RegisterPresenter(RegisterContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void registerProcess(Context context, String email, String name, String user_phone, String password, String konfirmasi_password) {
        view.showLoading();
        dataRepository.registerStepOne(context, email, name, user_phone, password, konfirmasi_password, new DataResponse.RegisterStepOneCallback() {
            @Override
            public void onSuccess(RegisterStepOne registerStepOne) {
                view.hideLoading();
                view.onRegisterSuccess(registerStepOne);
            }

            @Override
            public void onMessage(RegisterStepOne message) {
                view.hideLoading();
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
            }
        });
    }

    @Override
    public void loginProcess(Context context, String username, String password, String screen_category, String device_density, String device_density2, String screen_in_pixel, String screen_in_inchi, String osversion, String apiversion, String device, String model, String manufacturer, String product, String imei, String imsi, String simId, String lat, String lng) {
        Log.i("LP", "Login Presenter");
        view.setProgressBar(true);

        dataRepository.login(context, username, password, screen_category,
                device_density, device_density2, screen_in_pixel,
                screen_in_inchi, osversion, apiversion,
                device, model, manufacturer, product,
                imei, imsi, simId, lat, lng, new DataResponse.LoginCallback() {
                    @Override
                    public void onSuccess(User user) {
                        view.setProgressBar(false);
                        view.onLoginSuccess(user);
                    }

                    @Override
                    public void onMessage(String message) {
                        view.setProgressBar(false);
                        view.onLoginFailed(message);
                        view.showToastMessage(message);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        view.setProgressBar(false);

                    }
                });

    }
}
