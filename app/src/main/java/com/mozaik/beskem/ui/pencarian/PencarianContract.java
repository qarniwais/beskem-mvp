package com.mozaik.beskem.ui.pencarian;

import android.content.Context;

import com.mozaik.beskem.data.model.Pencarian;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface PencarianContract {
    interface IPencarianView extends IBaseView {

        void setPresenter(IPencarianPresenter presenter);

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onPencarianSuccess(List<Pencarian> response);
        void onPencarianFailed(String response);

    }

    interface IPencarianPresenter extends IBasePresenter<IPencarianView> {
        void getSearchKomunitas(Context context,String token, String name, String limit, String offset);

    }
}
