package com.mozaik.beskem.ui.tambahanggota;

import android.content.Context;

import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface TambahAnggotaContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

    }

    interface Presenter extends IBasePresenter<View> {
    }
}
