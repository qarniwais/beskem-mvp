package com.mozaik.beskem.ui.buatkomunitas;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.KomunitasKategori;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class BuatKomunitasPresenter extends BasePresenter<BuatKomunitasContract.View> implements BuatKomunitasContract.Presenter {

    DataRepository dataRepository;

    public BuatKomunitasPresenter(BuatKomunitasContract.View view, DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.view = view;
    }

    @Override
    public void createKomunitas(Context context, String token, String cover, String icon, String name, String desc, String website,
                                String categoryCode, String tanggalBerdiri, String hasQuestion, String questionTeks) {
        view.setProgressBar(true);
        dataRepository.createKomunitas(context, token, cover, icon, name, desc, website, categoryCode, tanggalBerdiri, hasQuestion, questionTeks, new DataResponse.CreateKomunitasCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onBuatKomunitasSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.showDialogError(message);

            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);

            }
        });
    }

    @Override
    public void updateKomunitas(Context context, String token, String communityCode, String name, String desc, String website, String tanggalBerdiri, String icon, String cover, String categoryCode, String hasQuestion, String questionCode, String questionTeks) {
        view.setProgressBar(true);
        dataRepository.updateKomunitas(context, token, communityCode, name, desc, website, tanggalBerdiri, icon, cover, categoryCode, hasQuestion, questionCode, questionTeks, new DataResponse.EditKomunitasCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onUpdateKomunitasSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onUpdateKomunitasFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void getKomunitasKategori(Context context, String token) {
        view.setProgressBar(true);
        dataRepository.getKomunitasKategori(context, token, new DataResponse.KomunitasKategoriCallback() {
            @Override
            public void onSuccess(List<KomunitasKategori> list) {
                view.setProgressBar(false);
                view.onKomunitasKategoriSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onKomunitasKategoriFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }
}