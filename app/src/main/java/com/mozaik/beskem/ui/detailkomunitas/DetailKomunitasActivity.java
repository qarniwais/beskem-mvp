package com.mozaik.beskem.ui.detailkomunitas;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.local.AppDbHelper;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.home.HomeActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailKomunitasActivity extends BaseActivity implements DetailKomunitasContract.View {
    AppDbHelper appDbHelper;
    DataRepository dataRepository;
    DetailKomunitasPresenter presenter;
    @BindView(R.id.iv_notif)
    ImageView ivNotif;

    @BindView(R.id.iv_keluar)
    ImageView ivKeluar;

    @BindView(R.id.tv_nama_komunitas)
    TextView tvNamaKomunitas;
    @BindView(R.id.tv_deskripsi_komunitas)
    TextView tvDeskripsiKomunitas;
    @BindView(R.id.tv_date_komunitas)
    TextView tvDateKomunitas;
    @BindView(R.id.tv_web_komunitas)
    TextView tvWebKomunitas;
    @BindView(R.id.btn_buat_berita)
    Button btnBuatBerita;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.btn_bergabung)
    Button btnBergabung;
    @BindView(R.id.layout1)
    LinearLayout layout1;
    @BindView(R.id.tv_lihatSemuaBerita)
    TextView tvLihatSemuaBerita;
    @BindView(R.id.recycler_berita)
    RecyclerView recyclerBerita;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    String communityCode, memberCode, token;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    KomunitasDetail komunitasDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_komunitas);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Hawk.init(this).build();
        dataRepository = Injection.provideDataRepository(this);
        presenter = new DetailKomunitasPresenter(this, dataRepository);
        Intent intent = getIntent();
        communityCode = intent.getStringExtra("communityCode");
        memberCode = intent.getStringExtra("memberCode");
        presenter.getDetailKomunitas(this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), communityCode, memberCode);

        swipeRefresh.setColorSchemeResources(R.color.colorBackgroudHome, R.color.colorBackgroudHome);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        presenter.getDetailKomunitas(DetailKomunitasActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), communityCode, memberCode);


                    }
                }, 2000);
            }
        });
        btnBergabung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                presenter.joinKomunitas(DetailKomunitasActivity.this,"Bearer " + Hawk.get(Constants.TOKEN), communityCode,"","" );
                dialogJoin();
            }
        });
    }

    public void dialogJoin() {
        final Dialog dialog = new Dialog(DetailKomunitasActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_komunitas_join);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        TextView pertanyaan = dialog.findViewById(R.id.tv_pertanyaan);
        pertanyaan.setText(komunitasDetail.getCommunityQuestionTeks());

        final EditText jawaban = dialog.findViewById(R.id.et_inputJawaban);

        Button btn = dialog.findViewById(R.id.btn_proses);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.joinKomunitas(DetailKomunitasActivity.this,"Bearer " + Hawk.get(Constants.TOKEN), communityCode,komunitasDetail.getCommunityQuestionCode(),jawaban.getText().toString() );
                dialog.dismiss();
                dialogJoinPersetujuan();
            }
        });

        dialog.show();
    }

    public void dialogJoinPersetujuan() {
        final Dialog dialog = new Dialog(DetailKomunitasActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_komunitas_persetujuan);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        Button btn = dialog.findViewById(R.id.btn_kembali);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(DetailKomunitasActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        dialog.show();
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onDetailKegiatanSuccess(KomunitasDetail list) {
        komunitasDetail = list;
        tvNamaKomunitas.setText(list.getCommunityName());
        tvDateKomunitas.setText("Tanggal Berdiri: " + list.getCommunityTanggalBerdiri());
        tvDeskripsiKomunitas.setText(list.getCommunityDescription());
        tvWebKomunitas.setText("Website: " + list.getCommunityWebsite());
        Glide.with(this)
                .load(list.getCommunityCover())
                .into(ivCover);
        System.out.println("sdsdsdsdsdsd" + list.getCommunityName());

    }

    @Override
    public void onDetailKegiatanFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJoinKomunitasSuccess(String message) {
//        AppDialog.dialogGeneral(this, message, new DialogAction() {
//            @Override
//            public void okClick(DialogInterface dialog) {
//                dialog.dismiss();
//            }
//        });
        dialogJoinPersetujuan();
    }

    @Override
    public void onJoinKomunitasFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }
}
