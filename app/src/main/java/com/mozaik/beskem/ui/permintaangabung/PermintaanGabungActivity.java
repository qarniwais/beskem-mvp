package com.mozaik.beskem.ui.permintaangabung;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.MemberRequestAdapter;
import com.mozaik.beskem.data.model.MemberRequest;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PermintaanGabungActivity extends BaseActivity implements PermintaanGabungContract.View {

    @BindView(R.id.recycler_permintaan)
    RecyclerView recyclerPermintaan;
    List<MemberRequest> requests = new ArrayList<MemberRequest>();
    MemberRequestAdapter adapter;
    DataRepository dataRepository;
    PermintaanGabungPresenter presenter;
    String profiletocommnitycode, usercode, pertanyaan, jawaban, image, name, user, statusAprove;
    Integer isAgree = 0;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permintaan_gabung);
        ButterKnife.bind(this);
        Hawk.init(this).build();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dataRepository = Injection.provideDataRepository(this);
        presenter = new PermintaanGabungPresenter(this, dataRepository);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerPermintaan.setLayoutManager(layoutManager);
        recyclerPermintaan.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerPermintaan, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MemberRequest memberRequest = requests.get(position);
                profiletocommnitycode = memberRequest.getProfileToCommunityCode();
                usercode = memberRequest.getUserCode();
                pertanyaan = memberRequest.getCommunityQuestionTeks();
                jawaban = memberRequest.getCommunityQuestionAnswerTeks();
                image = memberRequest.getUserImg();
                name = memberRequest.getName();
                dialogAprove1();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        presenter.getMemberRequest(this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString());

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getMemberRequest(this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString());

    }

    public void dialogAprove1() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permintaan_gabung);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);


        TextView tanya = dialog.findViewById(R.id.tv_pertanyaan);
        tanya.setText(pertanyaan);

        TextView textView = dialog.findViewById(R.id.tv_jawaban);
        textView.setText(jawaban);

        TextView textView1 = dialog.findViewById(R.id.tv_name);
        textView1.setText(name);

        CircleImageView circleImageView = dialog.findViewById(R.id.circle_image1);
        Glide.with(this)
                .load(image)
                .into(circleImageView);


        Button terima = dialog.findViewById(R.id.btn_terima);
        terima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusAprove = "0";
                dialogAprove2();
                dialog.dismiss();
            }
        });

        TextView tidak = dialog.findViewById(R.id.tv_tidak);
        tidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusAprove = "2";
                dialogAprove2();
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public void dialogAprove2() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permintaan_gabung2);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        final ImageView ivIsAgree = dialog.findViewById(R.id.iv_isAgree);

        ivIsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgree == 0) {
                    isAgree = 1;
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle);
                    Toast.makeText(PermintaanGabungActivity.this, "admin" + isAgree, Toast.LENGTH_SHORT).show();
//                    Hawk.put(Constants.INFOMEMBER,"1");
                    final EditText editText = dialog.findViewById(R.id.et_inputCode);
                    Button button = dialog.findViewById(R.id.btn_terima);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presenter.postMemberRequestKonfirmasi(PermintaanGabungActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), profiletocommnitycode, usercode, "0", editText.getText().toString(), isAgree.toString());
                            dialog.dismiss();
                        }
                    });
                } else {
                    isAgree = 0;
//                    Hawk.put(Constants.INFOMEMBER, "0");
                    ivIsAgree.setImageResource(R.drawable.ic_switch_toggle_grey);
                    Toast.makeText(PermintaanGabungActivity.this, "member" + isAgree, Toast.LENGTH_SHORT).show();
                    final EditText editText = dialog.findViewById(R.id.et_inputCode);
                    Button button = dialog.findViewById(R.id.btn_terima);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presenter.postMemberRequestKonfirmasi(PermintaanGabungActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), profiletocommnitycode, usercode, "0", editText.getText().toString(), isAgree.toString());
                            dialog.dismiss();
                        }
                    });
                }
            }
        });


        dialog.show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onMemberRequestSuccess(List<MemberRequest> list) {
        requests = list;
        adapter = new MemberRequestAdapter(PermintaanGabungActivity.this, list);
        recyclerPermintaan.setAdapter(adapter);
        System.out.println("dfddvcvcv" + list.get(0).getName());
    }

    @Override
    public void onMemberRequestFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onMemberRequestKonfirmasiSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onMemberRequestKonfirmasiFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
