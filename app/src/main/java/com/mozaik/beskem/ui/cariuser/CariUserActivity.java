package com.mozaik.beskem.ui.cariuser;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.PencarianUserAdapter;
import com.mozaik.beskem.data.model.PencarianUser;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CariUserActivity extends BaseActivity implements CariUserContract.View {

    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.recycler_pencarian)
    RecyclerView recyclerPencarian;
    DataRepository dataRepository;
    CariUserPresenter presenter;
    PencarianUserAdapter adapter;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private List<PencarianUser> users = new ArrayList<PencarianUser>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_user);
        ButterKnife.bind(this);
        Hawk.init(this).build();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dataRepository = Injection.provideDataRepository(this);
        presenter = new CariUserPresenter(this, dataRepository);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerPencarian.setLayoutManager(layoutManager);
        recyclerPencarian.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerPencarian, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                PencarianUser user = users.get(position);
                dialogAddMember(user.getUserCode(), user.getName());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                presenter.getCariUser(CariUserActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString(), s, "", "");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() > 0) {
                    presenter.getCariUser(CariUserActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), Hawk.get(Constants.COMMUNITYCODE).toString(), s, "", "");

                } else {
                    recyclerPencarian.setVisibility(View.GONE);
                }
                return false;
            }
        });
    }

    public void dialogAddMember(final String userCode, String nama) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_member);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        Button btndialogconfirm = dialog.findViewById(R.id.btndialogconfirm);
        Button btndialogcancel = dialog.findViewById(R.id.btndialogcancel);
        TextView textView = dialog.findViewById(R.id.tv_title);
        textView.setText("Sarankan " + nama + " untuk masuk ke komunitas " + Hawk.get(Constants.NAMEKOMUNITAS));
        btndialogconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                presenter.addMember(CariUserActivity.this, "Bearer " + Hawk.get(Constants.TOKEN), "application/json", Hawk.get(Constants.COMMUNITYCODE).toString(), userCode);
                dialog.dismiss();
            }
        });

        Toast.makeText(this, userCode, Toast.LENGTH_SHORT).show();

        btndialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {
        AppDialog.dialogGeneral(this, msg, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onCariUserSuccess(List<PencarianUser> response) {
        users = response;
        if (response.size() <= 0) {
            recyclerPencarian.setVisibility(View.GONE);
            AppDialog.dialogGeneral(this, "Data pencarian tidak ada", new DialogAction() {
                @Override
                public void okClick(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
        } else {
            recyclerPencarian.setVisibility(View.VISIBLE);
            adapter = new PencarianUserAdapter(this, response);
            recyclerPencarian.setAdapter(adapter);
        }


    }

    @Override
    public void onAddMemberSuccess(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onAddMemberFailed(String message) {
        AppDialog.dialogGeneral(this, message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
