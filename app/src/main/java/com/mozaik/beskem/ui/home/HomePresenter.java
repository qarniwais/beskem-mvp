package com.mozaik.beskem.ui.home;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.NotifCount;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by Ilham Saputra on 11/03/19.
 */
public class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter {

    private DataRepository dataRepository;

    public HomePresenter(HomeContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getCommunityData() {

    }

    @Override
    public void registerTwoProcess(Context context, String email, String province_code, String city_code, String district_code, String kelurahan_code) {
        view.setProgressBar(true);
        dataRepository.registerStepTwo(context, email, province_code, city_code, district_code, kelurahan_code, new DataResponse.RegisterStepTwoCallback() {
            @Override
            public void onSuccess(String registerStepTwo) {
                view.setProgressBar(false);
                view.onRegisterTwoSuccess(registerStepTwo);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onRegisterTwoFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
            }
        });
    }

    @Override
    public void getProvinsi(Context context) {
        view.setProgressBar(true);
        dataRepository.getProvinsi(context, new DataResponse.ProvinsiCallback() {
            @Override
            public void onSuccess(List<Provinsi> list) {
                view.setProgressBar(false);
                view.onProvinsiSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);

            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);

            }
        });

    }

    @Override
    public void checkRegister(Context context, String email) {
        view.setProgressBar(true);
        dataRepository.checkRegister(context, email, new DataResponse.CheckRegisterCallback() {
            @Override
            public void onSuccess(CheckRegister checkRegister) {
                view.setProgressBar(false);
                view.onCheckRegisterSuccess(checkRegister);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
            }
        });
    }

    @Override
    public void getNews(Context context, String token) {
        view.setProgressBar(true);
        dataRepository.getNews(context, token, new DataResponse.NewsCallback() {

            @Override
            public void onSuccess(List<News> list) {
                view.onGetNewsSuccess(list);
                view.showDialog("YEESSSSSSSS");

            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onGetNewsFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void getKomunitas(Context context, String token) {
        view.setProgressBar(true);
        dataRepository.getKomunitas(context, token, new DataResponse.KomunitasCallback() {
            @Override
            public void onSuccess(List<Komunitas> list) {
                view.setProgressBar(false);
                view.onGetKomunitasSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onGetKomunitasFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void cekToken(Context context, String token, String accept) {
        view.setProgressBar(true);
        dataRepository.CekToken(context, token, accept, new DataResponse.TokenCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onTokenSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onTokenFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                throwable.printStackTrace();
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void getNotifCount(Context context, String token, String accept) {
        view.setProgressBar(true);
        dataRepository.CountNotif(context, token, accept, new DataResponse.NotifCountCallback() {
            @Override
            public void onSuccess(NotifCount notif) {
                view.setProgressBar(false);
                view.onNotifCountSuccess(notif);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onNotifCountFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                throwable.printStackTrace();
                view.showHandling(throwable);
            }
        });
    }
}
