package com.mozaik.beskem.ui.notification;

import android.content.Context;

import com.mozaik.beskem.data.model.NotificationM;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface NotifContract {
    interface View extends IBaseView {

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void showLoading();

        void hideLoading();


        void onGetNotifSuccess(List<NotificationM> notif);

        void onGetNotifFailed(String message);

        void onPostNotifReadSuccess(String message);

        void onPostNotifReadFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getNotif(Context context, String token, String accept, String limit, String offset);
        void postNotifRead(Context context, String token, String accept, String notifikasi_id);
    }
}
