package com.mozaik.beskem.ui.base;

/**
 * Created by Ilham Saputra on 08/03/19.
 */
public class BasePresenter<ViewT> implements IBasePresenter<ViewT> {

    protected ViewT view;

    @Override
    public void onViewActive(ViewT view) {
        this.view = view;
    }

    @Override
    public void onViewInactive() {
        view = null;
    }
}
