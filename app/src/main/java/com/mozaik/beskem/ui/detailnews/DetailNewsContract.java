package com.mozaik.beskem.ui.detailnews;

import android.content.Context;

import com.mozaik.beskem.data.model.KomunitasPostDetail;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface DetailNewsContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogSuccess(String msg);

        void showHandling(Throwable t);

        void onGetDetailNewsSuccess(News list);

        void onGetDetailNewsFailed(String message);

        void onGetDetailPostSuccess(KomunitasPostDetail list);

        void onGetDetailPostFailed(String message);

        void onDeletePostingSuccess(String message);

        void onDeletePostingFailed(String message);

    }
    interface Presenter extends IBasePresenter<View> {
        void getDetailNews(Context context, String token, String code_news);
        void getDetailPost(Context context, String token,String accept,String contentType, String post_code);
        void deletePosting(Context context, String token, String posting_code);
    }
}
