package com.mozaik.beskem.ui.dashboardkomunitas;

import android.content.Context;

import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.ModelData;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface DashboardContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onDetailKegiatanSuccess(KomunitasDetail list);

        void onDetailKegiatanFailed(String message);

        void onKomunitasPostSuccess(List<KomunitasPost> list);

        void onKomunitasPostFailed(String message);

        void onDeleteMemberSuccess(String message);
        void onDeleteMemberFailed(String message);
    }

    interface IDashboardPresenter extends IBasePresenter<View> {
        void getDetailKomunitas(Context context, String token, String communityCode, String memberCode);
        void getPost(Context context, String token, String communityCode);
        void deleteMember(Context context, String token, String communityCode, String userCode);
    }
}
