package com.mozaik.beskem.ui.iuran;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mozaik.beskem.R;
import com.mozaik.beskem.ui.base.BaseView;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class IuranFragment extends BaseView implements IuranContract.View {

    ProgressBar progressBar;
    Unbinder unbinder;

    public IuranFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iuran, container, false);
        unbinder = ButterKnife.bind(this, view);
        progressBar = view.findViewById(R.id.progressBar);
        return view;
    }

    @Override
    public void showDialog(String message) {

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(getActivity(), t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onIuranSuccess(List<String> response) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
