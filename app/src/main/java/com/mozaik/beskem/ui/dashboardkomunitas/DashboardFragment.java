package com.mozaik.beskem.ui.dashboardkomunitas;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.adapter.BeritaAdapter;
import com.mozaik.beskem.data.adapter.MenuAdapter;
import com.mozaik.beskem.data.adapter.PostAdapter;
import com.mozaik.beskem.data.local.AppDbHelper;
import com.mozaik.beskem.data.model.CheckRegister;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.KomunitasDetail;
import com.mozaik.beskem.data.model.KomunitasPost;
import com.mozaik.beskem.data.model.ModelData;
import com.mozaik.beskem.data.model.News;
import com.mozaik.beskem.data.model.Provinsi;
import com.mozaik.beskem.data.response.RegisterStepTwoResponse;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.base.BaseView;
import com.mozaik.beskem.ui.buatkomunitas.BuatKomunitasActivity;
import com.mozaik.beskem.ui.buatpost.BuatPostActivity;
import com.mozaik.beskem.ui.detailnews.DetailNewsActivity;
import com.mozaik.beskem.ui.home.HomeActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends BaseView implements DashboardContract.View {

    DashboardContract.View view;
    DashboardPresenter presenter;
    RecyclerView recyclerKomunitas;
    MenuAdapter menuAdapter;
    RecyclerView recyclerBerita;
    BeritaAdapter beritaAdapter;
    PostAdapter adapter;
    Unbinder unbinder;
    CircleImageView profileImage;
    Spinner a;
    String email = Hawk.get(Constants.USER_EMAIL);
    TextView tvLihatSemuaBerita, tvWebKomunitas, tvDateKomunitas, tvDeskripsiKomunitas, tvTitleToolbar, tvNamaKomunitas;
    ImageView ivCover, ivSetting, ivAddPost;
    LinearLayout addPost, keluarKomunitas, setting;
    RelativeLayout layoutSetting, layoutKeluar, layoutAddpost;
    Button btnBergabung, btnBuatBerita;
    LinearLayout layout, layout1;
    ProgressBar progressBar;
    DataRepository dataRepository;
    AppDbHelper appDbHelper;
    private List<KomunitasPost> postList = new ArrayList<KomunitasPost>();
    private KomunitasDetail komunitasDetails;

    public DashboardFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        Hawk.init(getActivity()).build();
        tvTitleToolbar = view.findViewById(R.id.tv_title_toolbar);
        tvNamaKomunitas = view.findViewById(R.id.tv_nama_komunitas);
        tvDeskripsiKomunitas = view.findViewById(R.id.tv_deskripsi_komunitas);
        tvDateKomunitas = view.findViewById(R.id.tv_date_komunitas);
        tvWebKomunitas = view.findViewById(R.id.tv_web_komunitas);
        addPost = view.findViewById(R.id.add_post);
        ivAddPost = view.findViewById(R.id.iv_add_post);
        ivSetting = view.findViewById(R.id.iv_setting);
        keluarKomunitas = view.findViewById(R.id.keluarKomunitas);
        ivCover = view.findViewById(R.id.iv_cover);
        setting = view.findViewById(R.id.setting);
        layoutSetting = view.findViewById(R.id.layoutSetting);
        layoutKeluar = view.findViewById(R.id.layoutKeluar);
        layoutAddpost = view.findViewById(R.id.layoutAddpost);
        layout = view.findViewById(R.id.layout);
        layout1 = view.findViewById(R.id.layout1);
        tvLihatSemuaBerita = view.findViewById(R.id.tv_lihatSemuaBerita);
        btnBergabung = view.findViewById(R.id.btn_bergabung);
        btnBuatBerita = view.findViewById(R.id.btn_buat_berita);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerBerita = view.findViewById(R.id.recycler_berita);
        recyclerKomunitas = view.findViewById(R.id.recycler_komunitas);
        profileImage = view.findViewById(R.id.profile_image);
        DataRepository dataRepository = Injection.provideDataRepository(getActivity());
        presenter = new DashboardPresenter(this, dataRepository);
        presenter.getDetailKomunitas(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN).toString(), Hawk.get(Constants.COMMUNITYCODE).toString(), Hawk.get(Constants.MEMBERCODE).toString());
        presenter.getPost(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN).toString(), Hawk.get(Constants.COMMUNITYCODE).toString());

        if (Hawk.get(Constants.MEMBERLEVEL).equals("1")) {
            layoutSetting.setVisibility(View.VISIBLE);
            layoutAddpost.setVisibility(View.VISIBLE);
            layoutKeluar.setVisibility(View.GONE);
        } else {
            layoutSetting.setVisibility(View.GONE);
            layoutAddpost.setVisibility(View.GONE);
            layoutKeluar.setVisibility(View.VISIBLE);
        }

        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hawk.put(Constants.BUATPOST, 9);
                Intent intent = new Intent(getActivity(), BuatPostActivity.class);
                intent.putExtra("buatPost", "1");
                startActivity(intent);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerBerita.setLayoutManager(layoutManager);
        recyclerBerita.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerBerita, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Hawk.put(Constants.BUATPOST, 8);
                KomunitasPost komunitasPost = postList.get(position);
                Intent intent = new Intent(getActivity(), DetailNewsActivity.class);
                intent.putExtra("codeNews", komunitasPost.getPostingCode());
                intent.putExtra("titleNews", komunitasPost.getPostingJudul());
                intent.putExtra("dateNews", komunitasPost.getPostingDateCreated());
                intent.putExtra("contentLongNews", komunitasPost.getPostingContentLong());
                intent.putExtra("publisherNews", komunitasPost.getPostingPublisher());
                intent.putExtra("mediaNews", komunitasPost.getPostingMedia());
                intent.putExtra("toolbar_title", "KIRIMAN");
                startActivity(intent);
                Toast.makeText(getActivity(), komunitasPost.getPostingCode(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hawk.put(Constants.INFO, 1);
                try {
                    Intent intent = new Intent(getActivity(), BuatKomunitasActivity.class);
                    intent.putExtra("communityCode", komunitasDetails.getCommunityCode());
                    intent.putExtra("communityName", komunitasDetails.getCommunityName());
                    intent.putExtra("communityDesc", komunitasDetails.getCommunityDescription());
                    intent.putExtra("communityWeb", komunitasDetails.getCommunityWebsite());
                    intent.putExtra("communityDate", komunitasDetails.getCommunityTanggalBerdiri());
                    intent.putExtra("communityQuestion", komunitasDetails.getCommunityQuestionTeks());
                    intent.putExtra("communityCover", komunitasDetails.getCommunityCover());
                    intent.putExtra("communityIcon", komunitasDetails.getCommunityIcon());
                    startActivity(intent);
                } catch (Exception e) {

                }

            }
        });

        keluarKomunitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDialog.showDialogOpsi(getActivity(), "Konfirmasi", "Apakah anda ingin keluar komunitas?", "Ya", new DialogAction() {
                    @Override
                    public void okClick(DialogInterface dialog) {
                        presenter.deleteMember(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN).toString(), komunitasDetails.getCommunityCode(), Hawk.get(Constants.USER_CODE).toString());
                        System.out.println("token = " + Hawk.get(Constants.TOKEN).toString());
                        System.out.println("community code = " + komunitasDetails.getCommunityCode());
                        System.out.println("user code = " + Hawk.get(Constants.USER_CODE).toString());
                        dialog.dismiss();
                    }
                });

            }
        });

        Toast.makeText(getActivity(), Hawk.get(Constants.MEMBERLEVEL).toString(), Toast.LENGTH_SHORT).show();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getDetailKomunitas(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN).toString(), Hawk.get(Constants.COMMUNITYCODE).toString(), Hawk.get(Constants.MEMBERCODE).toString());
        presenter.getPost(getActivity(), "Bearer " + Hawk.get(Constants.TOKEN).toString(), Hawk.get(Constants.COMMUNITYCODE).toString());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(getActivity(), t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onDetailKegiatanSuccess(KomunitasDetail list) {
        komunitasDetails = list;
        tvNamaKomunitas.setText(list.getCommunityName());
        tvDateKomunitas.setText("Tanggal Berdiri: " + list.getCommunityTanggalBerdiri());
        tvDeskripsiKomunitas.setText(list.getCommunityDescription());
        tvWebKomunitas.setText("Website: " + list.getCommunityWebsite());
        if (getActivity() != null) {
            Glide.with(getActivity())
                    .load(list.getCommunityCover())
                    .into(ivCover);
        }

    }

    @Override
    public void onDetailKegiatanFailed(String list) {
        Toast.makeText(getActivity(), list, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onKomunitasPostSuccess(List<KomunitasPost> list) {
        postList = list;
        adapter = new PostAdapter(getActivity(), list);
        recyclerBerita.setAdapter(adapter);
    }

    @Override
    public void onKomunitasPostFailed(String message) {
        if (getActivity() != null) {
            AppDialog.dialogGeneral(getActivity(), message, new DialogAction() {
                @Override
                public void okClick(DialogInterface dialog) {
                    dialog.dismiss();
                }
            });
        }

    }

    @Override
    public void onDeleteMemberSuccess(String message) {
        AppDialog.dialogGeneral(getActivity(), message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onDeleteMemberFailed(String message) {
        AppDialog.dialogGeneral(getActivity(), message, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void showDialog(String message) {

    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
