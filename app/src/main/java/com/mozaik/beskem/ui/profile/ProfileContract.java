package com.mozaik.beskem.ui.profile;

import android.content.Context;

import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.Profile;
import com.mozaik.beskem.data.model.ProfileMember;
import com.mozaik.beskem.ui.base.BasePresenter;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface ProfileContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onProfileSuccess(Profile user);

        void onGetKomunitasSuccess(List<Komunitas> list);

        void onGetKomunitasFailed(String message);

        void onGetQRSuccess(ProfileMember list);

        void onGetQRFailed(String message);


    }

    interface Presenter extends IBasePresenter<View> {
        void profileProcess(Context context, String token);
        void getKomunitas(Context context, String token);
        void getQR(Context context, String token, String community_code);
    }
}
