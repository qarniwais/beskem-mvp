package com.mozaik.beskem.ui.kegiatan;


import android.content.Context;

import com.mozaik.beskem.data.model.Event;
import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

import java.util.List;

public interface KegiatanContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onEventSuccess(List<Event> list);

        void onEventFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void getEvent(Context context, String token, String community_code);
    }
}
