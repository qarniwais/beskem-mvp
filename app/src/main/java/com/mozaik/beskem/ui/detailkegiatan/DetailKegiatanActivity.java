package com.mozaik.beskem.ui.detailkegiatan;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mozaik.beskem.R;
import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.model.ChekJoin;
import com.mozaik.beskem.data.model.EventDetail;
import com.mozaik.beskem.di.Injection;
import com.mozaik.beskem.ui.attendance.AttendanceActivity;
import com.mozaik.beskem.ui.base.BaseActivity;
import com.mozaik.beskem.ui.buatkegiatan.BuatKegiatanActivity;
import com.mozaik.beskem.utils.AppDialog;
import com.mozaik.beskem.utils.Constants;
import com.mozaik.beskem.utils.dialoginterface.DialogAction;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailKegiatanActivity extends BaseActivity implements DetailKegiatanContract.View {

    @BindView(R.id.iv_notif)
    ImageView ivNotif;
    @BindView(R.id.tv_nama_kegiatan)
    TextView tvNamaKegiatan;
    @BindView(R.id.tv_deskripsi_komunitas)
    TextView tvDeskripsiKomunitas;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.place)
    ImageView place;
    @BindView(R.id.tv_lokasi)
    TextView tvLokasi;
    @BindView(R.id.tv_alamat)
    TextView tvAlamat;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    DataRepository dataRepository;
    DetailKegiatanPresenter presenter;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    String eventCode;
    @BindView(R.id.notif)
    LinearLayout notif;
    String event_code, community_code, member_code, event_name, event_description, event_image, event_date_start, event_date_end, event_time_start, event_time_end, location_latitude, location_longitude, location_convertion, has_rsvp, event_max_member, event_max_date;

    @BindView(R.id.btn_join)
    Button btnJoin;
    @BindView(R.id.join)
    LinearLayout join;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.cancel)
    LinearLayout cancel;
    EventDetail details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kegiatan);
        ButterKnife.bind(this);
        Hawk.init(this).build();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        DataRepository dataRepository = Injection.provideDataRepository(this);
        presenter = new DetailKegiatanPresenter(this, dataRepository);
        Intent intent = getIntent();
        eventCode = intent.getStringExtra("eventCode");

        presenter.getDetailKegiatan(this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), "application/json", eventCode);
        notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogKegiatan();
            }
        });
//        Toast.makeText(this, detail.getHasRsvp(), Toast.LENGTH_SHORT).show();

//        join.setVisibility(View.VISIBLE);
//        cancel.setVisibility(View.GONE);


        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.joinKegiatan(DetailKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), "application/json", Hawk.get(Constants.MEMBERCODE).toString(), Hawk.get(Constants.EVENTCODE).toString(), Hawk.get(Constants.USER_CODE).toString());
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            presenter.cancelKegiatan(DetailKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString(),"application/json",Hawk.get(Constants.EVENTCODE).toString(),Hawk.get(Constants.USER_CODE).toString(),Hawk.get(Constants.MEMBERCODE).toString());
            }
        });
    }

    public void dialogKegiatan() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_setting_kegiatan);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);

        TextView attendance = dialog.findViewById(R.id.tv_attendance);
        attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailKegiatanActivity.this, AttendanceActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        TextView ubah = dialog.findViewById(R.id.tv_ubah);
        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hawk.put(Constants.INFO, 5);
//                Hawk.put(Constants.POSTINGCODE, "");
                Intent intent = new Intent(DetailKegiatanActivity.this, BuatKegiatanActivity.class);
                intent.putExtra("eventCode", details.getEventCode());
                intent.putExtra("communityCode", details.getCommunityCode());
                intent.putExtra("memberCode", details.getMemberCode());
                intent.putExtra("eventName", details.getEventName());
                intent.putExtra("eventDeskrip", details.getEventDescription());
                intent.putExtra("eventImage", details.getEventImageUrl());
                intent.putExtra("eventDateStart", details.getEventDateStart());
                intent.putExtra("eventDateEnd", details.getEventDateEnd());
                intent.putExtra("eventTimeStart", details.getEventTimeStart());
                intent.putExtra("eventTimeEnd", details.getEventTimeEnd());
                intent.putExtra("eventLatitude", details.getLocationLatitude());
                intent.putExtra("eventLongtitude", details.getLocationLongitude());
                intent.putExtra("eventLocation", details.getLocationConvertion());
                intent.putExtra("eventRSVP", details.getEventStatus());
                intent.putExtra("eventMaxMember", details.getEventMaxMember());
                intent.putExtra("eventMaxDate", details.getEventMaxDate());
                startActivity(intent);

                dialog.dismiss();
            }
        });

        TextView hapus = dialog.findViewById(R.id.tv_hapus);
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.hapusKegiatan(DetailKegiatanActivity.this, "Bearer " + Hawk.get(Constants.TOKEN).toString(), "application/json", eventCode);
            }
        });

        dialog.show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialogError(String msg) {

    }

    @Override
    public void showHandling(Throwable t) {
        AppDialog.dialogGeneral(this, t.toString(), new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onDetailKegiatanSuccess(EventDetail response) {
        details = response;
        tvNamaKegiatan.setText(response.getEventName());
        tvDeskripsiKomunitas.setText(response.getEventDescription());
        tvDate.setText(response.getEventDateStart());
        tvTime.setText(response.getEventTimeStart() + " - " + response.getEventTimeEnd());
        tvLokasi.setText(response.getLocationConvertion());
        try {
            Glide.with(this)
                    .load(response.getEventImageUrl())
                    .into(ivCover);
        } catch (Exception e) {

        }
        if (response.getHasRsvp().toString().equals("1")){
            join.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.GONE);
            presenter.checkKegiatan(this, "Bearer " + Hawk.get(Constants.TOKEN).toString(),"application/json",Hawk.get(Constants.EVENTCODE).toString(),Hawk.get(Constants.MEMBERCODE).toString(),Hawk.get(Constants.USER_CODE).toString());
        }

    }

    @Override
    public void onDetailKegiatanFailed(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onHapusKegiatanSuccess(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                onBackPressed();
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onHapusKegiatanFailed(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onJoinKegiatanSuccess(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                join.setVisibility(View.GONE);
                cancel.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onJoinKegiatanFailed(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onCancelKegiatanSuccess(String response) {
        AppDialog.dialogGeneral(this, response, new DialogAction() {
            @Override
            public void okClick(DialogInterface dialog) {
                join.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onCancelKegiatanFailed(String response) {

    }

    @Override
    public void onCheckKegiatanSuccess(ChekJoin response) {
        if (response.getStatusJoin() == 1){
            cancel.setVisibility(View.VISIBLE);
            join.setVisibility(View.GONE);
        }else if (response.getStatusJoin() == 0){
            join.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCheckKegiatanFailed(String response) {

    }
}
