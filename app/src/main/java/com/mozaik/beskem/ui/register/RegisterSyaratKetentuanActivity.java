package com.mozaik.beskem.ui.register;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mozaik.beskem.R;
import com.mozaik.beskem.ui.MainActivity;
import com.mozaik.beskem.ui.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterSyaratKetentuanActivity extends AppCompatActivity {

    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.btn_setuju)
    Button btnSetuju;
    @BindView(R.id.button)
    LinearLayout button;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_syarat_ketentuan);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == 0) {
                    button.setVisibility(View.GONE);
                } else if (scrollY < oldScrollY) {
                    button.setVisibility(View.VISIBLE);
                    btnSetuju.setBackgroundResource(R.drawable.bg_blue_cyrcle);

                } else if (oldScrollY < 0) {
                    button.setVisibility(View.VISIBLE);
                    btnSetuju.setBackgroundResource(R.drawable.bg_blue_cyrcle);
                }
            }
        });

        btnSetuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterSyaratKetentuanActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

}
