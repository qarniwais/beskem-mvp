package com.mozaik.beskem.ui.buatkegiatan;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.ui.base.BasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;

public class BuatKegiatanPresenter extends BasePresenter<BuatKegiatanContract.View> implements BuatKegiatanContract.Presenter {

    DataRepository dataRepository;

    public BuatKegiatanPresenter(BuatKegiatanContract.View view, DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        this.view = view;
    }

    @Override
    public void buatKegiatan(Context context, String token, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String event_max_member, String event_max_date,String has_rsvp) {
        view.setProgressBar(true);
        dataRepository.buatEvent(context, token, community_code, member_code, event_name, event_description, event_image, event_date_start, event_date_end, event_time_start, event_time_end, location_latitude, location_longitude, location_convertion, event_max_member, event_max_date,has_rsvp, new DataResponse.BuatKegiatanCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onBuatKegiatanSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.showDialogError(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void updateKegiatan(Context context, String token, String accept, String event_code, String community_code, String member_code, String event_name, String event_description, String event_image, String event_date_start, String event_date_end, String event_time_start, String event_time_end, String location_latitude, String location_longitude, String location_convertion, String has_rsvp, String event_max_member, String event_max_date) {
        view.setProgressBar(true);
        dataRepository.updateEvent(context, token, accept, event_code, community_code, member_code, event_name, event_description, event_image, event_date_start, event_date_end, event_time_start, event_time_end, location_latitude, location_longitude, location_convertion, has_rsvp, event_max_member, event_max_date, new DataResponse.UpdateKegiatanCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onUpdateKegiatanSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onUpdateKegiatanFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.showHandling(throwable);
            }
        });
    }
}
