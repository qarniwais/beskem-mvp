package com.mozaik.beskem.ui.profile;

import android.content.Context;
import android.util.Log;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.Komunitas;
import com.mozaik.beskem.data.model.Profile;
import com.mozaik.beskem.data.model.ProfileMember;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class ProfilePresenter extends BasePresenter<ProfileContract.View> implements ProfileContract.Presenter {
    private DataRepository dataRepository;

    public ProfilePresenter(ProfileContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void profileProcess(Context context, String token) {
        view.setProgressBar(true);
        Log.i("PP","Profile Presenter");
        dataRepository.getProfile(context, token, new DataResponse.ProfileCallback() {
            @Override
            public void onSuccess(Profile profile) {
                view.setProgressBar(false);
                view.onProfileSuccess(profile);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });

    }

    @Override
    public void getKomunitas(Context context, String token) {
        view.setProgressBar(true);
        dataRepository.getKomunitas(context, token, new DataResponse.KomunitasCallback() {
            @Override
            public void onSuccess(List<Komunitas> list) {
                view.setProgressBar(false);
                view.onGetKomunitasSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onGetKomunitasFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                view.showHandling(throwable);
            }
        });
    }

    @Override
    public void getQR(Context context, String token, String community_code) {
        view.setProgressBar(true);
        dataRepository.getQR(context, token, community_code, new DataResponse.QRCodeCallback() {
            @Override
            public void onSuccess(ProfileMember list) {
                view.setProgressBar(false);
                view.onGetQRSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onGetQRFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
                throwable.printStackTrace();
                view.showHandling(throwable);
            }
        });
    }
}
