package com.mozaik.beskem.ui.notification;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.data.model.NotificationM;
import com.mozaik.beskem.ui.base.BasePresenter;

import java.util.List;

public class NotifPresenter extends BasePresenter<NotifContract.View> implements NotifContract.Presenter {

    private DataRepository dataRepository;

    public NotifPresenter(NotifContract.View view, DataRepository dataRepository){
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void getNotif(Context context, String token, String accept, String limit, String offset) {
        view.showLoading();
        dataRepository.getNotif(context, token, accept, limit, offset, new DataResponse.GetNotifCallback() {
            @Override
            public void onSuccess(List<NotificationM> list) {
                view.hideLoading();
                view.onGetNotifSuccess(list);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onGetNotifFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }

    @Override
    public void postNotifRead(Context context, String token, String accept, String notifikasi_id) {
        view.showLoading();
        dataRepository.postReadNotif(context, token, accept, notifikasi_id, new DataResponse.PostNotifReadCallback() {
            @Override
            public void onSuccess(String message) {
                view.hideLoading();
                view.onPostNotifReadSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.hideLoading();
                view.onPostNotifReadFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideLoading();
                view.showHandling(throwable);
                throwable.printStackTrace();
            }
        });
    }
}
