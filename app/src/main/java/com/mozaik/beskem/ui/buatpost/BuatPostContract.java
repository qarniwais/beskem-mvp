package com.mozaik.beskem.ui.buatpost;

import android.content.Context;

import com.mozaik.beskem.ui.base.IBasePresenter;
import com.mozaik.beskem.ui.base.IBaseView;
import com.mozaik.beskem.ui.detailkomunitas.DetailKomunitasContract;

import java.util.List;

public interface BuatPostContract {
    interface View extends IBaseView {

        void showLoading();

        void hideLoading();

        void showDialogError(String msg);

        void showHandling(Throwable t);

        void onInputPostSuccess(String message);

        void onInputPostFailed(String message);

        void onUpdatePostSuccess(String message);

        void onUpdatePostFailed(String message);

    }

    interface Presenter extends IBasePresenter<View> {
        void inputPost(Context context, String token, String community_code, String posting_content, String posting_media, String posting_judul);
        void updatePost(Context context, String token,String posting_code, String posting_judul, String posting_content, String posting_media);
    }
}
