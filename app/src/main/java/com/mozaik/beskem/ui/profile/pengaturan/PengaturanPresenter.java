package com.mozaik.beskem.ui.profile.pengaturan;

import android.content.Context;

import com.mozaik.beskem.data.DataRepository;
import com.mozaik.beskem.data.DataResponse;
import com.mozaik.beskem.ui.base.BasePresenter;
import com.mozaik.beskem.ui.home.HomeContract;

public class PengaturanPresenter extends BasePresenter<PengaturanContract.View> implements PengaturanContract.Presenter {

    private DataRepository dataRepository;

    public PengaturanPresenter(PengaturanContract.View view, DataRepository dataRepository) {
        this.view = view;
        this.dataRepository = dataRepository;
    }

    @Override
    public void logoutProcess(Context context,String accept, String token) {
        view.setProgressBar(true);
        dataRepository.logout(context, accept, token, new DataResponse.LogoutCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onLogoutSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
            }
        });
    }

    @Override
    public void updateProfileProcess(Context context, String token, String name, String user_phone, String province_code, String city_code, String district_code, String kelurahan_code, String kode_pos, String user_img, String email) {
        view.setProgressBar(true);
        dataRepository.updateProfile(context, token, name, user_phone, province_code, city_code, district_code, kelurahan_code, kode_pos, user_img, email, new DataResponse.UpdateProfileCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onUpdateProfileSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
            }
        });
    }

    @Override
    public void updatePassword(Context context, String token, String oldPassword, String newPassword) {
        view.setProgressBar(true);
        dataRepository.updatePassword(context, token, oldPassword, newPassword, new DataResponse.UpdatePasswordCallback() {
            @Override
            public void onSuccess(String message) {
                view.setProgressBar(false);
                view.onUpdatePasswordSuccess(message);
            }

            @Override
            public void onMessage(String message) {
                view.setProgressBar(false);
                view.onUpdatePasswordFailed(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.setProgressBar(false);
            }
        });
    }

//    @Override
//    public void forgotPasswordProscess(Context context, String email) {
//        view.setProgressBar(true);
//        dataRepository.forgotPassword(context, email, new DataResponse.ForgotPasswordCallback() {
//            @Override
//            public void onSuccess(String message) {
//                view.setProgressBar(false);
//                view.onForgotPasswordSuccess(message);
//            }
//
//            @Override
//            public void onMessage(String message) {
//                view.setProgressBar(false);
//                view.onForgotPasswordFiled(message);
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//                view.setProgressBar(false);
//            }
//        });
//    }

}
